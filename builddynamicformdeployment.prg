*=======================================================================================
* Dynamic Form Deployment Builder - By Matt Slay
*--------------------------------------------------------------------------------------
*-- This file is only used to help be deploy the files to the cloud server for dowloading through Thor.
*-- It has nothing to do with using DynamicForm.

Local laParse[1], ;
	lcAuthor As String, ;
	lcBuildDate As String, ;
	lcCloudVersionFileContents As String, ;
	lcCode As String, ;
	lcContents As String, ;
	lcDate As String, ;
	lcHeader As String, ;
	lcLocalVersionFile As String, ;
	lcMonthDay As String, ;
	lcName As String, ;
	lcVersion As String, ;
	lcVersionStringForVersionFile As String, ;
	lcYear As String
*:Global ix

Cd 'h:\work\repos\DynamicForm'

lcVersion = '1.7.0 Alpha'
lcName    = 'Dynamic Form'
lcAuthor  = 'Matt Slay'

*-- Create a file in the source file which contains the Version number
lcDate      = Transform (Date(), '@YL')
lcMonthDay  = Alltrim (Getwordnum (m.lcDate, 2, ','))
lcYear      = Alltrim (Getwordnum (m.lcDate, 3, ','))
lcBuildDate = m.lcMonthDay + ', ' + m.lcYear

lcVersionStringForVersionFile = m.lcName + ' - ' + m.lcVersion + ' - ' + m.lcBuildDate + ' - ' + Dtoc (Date(), 1)

lcLocalVersionFile = 'DynamicFormVersionFile.txt'
Delete File (m.lcLocalVersionFile)
Strtofile (m.lcVersionStringForVersionFile, m.lcLocalVersionFile)


*=======================================================================================
*-- Build the cloud version file ------------

Text To m.lcCloudVersionFileContents Noshow Textmerge Pretext 3

	Lparameters toUpdateInfo

		###Text to lcNote NoShow
			<<FileToStr('Changelog.txt')>>
		###EndText


		AddProperty(toUpdateInfo, 'AvailableVersion', '<<lcVersionStringForVersionFile>>')
		AddProperty(toUpdateInfo, 'SourceFileUrl', 'http://bit.ly/DynamicForm_Alpha')
		AddProperty(toUpdateInfo, 'LinkPrompt', 'Dynamic Form Home Page')
		AddProperty(toUpdateInfo, 'Link', 'http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms')
		AddProperty(toUpdateInfo, 'Notes', lcNote)

		Execscript (_Screen.cThorDispatcher, 'Result=', toUpdateInfo)
		Return toUpdateInfo

Endtext

lcCode = Strtran (m.lcCloudVersionFileContents, '###', '')

Strtofile (m.lcCode, '_' + m.lcLocalVersionFile )


*======================================================================================================
*-- Add version info and Header to the top of DynamicForm.prg

Text To m.lcHeader Textmerge Noshow
*=======================================================================================================
* <<lcVersionStringForVersionFile>>
*
* By: Matt Slay
*-------------------------------------------------------------------------------------------------------
Endtext

lcContents = m.lcHeader + Chr(13) + Chr(10)
Dimension m.laParse[1]
Alines (laParse, Filetostr ('DynamicForm.prg'))


For ix = 6 To Alen (m.laParse)

	lcContents = m.lcContents + m.laParse[m.ix]  && AParse[m.ix] contains current line in loop - do whatever with it
	lcContents = m.lcContents + Chr(13) + Chr(10)

Endfor
Try
	Copy File 'DynamicForm.prg' To ('Backups\DynamicForm_' + Ttoc (Datetime(), 1) + '.prg')
Catch
Endtry

Delete File 'DynamicForm.prg'
Strtofile (m.lcContents, 'DynamicForm.prg')

? ' '
? ' '
? ' '
? '================================='
? m.lcVersionStringForVersionFile
