*-- Demo 1 - Binding to a cursor, showing ALL fields
Local loForm As Object
Close Databases

*!*	If Used ('Jobs')
*!*		Use In Jobs
*!*	Endif

Select 0
Use In Select( 'Jobs' )
Use ( Locfile( 'Jobs', 'dbf', 'Jobs table') ) In 0 Shared Again

loForm          = _NewObject ( 'DynamicForm', Locfile ('DynamicForm', 'prg', 'DynamicForm Lib' ) )
loForm.Caption  = 'Dynamic Forms Demo'
loForm.cHeading = 'Job ' + Jobs.job_num

loForm.cAlias = 'Jobs'

loForm.oRenderEngine.nColumnHeight = 400

m.loForm.Show()

If Vartype (m.loForm) = 'O'
	m.loForm.Release()
Endif
