*-- Demo 2 - Binding to a cursor, showing all fields, except we'll skip a few.
Local loForm As Object
Close Databases

Select 0
Use In Select( 'Jobs' )
Use ( Locfile( 'Jobs', 'dbf', 'Jobs table') ) In 0 Shared Again

loForm          = _NewObject ( 'DynamicForm', Locfile ('DynamicForm', 'prg', 'DynamicForm Lib' ) )
loForm.Caption  = 'Dynamic Forms Demo'
loForm.cHeading = 'Job ' + Jobs.job_num

loForm.cAlias                      = 'Jobs'
loForm.oRenderEngine.nColumnHeight = 400

*-- Skip over certain fields.
loForm.oRenderEngine.cSkipFields = 'ipkey, mach_cost, mat_cost, total_cost, mat_mkup'

m.loForm.Show(1, _Screen)

