*-- Demo 4 - Binding to a cursor, using attributes in the the markup to add styling
Local lcBodyMarkup As String, ;
	loForm As Object
Close Databases

Select 0
Use In Select( 'Jobs' )
Use ( Locfile( 'Jobs', 'dbf', 'Jobs table') ) In 0 Shared Again

loForm          = _NewObject ( 'DynamicForm', Locfile ('DynamicForm', 'prg', 'DynamicForm Lib' ) )
loForm.Caption  = 'Dynamic Forms Demo'
loForm.cHeading = 'Job ' + Jobs.job_num

loForm.cAlias = 'Jobs'

loForm.oRenderEngine.lLabelsAbove = .F.

Text To m.lcBodyMarkup Noshow
	
		ipkey
			:enabled => .f.
			:caption => 'Key Value'
			:label-fontbold => .t. |

		job_start |

		p_o_num
			:row-increment => 0 |

		ship_date
			:row-increment => 0 |

		ship_info
			:class => 'editbox'
			:width => 500
			:height => 200
			:anchor => 15 |

Endtext

loForm.cBodyMarkup = m.lcBodyMarkup

m.loForm.Show()


* DEMO NOTES
*--------------------------------------------------------------------------------------- 
* 1. :row-increment => 0
* 2. loForm.oRenderEngine.lLabelsABove =.t.
