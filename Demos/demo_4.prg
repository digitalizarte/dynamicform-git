*-- Demo 4 - Binding to a cursor, using attributes in the the markup to add styling
Set Asserts On 

Local lcBodyMarkup As String, ;
	loForm As Object
Close Databases


	Select 0
	Use In Select( 'Jobs' )
	Use ( Locfile( 'Jobs', 'dbf', 'Jobs table') ) In 0 Shared Again

	loForm          = _NewObject ( 'DynamicForm', Locfile ('DynamicForm', 'prg', 'DynamicForm Lib' ) )
	loForm.Caption  = 'Dynamic Forms Demo'
	loForm.cHeading = 'Job ' + Jobs.job_num

	loForm.cAlias                      = 'Jobs'
	loForm.oRenderEngine.nColumnHeight = 400

	*lcBodyMarkup = 'job_start | p_o_num | ship_date | ship_info'
	* job_star3t |
	* ship_info3
	* 		:class => 'commandbutton' :caption => '...' :width => 20 :row-increment => 0 :margin-left => -10 |
	TEXT To m.lcBodyMarkup Noshow

		ipkey :enabled => .f. :caption => 'Key Value'|
		job_start |
		p_o_num :caption => 'Notes' :width => 200|

		ship_date |
		ship_info :class => 'editbox' :width => 500 :height => 200 :anchor => 15 |

	ENDTEXT

	loForm.cBodyMarkup = m.lcBodyMarkup

	m.loForm.Render()
	Messagebox (m.loForm.oRenderEngine.GetErrorsAsString(), 0, 'Notice.')

	m.loForm.Show()

	* DEMO NOTES
	*---------------------------------------------------------------------------------------
	* 1. Add ipkey 											field ipkey |
	* 2. Disable ipkey field 						:enabled => .f.
	* 3. Add caption to p_o_num field 	:caption => 'PO No.'
	* 4. Convert ship_info to edibox 		:class => 'editbox' :width => 500 :height => 200
	* Add anchor to editbox							:anchor => 15

