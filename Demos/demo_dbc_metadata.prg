
Local loForm As 'DynamicForm'

Use In Select( 'addresses' )
Open Database ( Addbs( Home( 1 ) ) + 'Wizards\Template\Address Book\Data\address book.dbc' ) Shared
Use addresses In 0 Shared Again
Select addresses
Locate

loForm = _NewObject('DynamicForm', Locfile( 'DynamicForm.prg', 'prg' ) )
loForm.Caption = 'Dynamic Forms'
loForm.oRenderEngine.lLabelsAbove = .F. && Generate field labels above each control. Default is .F., meaning "inline with controls", to the left.
loForm.cHeading = 'Sample Form using DBC Metadata'
loForm.nHeadingFontSize = 14 && You can set heading label font size as desired
loForm.oRenderEngine.nColumnHeight = 300

*-- Set Alias ---------
loForm.cAlias = 'Addresses'
llResult = loForm.Render()

If llResult == .T.
	loForm.MinHeight = loForm.cntMain.Height
	loForm.MinWidth = loForm.cntMain.Width
	loForm.Show()

Else && llResult == .T.
	Messagebox(loForm.oRenderEngine.GetErrorsAsString() , 0, 'Notice.')
	loForm.Show(1)

Endif && llResult == .T.

If Vartype(loForm) == 'O' And Lower( loForm.cReturn ) == 'save'

	Release loForm

Endif && Vartype(loForm) == 'O' And Lower(loForm.cReturn) == 'save'

loForm = Null
