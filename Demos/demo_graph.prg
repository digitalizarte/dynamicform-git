*-- Demo 6 - Vector graphics
Local last_x, ;
	last_y, ;
	lcBodyMarkup As String, ;
	lnHeight As Number, ;
	lnPoints As Number, ;
	lnWidth As Number, ;
	loForm As Object
*:Global n, ;
pi2, ;
r, ;
x, ;
x_center, ;
y, ;
y_center
loForm         = _NewObject ( 'DynamicForm', Locfile ('DynamicForm', 'prg', 'DynamicForm Lib' ) )
loForm.Caption = 'Dynamic Forms'

loForm.cHeading      = 'Vector graphics demo.'
loForm.cHeaderMarkup = ''
loForm.cFooterMarkup = ''

lcBodyMarkup = ''
pi2          = Pi() * 2

r        = 250
x_center = 300
y_center = 300
lnPoints = 720

last_x = r * Cos(0) + x_center
last_y = r * Sin(0) + y_center

For N = 1 To m.lnPoints

	x = r * Cos (N / 360 * pi2) + x_center
	Y = r * Sin (N / 360 * pi2) + y_center

	lnWidth  = Abs (Abs (x) - Abs (m.last_x)) + 1
	lnHeight = Abs (Abs (Y) - Abs (m.last_y)) + 1

	lcBodyMarkup = m.lcBodyMarkup + ":class => 'line' " + ;
		':top => ' + Transform (Y) + ;
		':left => ' + Transform (x) + ;
		':height => ' + Transform (m.lnHeight) + ;
		':width => ' + Transform (m.lnWidth) + ;
		":lineslant => '" + Iif ((x > x_center And Y > y_center) Or (x < x_center And Y < y_center), '/', '') + "'|"
	last_x = x
	last_y = Y

Endfor

loForm.cBodyMarkup = m.lcBodyMarkup

m.loForm.Show(1, _Screen)


