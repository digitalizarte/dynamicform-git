
Local loColumn As Column, ;
	loForm As 'DynamicForm', ;
	loGrid As Grid, ;
	loRenderEngine As DynamicFormRenderEngine Of dynamicform.Prg

Use In Select ( 'addresses' )
Open Database ( Addbs ( Home ( 1 ) ) + 'Wizards\Template\Address Book\Data\address book.dbc' ) Shared
Use addresses In 0 Shared Again
Select addresses
Locate

Browse Name m.loGrid Nowait

loGrid.ColumnCount       = 1
loGrid.RowHeight         = 1000
loColumn                 = m.loGrid.Columns[ 1 ]
loColumn.ControlSource   = ''
loColumn.Header1.Caption = 'Datos'
m.loColumn.AddObject ( 'cntwrapperDatos', 'Container' )
m.loColumn.cntwrapperDatos.AddObject ( 'cntDatos', 'Container' )
loColumn.cntwrapperDatos.cntDatos.Visible = .T.
m.loColumn.cntwrapperDatos.cntDatos.Move ( 8, 8 )
loColumn.cntwrapperDatos.cntDatos.BorderWidth = 0
loColumn.CurrentControl                       = 'cntwrapperDatos'
loColumn.Sparse                               = .F.
loColumn.Width                                = m.loGrid.Width * 1.2

loRenderEngine = _NewObject ('DynamicFormRenderEngine', Locfile ( 'dynamicform.prg' ) )
With m.loRenderEngine As DynamicFormRenderEngine Of dynamicform.Prg
	.cAlias           = 'addresses'
	.cFooterMarkup    = ''
	.lResizeContainer = .T.
	.nControlLeft     = 120
	.nVerticalSpacing = 2
	.nColumnHeight    = m.loGrid.Height / 3
	.Render ( m.loColumn.cntwrapperDatos.cntDatos )

Endwith

loGrid.RowHeight                            = m.loColumn.cntwrapperDatos.cntDatos.Height + 16
loColumn.cntwrapperDatos.BackColor          = Rgb ( 255, 255, 255 )
loColumn.cntwrapperDatos.cntDatos.BackColor = Rgb ( 255, 255, 255 )
m.loColumn.cntwrapperDatos.cntDatos.AddObject ( 'shpBorder', 'Shape' )
loColumn.cntwrapperDatos.cntDatos.shpBorder.Visible   = .T.
loColumn.cntwrapperDatos.cntDatos.shpBorder.Curvature = 15
m.loColumn.cntwrapperDatos.cntDatos.shpBorder.Move ( 0, 0, m.loColumn.cntwrapperDatos.cntDatos.Width, m.loColumn.cntwrapperDatos.cntDatos.Height )
loColumn.cntwrapperDatos.cntDatos.shpBorder.BorderWidth = 2
loColumn.cntwrapperDatos.cntDatos.shpBorder.BorderColor = Rgb ( 192, 192, 192 )
loColumn.cntwrapperDatos.cntDatos.shpBorder.FillStyle   = 1
loColumn.cntwrapperDatos.cntDatos.shpBorder.BackStyle   = 0
m.loColumn.cntwrapperDatos.cntDatos.shpBorder.ZOrder ( 1 )
If Pemstatus( m.loColumn.cntwrapperDatos.cntDatos, 'oRenderEngine', 5 )
	m.loColumn.cntwrapperDatos.cntDatos.oRenderEngine = null
	Removeproperty( m.loColumn.cntwrapperDatos.cntDatos, 'oRenderEngine' )
	
Endif

loRenderEngine = Null
loColumn       = Null
loGrid         = Null
