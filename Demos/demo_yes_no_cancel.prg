*-- Custom Dialog box --------------

Local lcBodyMarkup As String, ;
	loForm As Object
loForm = _NewObject ( 'DynamicForm', Locfile ('DynamicForm', 'prg', 'DynamicForm Lib' ) )

TEXT To m.lcBodyMarkup NOSHOW

	.class = 'label'
	.caption = 'Do you want to try Dynamic Forms?'
	.FontSize = 14
	.FontName = 'Arial'
	.AutoSize = .t.
	.margin-bottom = 15 |

	.class = 'DF_ResultButton'
	.caption = '\<Yes' |


	.class = 'DF_ResultButton'
	.caption = '\<No'
	.row-increment = 0 |

	.class = 'DF_ResultButton'
	.caption = '\<Cancel'
	.row-increment = 0 |

ENDTEXT

loForm.oRenderEngine.nControlLeft = 35
loForm.MinHeight                  = 100

loForm.Caption       = 'Please consider this:'
loForm.cHeaderMarkup = ''
loForm.cBodyMarkup   = m.lcBodyMarkup
loForm.cFooterMarkup = ''

m.loForm.Show()

Do Case

	Case m.loForm.cReturn = 'Yes'
		*...

	Case m.loForm.cReturn = 'No'
		*...

	Case m.loForm.cReturn = 'Cancel'
		*...

Endcase


