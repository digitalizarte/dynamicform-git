*-- Demo 3 - Binding to a cursor, showing only selected fields using Markup Syntax
Local lcBodyMarkup As String, ;
	loForm As Object
Close Databases

Select 0
Use In Select( 'Jobs' )
Use ( Locfile( 'Jobs', 'dbf', 'Jobs table') ) In 0 Shared Again

loForm          = _NewObject ( 'DynamicForm', Locfile ('DynamicForm', 'prg', 'DynamicForm Lib' ) )
loForm.Caption  = 'Dynamic Forms Demo'
loForm.cHeading = 'Job ' + Jobs.job_num

loForm.cAlias                      = 'Jobs'
loForm.oRenderEngine.nColumnHeight = 400

*-- Build custom list of fields
lcBodyMarkup = 'job_start | p_o_num | ship_date | ship_info'

loForm.cBodyMarkup = m.lcBodyMarkup

loForm.oRenderEngine.lLabelsabove = .T.

m.loForm.Show()

