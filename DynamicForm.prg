*=======================================================================================================
* Dynamic Form - 1.8.0 Beta - September 08, 2014 - 20140908
*
* By: Matt Slay
*-------------------------------------------------------------------------------------------------------
*--
*-- ALPHA WARNING!!! This is ALPHA software!! Use with caution and testing!!!!
*--
*-- Web Site: http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms
*--
*-- Check the site OFTEN, as this tool will be updated frequently during this alpha stage.
*--
*-- You can automatically download this Component and its updates through "Thor - Check for Updates".
*-- To learn more about Thor and its Tools and Updaters for FoxPro, see:
*-- http://vfpx.codeplex.com/wikipage?title=Thor%20One-Cick%20Update
*--
*-- User Discussions Group: https://groups.google.com/forum/#!forum/foxprodynamicforms
*--
*--	 after JOINING the Group above, you can post to the web forum or
*-- email to: foxprodynamicforms@googlegroups.com
*--
*--=======================================================================================
*--
*-- This source code PRG contains the class definitions needed to create Dynamic Forms in your apps,
*-- and is also a ready-to-run sample to show you a rendered form sample.
*--
*-- Just run this PRG to see a Dynamic Form sample from the code below. The sample creates a Modal form
*-- which is bound to a few Private variables and an simple data object (the data object is created in the code
*-- at run time simply to mock a real data object and to prevent the demo from having to ship with a sample dbf.)
*-- You can also easily bind to local cursors via the cAlias property, rather than data objects. Please
*-- see the documentation link below for more details on using Dynamic Forms with cursors, and advanced
*-- uses like creating Modeless forms or working with Business Objects to save the data when binding to
*-- an oDataObject.
*--
*--=======================================================================================
*-- DOCUMENTAION - Visit this link to see FULL DOCUMENTAION pages:
*--
*-- http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms
*--
*-----------------------------------------------------------------------------------------
*-- VIDEOS -
*--
*-- Video #1 � Introduction and Demos (8:15) View here: http://bit.ly/DynamicForms-Video-1
*--
*-- Video #2 � Exploring the class PRG and code sample (9:09) View here: http://bit.ly/Dynamic-Forms-Video-2
*--
*-----------------------------------------------------------------------------------------

Private lnPrice, laOptions[ 1 ], lnOption
Private loForm As 'DynamicForm'

Local lcBodyMarkup As String, ;
	llResult As Boolean, ;
	loObject As Object

*---- Step 1: Prepare the data...
*-- As noted in the Documentation you can bind the form to table aliases and cursors, private or public variables, or Data Objects.
*-- See http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms#binding
*--
*-- In this example, we'll build a Data Object in code so that we do not have to distribute a sample dbf with this project package.
*-- Often, "Data Objects" come from table rows, via the Scatter command, or other object-building techniques. You then pass that
*-- object into this Dynamic Form per this code sample. Watch the video series to see examples of working with cursors.
*--
m.loObject = Createobject ('Empty')
AddProperty (m.loObject, 'id', '12345')
AddProperty (m.loObject, 'first_name', 'Joe')
AddProperty (m.loObject, 'mid_init', 'N.')
AddProperty (m.loObject, 'last_name', 'Coderman')
AddProperty (m.loObject, 'ad_type', 'Banner')
AddProperty (m.loObject, 'notes', 'This man came here and wrote some codez.')
AddProperty (m.loObject, 'still_here', .F.)
AddProperty (m.loObject, 'has_laptop', .T.)
AddProperty (m.loObject, 'bool_1', .T.)
AddProperty (m.loObject, 'bool_2', .T.)
AddProperty (m.loObject, 'bool_3', .T.)
AddProperty (m.loObject, 'weight', 185)

*-- Step 2: Define the UI layout string (similar to HTML / XAML markup) --------------------------------------------
*-- See http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms#markup_syntax

TEXT To m.lcBodyMarkup Noshow
 	.lLabelsAbove = .t. |

	id			.enabled = .f.
				.fontbold = .t.
				.label.FontBold = .t. |

	ad_type 	.class = 'combobox'
				.RowSource = 'laOptions'
				.RowSourceType = 5
				.row-increment = 0 |

	bool_3	.caption = 'You can specify BOLD captions.'
					.FontBold = .t.
					.width = 400 |


	first_name	.set-focus = .t. |
	mid_init	.row-increment = 0 |
	last_name	.row-increment = 0 |

	notes		.class = 'editbox'
				.width = 400
				.height = 80
				.anchor = 10  .ShowEditButton = .t.|

	lnPrice		.label.caption = 'List Price'
				.label.alignment = 1 |

	weight	.row-increment  = 0
				.label.alignment = 1 |

	lnOption	.class = 'optiongroup'
				.caption = 'Color options.'
				.buttoncount = 2
				.width = 200
				.height = 60
				.option1.caption = 'Red with orange stripes'
				.option1.autosize = .t.
				.option2.caption = 'Purple with black dots'
				.option2.autosize = .t. |

	.class = 'label' 	.caption = 'Thank you for trying DynamicForm.'
						.autosize = .t.
						.render-if = (Day(Date()) > 1)

ENDTEXT

*-- Example of a Private variables that can be bound to also
m.lnPrice  = 107.15
m.lnOption = 2

*-- Array of options/values to display in the ComboBox defined in cMarkup above (note that it's declared Private above ---
Dimension m.laOptions[3]
m.laOptions[1] = 'Banner'
m.laOptions[2] = 'Placard'
m.laOptions[3] = 'Name Tag'

*-- Step 3. Create an instance of DynamicForm class
* See http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms#step3
m.loForm = Createobject ('DynamicForm')

*-- Step 4. Set a few properties on loForm to wire everything up and set rendering options...
* See http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms#step4
m.loForm.oDataObject = m.loObject && Set the data object that the form fields bind to

m.loForm.Caption                    = 'Dynamic Forms'
m.loForm.oRenderEngine.lLabelsAbove = .T. && Generate field labels above each control. Default is .F., meaning "inline with controls", to the left.

*-- Setup Header area (or disable it)-------------
m.loForm.cHeading         = 'Sample Form'
m.loForm.nHeadingFontSize = 14 && You can set heading label font size as desired

*loForm.cHeaderMarkup = ''	&& Set to empty to disable automatic Header markup. Or,
&& assign custom markup string to customize the header area. See http://vfpx.codeplex.com/wikipage?title=Dynamic%20Form%20Main%20Form%20Layout

*-- Set the main body area markup ---------
m.loForm.cBodyMarkup = m.lcBodyMarkup

* loForm.cFooterMarkup = ''	&& Set to empty to disable use of automatic Footer markup. Or,
&& assign custom markup string to customize the footer area. See http://vfpx.codeplex.com/wikipage?title=Dynamic%20Form%20Main%20Form%20Layout

*-- Step 5. Call Render method to create the controls in the Form
*--			See http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms#step5
m.llResult = m.loForm.Render()

m.loForm.MinHeight = m.loForm.cntMain.Height
m.loForm.MinWidth  = m.loForm.cntMain.Width

*-- Step 6. Show the form to the user
* See http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms#step6
If m.llResult = .T.
	*-- Note. You have a chance here to programmatically change anything on the form or controls
	*-- in any way needed before showing the form to the user...
	m.loForm.Show(0)
	*loForm.Show(1, '300,10')	&& 0 = Modeless, 1 = Modal. See http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms%20Properties#show
	&& You can also pass a 'left,top' pair to position the form at a fixed point.
Else
	Messagebox (m.loForm.oRenderEngine.GetErrorsAsString(), 0, 'Notice.')
	*-- If there were any rendering errors (llResult = .f.), then you can read loForm.oRenderEngine.nErrorCount property
	*-- and loForm.oRenderEngine.oErrors collection for a detail of each error. Or call loForm.oRenderEngine.GetErrorsAsString().
	m.loForm.Show ( 0 )

Endif

*-- At this point, the user is interacting with the form, and it will eventually be closed when they click
*-- Save, Cancel, or the [X] button. At that time, flow will return here, and we can then read any property
*-- on loForm and loForm.oRenderEngine, and even access the rendered controls.

*-- Step 7. Proceed with program flow based on whether user clicked Save or Cancel/closed the form.
* See http://vfpx.codeplex.com/wikipage?title=Dynamic%20Forms#step7
If Vartype (m.loForm) = 'O' And Lower (m.loForm.cReturn) = 'save'
	*-- If Save is clicked, the controlsources are already updated with the new values from the UI.
	*-- Do whatever local processing you need following the Save click by the user...
	Release m.loForm

Else
	*-- Do whatever processing for Close/Cancel user action...
	*-- If using the Button Bar or and instance of DF_CancelButton on the form and Cancel was clicked,
	*-- and the property loForm.lRestoreDataOnCancel = .t. (default), then the controlsources will already
	*-- be restored to their original value by the Form class.
Endif

*-- After the preceding Save/Cancel processing, we can now Release the loForm object.

*=======================================================================================
#Define CR Chr(13)
#Define CRCR Chr(13)
* #Define DebugoutCls Debugout Time(0), Program(), m.loErr.Message, m.loErr.Details, m.loErr.ErrorNo, m.loErr.LineContents, m.loErr.StackLevel, This.Class + '.' + m.loErr.Procedure, This.ClassLibrary, m.loErr.Lineno
#Define DebugoutCls Debugout Time(0), Program(), '[Msg]', m.loErr.Message, '[Det]',m.loErr.Details, '[E#]',m.loErr.ErrorNo, '[LC]',m.loErr.LineContents, '[S]',m.loErr.StackLevel, '[Mth]',This.Class + '.' + m.loErr.Procedure, '[Lib]',This.ClassLibrary, '[L#]',m.loErr.Lineno, '|'

Define Class DynamicForm As Form

	Caption = ''
	*-- Binding form fields to a cursor/alias...
	cAlias = '' && The cursor/alias that your form fields bind to. Make sure this alias is opened and positioned to the correct record.

	*-- Binding form fields to a DataObject for data, and optionally setting an oBusinessObject for Saving the data.
	oDataObject               = Null && The object which has the data properties that you form fields bind to.
	oBusinessObject           = Null	&& oBusinessObject often has a Save() method to save the values from oDataObject back to its table.
	cBusinessObjectSaveMethod = 'Save()' && This method on the oBusinessObject will be called when the Save button is clicked.
	cDataObjectRef            = 'Thisform.oDataObject'

	lRestoreDataOnCancel = .T.	&& When .T., changes to oDataObject or cAlias will be restored to their original values if form is Cancelled by user.
	lClearEventsOnClose  = .F.	&& Only use if you want to call Clear Events when this form is closed.
	oRenderEngine        = Null 		&& Will be populated in Thisform.Init() event. You can override with your own instance of a Render Engine after this form in Initlialized.

	cHeading             = Null			&& This text is displayed in the default Header area of the form.
	cSaveButtonCaption   = Null
	cCancelButtonCaption = Null
	nHeadingFontSize     = 14		&& The font size for the label in the Header area.

	cHeaderMarkup        = Null
	cBodyMarkup          = Null
	cFooterMarkup        = Null
	cPopupFormBodyMarkup = Null

	MinWidth  = 400
	MinHeight = 250

	*-- Consider these as ReadOnly after the form has been Hidden by one of the form Buttons --------
	lSaveClicked   = .F.
	lCancelClicked = .F.
	cReturn        = ''
	cHandle        = '' && A unique string used to store a a reference to this form on _screen. This is used in handling Modeless forms.

	Width       = 10000 && This high values will be set to actual rendered size in the Show() method
	Height      = 10000 && This high values will be set to actual rendered size in the Show() method
	DataSession = 1

	*-- DAE 2012-12-01 Configuraci�n de la clase y libreria del RenderEngine.
	*--				 Configuring the RenderEngine class and library.
	cRenderEngineClass    = 'DynamicFormRenderEngine'
	cRenderEngineClassLib = ''

	Add Object cntMain As Container With ;
		Top = 0, ;
		Left = 0, ;
		Width = 1, ; 	&& Will be resized by RenderEngine to size required to hold all controls
	Height = 1, ;
		BorderWidth = 0, ;
		Anchor = 15, ;
		margin_left = 0, ;
		margin_right = 0, ;
		margin_top = 0, ;
		margin_bottom = 0

	lValidateControlSource = .T.

	lAddLabels = .T.

	* lAddLabels_Assign
	Protected Procedure lAddLabels_Assign
		Lparameters tlAddLabels

		* Debugout Time( 0 ), Program(), tlAddLabels

		This.lAddLabels               = m.tlAddLabels
		This.oRenderEngine.lAddLabels = m.tlAddLabels

	Endproc && lAddLabels_Assign

	*---------------------------------------------------------------------------------------
	Protected Procedure cSaveButtonCaption_Assign
		Lparameters tcCaption

		* Debugout Time( 0 ), Program(), tcCaption

		This.oRenderEngine.cSaveButtonCaption = m.tcCaption

	Endproc

	*---------------------------------------------------------------------------------------
	Protected Procedure cCancelButtonCaption_Assign
		Lparameters tcCaption

		* Debugout Time( 0 ), Program(), tcCaption

		This.oRenderEngine.cCancelButtonCaption = m.tcCaption

	Endproc

	*---------------------------------------------------------------------------------------
	Protected Procedure cHeading_Assign
		Lparameters tcCaption

		* Debugout Time( 0 ), Program(), tcCaption

		This.oRenderEngine.cHeading = m.tcCaption

	Endproc

	*---------------------------------------------------------------------------------------
	Protected Procedure nHeadingFontSize_Assign
		Lparameters tnFontSize

		* Debugout Time( 0 ), Program(), tnFontSize

		This.oRenderEngine.nHeadingFontSize = m.tnFontSize

	Endproc

	*---------------------------------------------------------------------------------------
	Protected Procedure cHeaderMarkup_Assign
		Lparameters tcMarkup

		* Debugout Time( 0 ), Program(), tcMarkup

		This.oRenderEngine.cHeaderMarkup = m.tcMarkup

	Endproc

	*---------------------------------------------------------------------------------------
	Protected Procedure cBodyMarkup_Assign
		Lparameters tcMarkup

		* Debugout Time( 0 ), Program(), tcMarkup

		This.cBodyMarkup               = m.tcMarkup
		This.oRenderEngine.cBodyMarkup = m.tcMarkup

	Endproc

	*---------------------------------------------------------------------------------------
	Protected Procedure cFooterMarkup_Assign
		Lparameters tcMarkup

		* Debugout Time( 0 ), Program(), tcMarkup

		This.oRenderEngine.cFooterMarkup = m.tcMarkup

	Endproc

	* lValidateControlSource_Assing
	Protected Procedure lValidateControlSource_Assing ( m.tlValidateControlSource As Boolean ) As Void

		* Debugout Time( 0 ), Program(), tlValidateControlSource

		This.lValidateControlSource               = m.tlValidateControlSource
		This.oRenderEngine.lValidateControlSource = m.tlValidateControlSource

	Endproc && lValidateControlSource_Assing

	* Init
	Procedure Init() As Void

		* Debugout Time( 0 ), Program()

		This.cHandle = 'DF_' + Sys(2015) && Used to keep a ref to modeless forms alive

	Endproc && Init

	*---------------------------------------------------------------------------------------
	Protected Function oRenderEngine_Access()

		* Debugout Time( 0 ), Program()

		If Vartype ( This.oRenderEngine ) # 'O'
			This.oRenderEngine = _NewObject ( Nvl ( This.cRenderEngineClass, 'DynamicFormRenderEngine' ), This.cRenderEngineClassLib, '' )

		Endif && Vartype( This.oRenderEngine ) # 'O'

		Return This.oRenderEngine

	Endfunc

	*---------------------------------------------------------------------------------------
	Procedure Save

		* Debugout Time( 0 ), Program()

		&& Add any code to be called when the Save() button is clicked.
		&& You can also set a BindEvent() call to this method to react to the Save button click.

	Endproc && You can also set a BindEvent() call to this method to react to the Save button click.

	*---------------------------------------------------------------------------------------
	Procedure Activate
		This.Refresh()

	Endproc
	*---------------------------------------------------------------------------------------
	Procedure Destroy

		* Debugout Time( 0 ), Program()

		If Vartype ( This.oRenderEngine ) == 'O'
			This.oRenderEngine.Destroy() && This will force objects on RE to get released

		Endif && Vartype( This.oRenderEngine ) == 'O'

		This.oRenderEngine   = Null
		This.oBusinessObject = Null
		This.oDataObject     = Null

		Removeproperty ( _Screen, This.cHandle )
		If Pemstatus ( _Screen, 'dfColForms', 5 )
			If ! Empty ( _Screen.dfColForms.GetKey ( This.cHandle ) )
				_Screen.dfColForms.Remove ( This.cHandle )

			Endif

		Endif
		*Store Null To ( This.cHandle )
		This.cHandle = Null

		If This.lClearEventsOnClose
			Clear Events

		Endif && This.lClearEventsOnClose

		DoDefault()

	Endproc && Destroy

	Procedure Release()

		DoDefault()
		Release This

	Endproc

	*---------------------------------------------------------------------------------------
	Procedure QueryUnload

		* Debugout Time( 0 ), Program()

		*-- This Event is triggered when the Form's close button [X] is clicked.
		If This.lRestoreDataOnCancel == .T.
			This.RestoreData()

		Endif && This.lRestoreDataOnCancel == .T.

	Endproc

	*---------------------------------------------------------------------------------------
	Procedure Show
		Lparameters tnStyle, toHostForm

		Local llRO As Boolean, ;
			llShowWindow As Boolean, ;
			lnAnchor As Number, ;
			lnStyle As Number, ;
			lnX As Number, ;
			loControl As Object

		* Debugout Time( 0 ), Program(), tnStyle, toHostForm

		*-- Params:
		*-- tnStyle. 1 = Modal (Default), 0 = Modeless
		*-- toHostForm. (Optional) The form this was called from. If passed, we will center this form in the center of host form.


		*--		lnAnchor = This.cntMain.Anchor
		*--		This.cntMain.Anchor = 0
		*--		This.cntMain.Width = Max(This.MinWidth, This.Width)
		*--		This.cntMain.Height = Max(This.MinHeight, This.Height)
		*--		This.cntMain.Anchor = lnAnchor


		If This.oRenderEngine.lRendered == .F.
			This.Render()

		Endif && This.oRenderEngine.lRendered == .F.

		If Pcount() < 2 And ( Version ( 2 ) # 0 ) && No host passed, and working in dev mode
			This.Left = Max ( Int ( Min ( _vfp.Width, 1600 ) - This.Width ) / 2, 0 )
			This.Top  = Max (Int ( ( _vfp.Height - This.Height ) / 2) - Sysmetric ( 9 ) - 0, 0 )

		Endif && Pcount() < 2 And ( Version( 2 ) # 0 )

		*-- If a reference to the calling form was passed, then center this form in host form
		If Vartype ( m.toHostForm ) == 'O'
			*-- DAE 2012-12-01 Lee el valor de la propiedad.
			*--				 Reads the value of the property.
			m.llShowWindow = Pemstatus ( m.toHostForm, 'ShowWindow', 5 )
			Do Case
				Case m.llShowWindow And m.toHostForm.ShowWindow == 1
					This.Left = Max (Int (m.toHostForm.Width - This.Width) / 2 + m.toHostForm.Left, 0)
					This.Top  = Max (Int (m.toHostForm.Height - This.Height - Sysmetric(9)) / 2 + m.toHostForm.Top, 0)

				Case m.llShowWindow And m.toHostForm.ShowWindow == 2
					This.Left = Max (Int (m.toHostForm.Width - This.Width) / 2, 0)
					This.Top  = Max (Int (m.toHostForm.Height - This.Height - Sysmetric(9)) / 2, 0)

				Otherwise
					This.Left = Max (Int (m.toHostForm.Width - This.Width) / 2 + m.toHostForm.Left, 0)
					This.Top  = Max (Int (m.toHostForm.Height - This.Height) / 2 + m.toHostForm.Top, 0)
			Endcase

		Endif && Vartype ( toHostForm ) == 'O'

		If Vartype ( m.toHostForm ) == 'C'
			This.Top  = Val (Getwordnum (m.toHostForm, 2, ','))
			This.Left = Val (Getwordnum (m.toHostForm, 1, ','))

		Endif

		If Vartype ( m.tnStyle ) # 'N' Or m.tnStyle < 0 Or m.tnStyle > 1
			m.lnStyle = 1

		Else && Vartype ( tnStyle ) # 'N' Or tnStyle < 0 Or tnStyle > 1
			m.lnStyle = m.tnStyle

		Endif && Vartype ( tnStyle ) # 'N' Or tnStyle < 0 Or tnStyle > 1

		If m.lnStyle == 0 && Modeless
			AddProperty (_Screen, This.cHandle, This)
			If ! Pemstatus ( _Screen, 'dfColForms', 5 )
				AddProperty (_Screen, 'dfColForms', Createobject ( 'Collection' ) )
			Endif
			_Screen.dfColForms.Add ( This, This.cHandle )

		Endif && lnStyle == 0

		This.WindowType = m.lnStyle
		DoDefault ( m.lnStyle )

		*-- Set Focus handling...
		If Type ('This.cntMain.DF_oSetFocus') == 'O'
			This.cntMain.DF_oSetFocus.SetFocus()

		Else && Set focus to first enabled control
			For m.lnX = 1 To This.cntMain.ControlCount
				m.loControl = This.cntMain.Controls (m.lnX)
				If Pemstatus ( m.loControl, 'SetFocus', 5 ) And Pemstatus ( m.loControl, 'Enabled', 5 ) And m.loControl.Enabled == .T.
					m.llRO = Pemstatus (m.loControl, 'ReadOnly', 5)
					If ! m.llRO Or ( m.llRO And ! m.loControl.ReadOnly )
						m.loControl.SetFocus()
						Exit

					Endif && ! llRO Or ( llRO And ! loControl.ReadOnly )

				Endif && Pemstatus ( loControl, 'SetFocus', 5 ) And Pemstatus( loControl, 'Enabled', 5 ) And loControl.Enabled == .T.

			Endfor

		Endif && Type ('This.cntMain.DF_oSetFocus') == 'O'

		*-- For some reason the Save button will not appear unless the form is resized. Crazy. So, I jiggle the size around and it appears!!!
		Thisform.Width = Thisform.Width + 1
		Thisform.Width = Thisform.Width - 1

	Endproc

	*---------------------------------------------------------------------------------------
	Procedure Hide

		* Debugout Time( 0 ), Program()

		DoDefault()

		If This.lClearEventsOnClose
			Clear Events

		Endif && This.lClearEventsOnClose

	Endproc


	*---------------------------------------------------------------------------------------
	Procedure Render
		Lparameters tcBodyMarkup

		Local lcRenderSizeMessage As String, ;
			llReturn As Boolean, ;
			lnAnchor As Number, ;
			lnRenderHeight As Number, ;
			lnRenderWidth As Number, ;
			loRenderEngine As Object

		* Debugout Time( 0 ), Program(), tcBodyMarkup

		If Vartype ( m.tcBodyMarkup ) == 'C'
			This.cBodyMarkup = m.tcBodyMarkup

		Endif && Vartype ( tcBodyMarkup ) == 'C'

		*-- DAE 2012-12-01 Lee la propiedad del RenderEngine que tiene un metodo _Access.
		*--				 Access to RenderEngine property that has a method _Access.
		m.loRenderEngine = This.oRenderEngine
		If m.loRenderEngine.lRendered == .F.
			This.SetupRenderEngine()
			m.llReturn = m.loRenderEngine.Render()

		Endif && loRenderEngine.lRendered == .F.

		m.lnAnchor          = This.cntMain.Anchor
		This.cntMain.Anchor = 0

		*-- Move continer for any margin-top or margin-bottom that was set
		This.cntMain.Left = This.cntMain.Left + This.cntMain.margin_left
		This.cntMain.Top  = This.cntMain.Top + This.cntMain.margin_top

		*-- If form it still at its default size, then resize to fit the size of cntMain, which now has all its controls in it.
		If This.Width == 10000
			With This.cntMain
				This.Width = Max (.Width + .Left + .margin_right, This.MinWidth)

			Endwith

		Endif && This.Width == 10000

		If This.Height == 10000
			With This.cntMain
				This.Height = Max (.Top + .Height + .margin_bottom, This.MinHeight)

			Endwith

		Endif && This.Height == 10000

		*-- Make sure container Width and Height fills up the entire width of the form.
		If ! Pemstatus (This.cntMain, 'container_width', 5)
			This.cntMain.Width = Thisform.Width - This.cntMain.Left - This.cntMain.margin_right

		Endif && ! Pemstatus (This.cntMain, 'container_width', 5)

		If ! Pemstatus (This.cntMain, 'container_height', 5)
			This.cntMain.Height = Thisform.Height - This.cntMain.Top - This.cntMain.margin_bottom

		Endif && ! Pemstatus (This.cntMain, 'container_height', 5)

		m.lcRenderSizeMessage = ''
		m.lnRenderWidth       = This.cntMain.Left + This.cntMain.Width + This.cntMain.margin_right
		If m.lnRenderWidth > This.Width
			m.lcRenderSizeMessage = 'Warning: Rendered control area is wider than form width.' + CRCR + ;
				'[' + Transform (m.lnRenderWidth) + ' vs.' + Transform (This.Width) + ']'

			m.lnAnchor = m.lnAnchor - 8

		Endif && lnRenderWidth > This.Width

		m.lnRenderHeight = This.cntMain.Top + This.cntMain.Height + This.cntMain.margin_bottom
		If m.lnRenderHeight > This.Height
			m.lcRenderSizeMessage = Iif (!Empty (m.lcRenderSizeMessage), CRCR + m.lcRenderSizeMessage, '') + ;
				'Rendered control area it taller than form height. ' + CRCR + ;
				'[' + Transform (m.lnRenderHeight) + ' vs.' + Transform (This.Height) + ']'
			m.lnAnchor = m.lnAnchor - 4

		Endif && lnRenderHeight > This.Height

		If ! Empty ( m.lcRenderSizeMessage )
			Messagebox (m.lcRenderSizeMessage, 64, 'Render size warning:')
			m.loRenderEngine.AddError (m.lcRenderSizeMessage, Null)

		Endif && ! Empty ( lcRenderSizeMessage )

		This.cntMain.Anchor = Iif (m.lnAnchor > 0, m.lnAnchor, 0)

		Return m.llReturn

	Endproc

	*---------------------------------------------------------------------------------------
	Procedure RestoreData

		* Debugout Time( 0 ), Program()

		If Vartype ( This.oRenderEngine ) == 'O'
			This.oRenderEngine.RestoreData()

		Endif && Vartype ( This.oRenderEngine ) == 'O'

	Endproc

	*---------------------------------------------------------------------------------------
	Procedure SetupRenderEngine

		Local loRenderEngine As Object

		* Debugout Time( 0 ), Program()

		*-- DAE 2012-12-01 Lee la propiedad del RenderEngine que tiene un metodo _Access.
		*--				 Access to RenderEngine property that has a method _Access.
		m.loRenderEngine = This.oRenderEngine
		With m.loRenderEngine
			.cAlias                    = This.cAlias
			.oBusinessObject           = This.oBusinessObject
			.oDataObject               = This.oDataObject
			.cDataObjectRef            = This.cDataObjectRef
			.cBusinessObjectSaveMethod = This.cBusinessObjectSaveMethod
			.oContainer                = This.cntMain
			.lResizeContainer          = .T.

		Endwith

		Bindevent ( m.loRenderEngine, 'AfterRender', This, 'OnAfterRender' )

	Endproc

	* AfterRender
	* Event
	Procedure AfterRender ( toForm, toEngine )
	Endproc && AfterRender

	* OnAfterRender
	Protected Procedure OnAfterRender ( m.toEngine )
		Raiseevent ( This, 'AfterRender', This, m.toEngine )

	Endproc && OnAfterRender

	*---------------------------------------------------------------------------------------
	Procedure BindBusinessAndDataObjects
		Lparameters toBusinessObject, toDataObject

		* Debugout Time( 0 ), Program(), toBusinessObject, toDataObject
		*-- This method allows you to pass in toBusinessObject and toDataObject all at once.

		This.oBusinessObject = Evl ( m.toBusinessObject, Null )
		This.oDataObject     = Evl ( m.toDataObject, Null )

	Endproc

	* Close
	Procedure Close() As Void

		* Debugout Time( 0 ), Program()

		Try
			This.Hide()
			This.Release()

		Catch To loErr
			Throw

		Endtry

	Endproc && Close

Enddefine && DynamicForm

*=======================================================================================
Define Class DynamicFormRenderEngine As Custom

	*-- See website for complete documentation.
	*-- http.//vfpx.codeplex.com/wikipage?title=Dynamic%20Forms

	cAlias = '' 		&& The name of a cursor or alias to which the cMarkup controls are bound

	*-- These properties deal with the data object and properties/fields on it
	oBusinessObject = Null
	oDataObject     = Null	&& The Object to which the cMarkup controls are bound.
	cDataObjectRef  = '' 	&& The reference to the oDataObject to be used in the ControlSource property of each control that gets generated..
	&& I.e. 'Thisform.oBusObj.oData'
	cBusinessObjectSaveMethod = ''

	cSkipFields = ''
	*cDisabledFields = '' 2012-09-26 Support for this feature has been removed.

	cAttributeNameDelimiterPattern  = ':'
	cAttributeValueDelimiterPattern = '=>'
	cFieldDelimiterPattern          = '|'		 && Caution. Don't use a comma as delimiter. It will likely break things!

	oContainer = Null	&& The container in which controls will be rendered. Set by the calling form.

	*-- These properties are only used when a BusinessObject is configured to handle the Save button click.
	lShowSaveErrors   = .T.	&& Determines if an error dialog will appear if the call to the Business Object Save() method returns .f.
	cSaveErrorMsg     = 'Could not save data in Business Object.'
	cSaveErrorCaption = 'Warning...'

	cHeading             = ''
	cSaveButtonCaption   = Null
	cCancelButtonCaption = Null
	nHeadingFontSize     = 14
	cHeaderMarkup        = Null		&& See GetHeaderMarkup() for default markup string
	cBodyMarkup          = Null			&& The markup/field list for the "body" of the form.
	cFooterMarkup        = Null		&& See GetFooterMarkup() for default markup string
	cPopupFormBodyMarkup = Null	&& See GetPopupFormBodyMarkupMarkup() for default markup string

	*-- These properties control the visual layout and flow of the UI controls
	nControlLeft                             = Null 		&& See Render method for calculation of default value
	nFirstControlTop                         = Null	&& See Render method for calculation of default value
	nVerticalSpacing                         = Null	&& See Render method for calculation of default value
	nVerticalSpacingNonControlSourceControls = Null
	nHorizontalSpacing                       = Null	&& Only used when rendering on the same row with .row=increment = '0'
	nHorizontalLineLeft                      = 10
	nControlHeight                           = 24 		&& I.e. the Height of Textboxes
	nCheckboxHeight                          = 18		&& Default Height for Checkboxes
	nCommandButtonHeight                     = This.nControlHeight
	nHorizontalLabelGap                      = 8 	&& The horizontal spacing between the label and the input control (when label are NOT above the inputs)
	lLabelsAbove                             = .F.			&& Default position if for labels to be inline with the input control, to its left. Set this property to .t. to have the labels placed ABOVE the input control.
	lAutoAdjustVerticalPositionAndHeight     = .F.	&& Forces the .Top and .Height of each control to 'snap'� to a grid system based on increments on
	&& nControlHeight and nVerticalSpacing. The helps keeps control vertically aligned when form spans two
	&& columns or more. When enabling this feature, any .Top and .Height values specified in attributes may
	&& be adjusted to “snap” to the grid system at its incremental points.
	lResizeContainer                  = .F.	&&Indicates if engine should resize (enlarge) oContainer to fit controls as they are added.
	lGenerateEditButtonsForMemoFields = .T.	&& If the field is form a cursor or table and it is a mem data type
	&& DF can render a small command button beside the editbox which can be used
	&& to pop-up a larger editbox for the memo field. This pop-up can also be activated
	&& by double-clicking in the editbox.

	*-- Properties related to the popup editbox form feature
	cPopupFormEditboxClass  = 'editbox'
	nPopupFormEditboxWidth  = 500
	nPopupFormEditboxHeight = 300
	*-- Fields related to columns. See. http.//vfpx.codeplex.com/wikipage?title=Dynamic%20Forms#columns
	nColumnWidth  = 200
	nColumnHeight = 800 	&& The host container can grow to this height before engine will switch to next column

	nTextBoxWidth              = 100
	nEditBoxWidth              = 200
	nNumericFieldTextboxWidth  = 100
	nDateFieldTextboxWidth     = 100
	nDateTimeFieldTextboxWidth = 150
	nCheckBoxWidth             = 100
	nControlWidth              = 100		&& For any other controls besides the specific ones above

	nCheckBoxAlignment = 0	&& 0 = Middle Left (Default in VFP) - Places caption to the right of checkbox.
	&& 1 = Middle Right - Places caption to the left of checkbox.
	*nWidth = 0				&& The _Assign method for this property will set oContianer Width to this value
	*nHeight = 0				&& The _Assign method for this property will set oContianer Height to this value

	*-- Default classes used to create UI controls (You can override these at run time to use your own custom classes.)
	cLabelClass            = 'DF_Label'
	cLabelClassLib         = ''
	cTextboxClass          = 'textbox'
	cTextboxClassLib       = ''
	cEditboxClass          = 'DF_MemoFieldEditBox'
	cEditboxCLassLib       = ''
	cCommandButtonClass    = 'DF_ResultButton'
	cCommandButtonClassLib = ''
	cOptionGroupClass      = 'optiongroup'
	cOptionGroupClassLib   = ''
	cCheckboxClass         = 'DF_Checkbox'
	cCheckboxClassLib      = ''
	cComboboxClass         = 'combobox'
	cComboboxClassLib      = ''
	cListboxClass          = 'listbox'
	cListboxClassLib       = ''
	cSpinnerClass          = 'spinner'
	cSpinnerClassLib       = ''
	cGridClass             = 'grid'
	cGridClassLib          = ''
	cImageClass            = 'image'
	cImageClassLib         = ''
	cTimerClass            = 'timer'
	cTimerClassLib         = ''
	cPageframeClass        = 'pageframe'
	cPageframeGroupClass   = 'pageframe'
	cPageframeClassLib     = ''
	cLineClass             = 'line'
	cLineClassLib          = ''
	cShapeClass            = 'shape'
	cShapeClassLib         = ''
	cContainerClass        = 'container'
	cContainerClassLib     = ''
	cClassLib              = '' && General classlib where controls can be found, if not specified above.
	*---------------------------------------------------------------------------------------
	* Control classes based on data types. If specified, will override the default classes.
	cCharacterClass     = ''
	cCharacterClassLib  = ''
	cNumericClass       = ''
	cNumericClassLib    = ''
	cDateClass          = ''
	cDateClassLib       = ''
	cDateTimeClass      = ''
	cDateTimeClassLib   = ''
	cDatepickerClass    = ''
	cDatepickerClassLib = ''

	*-- Consider these read only ---
	nErrorCount = 0
	oErrors     = Null
	oRegex      = Null

	*=======================================================================================
	*-- Private properties used/maintained by this class only!!
	*Hidden nFieldCount

	*!*		Hidden nColumnCount
	*!*		Hidden nFieldsInCurrentColumn
	*!*		Hidden oFieldList
	*!*		Hidden nNextControlTop
	*!*		Hidden nLastControlTop
	*!*		Hidden nLastControlBottom
	*!*		Hidden nLastControlLeft
	*!*		Hidden nLastControlRight
	*!*		Hidden nControlCount
	*!*		Hidden lInHeader
	*!*		Hidden lInBody
	*!*		Hidden lInFooter
	*!*		Hidden lLastControlRendered
	*!*		Hidden cMarkup
	*!*		Hidden aBackup[1]
	*!*		Hidden aColumnWidths[1]

	Protected nColumnCount
	Protected nFieldsInCurrentColumn
	Protected oFieldList
	Protected nNextControlTop
	Protected nLastControlTop
	Protected nLastControlBottom
	Protected nLastControlLeft
	Protected nLastControlRight
	Protected nControlCount
	Protected lInHeader
	Protected m.lInBody
	Protected m.lInFooter
	Protected m.lLastControlRendered
	Protected m.cMarkup
	Protected m.aBackup[1]
	Protected m.aColumnWidths[1]

	m.nNextControlTop        = 0
	m.nColumnCount           = 1
	m.nFieldsInCurrentColumn = 1
	m.oFieldList             = Null
	m.nLastControlTop        = 0
	m.nLastControlBottom     = 0
	m.nLastControlLeft       = 0
	m.nLastControlRight      = 0
	m.nControlCount          = 0
	m.lInHeader              = .F.
	m.lInBody                = .F.
	m.lInFooter              = .F.
	m.lLastControlRendered   = .F.
	m.lRendered              = .F.
	m.cMarkup                = ''

	Dimension m.aBackup[1]
	Dimension m.aColumnWidths[1]

	*-- DAE 2012-12-01 Lista de propiedades para determinar el tipo de dato de las propiedades de los controles.
	*--				 List of properties to determine the data type of the control properties.
	* Hidden cLogicalAttr
	Protected m.cLogicalAttr
	m.cLogicalAttr = ''
	* Hidden cNumericAttr
	Protected m.cNumericAttr
	m.cNumericAttr = ''
	* Hidden cStringAttr
	Protected m.cStringAttr
	m.cStringAttr = ''
	* Hidden cReadOnlyAttr
	Protected m.cReadOnlyAttr
	m.cReadOnlyAttr = ''

	m.lValidateControlSource = .T.

	m.lAddLabels = .T.



	Protected cChr9
	Protected cChr10
	Protected cChr13
	Protected cChr32

	Protected m.cDefaultFieldMembers
	m.cDefaultFieldMembers = '|CAPTION|CCAPTION|CCOMMENT|CCUETEXT|CDATATYPE|CDISPLAYCLASS|CDISPLAYCLASSLIBRARY|CFORMAT|CINPUTMASK|CLASS|CLASSLIBRARY|FORMAT|INPUTMASK|LFORCEREADONLY|LREADONLY|NCAPTIONWIDTH|NWIDTH|PARENT|TOOLTIPTEXT|WIDTH|'
	Protected m.cFontStyle
	m.cFontStyle = 'N'
	Protected m.cFontName
	m.cFontName  = 'Segoe UI'
	Protected m.nFontSize
	m.nFontSize  = 9
	Protected m.nExtraLen
	m.nExtraLen  = 1.7
	Protected m.nWidth
	m.nWidth           = 0
	Protected m.nWidthTot
	m.nWidthTot        = 0
	Protected m.nWidthCnt
	m.nWidthCnt        = 0
	Protected m.nCaptionWidth
	m.nCaptionWidth    = 0
	Protected m.nCaptionWidthTot
	m.nCaptionWidthTot = 0
	Protected m.nCaptionWidthCnt
	m.nCaptionWidthCnt = 0

	* Init
	Procedure Init() As Void

		This.oFieldList = Createobject ( 'Collection' )
		This.oErrors    = Createobject ( 'Collection' )

		This.cChr9  = Chr ( 9 )
		This.cChr10 = Chr ( 10 )
		This.cChr13 = Chr ( 13 )
		This.cChr32 = Chr ( 32 )

		TEXT To This.cLogicalAttr Noshow Pretext 15
|addlinefeeds|allowaddnew|allowcellselection|allowdelete|allowheadersizing|allowinsert|allowmodalmessages|allowoutput|allowrowsizing|
allowsimultaneousfetch|allowtabs|allowupdate|alwaysonbottom|alwaysontop|autocenter|autorelease|autosize|bindcontrols|bound|boundto|breakonerror|
cancel|centered|clipcontrols|closable|columnlines|comparememo|continuousscroll|controlbox|declarexmlprefix|default|deletemark|disableencode|
dynamiclineheight|enabled|enablehyperlinks|exclusive|fetchasneeded|fetchmemo|fontbold|fontcondense|fontextend|fontitalic|fontoutline|
fontshadow|fontstrikethru|fontunderline|forceclosetag|formattedoutput|halfheightcaption|hideselection|highlight|highlightrow|incrementalsearch|
integralheight|isattribute|isbase64|isbinary|isdiffgram|isnull|itemtips|keyfield|keypreview|lockscreen|mapbinary|mapn19_4tocurrency|mapvarchar|
maxbutton|mdiform|minbutton|movable|moverbars|multiselect|nocptrans|nodata|nodataonload|onetomany|openwindow|optimize|panellink|prepared|
preservewhitespace|quietmode|readonly|recordmark|refreshtimestamp|resizable|respectcursorcp|respectnesting|righttoleft|selected|selectedid|
selectonentry|sendupdates|showtips|sizebox|sorted|sparse|tabstop|terminateread|themes|twopassprocess|unicode|usecodepage|usecursorschema|
usededatasource|usetransactions|utf8encoded|visible|wordwrap|wrapcharincdata|wrapincdata|wrapmemoincdata|xmlnameisxpath|zoombox|
		ENDTEXT

		TEXT To This.cNumericAttr Noshow Pretext 15
|activepage|adocodepage|alignment|allowautocolumnfit|anchor|autocomplete|autohidescrollbar|backcolor|backstyle|batchupdatecount|bordercolor|
borderstyle|borderwidth|boundcolumn|buffermode|buffermodeoverride|buttoncount|calladjustobjectsize|callevaluatecontents|century|colorscheme|
colorsource|columncount|columnorder|conflictchecktype|containerreleasetype|currentdatasession|currentpass|currentx|currenty|curvature|
datasessionid|dateformat|defolelcid|disabledbackcolor|disabledforecolor|disableditembackcolor|disableditemforecolor|displaycount|dockable|
dragmode|drawmode|drawstyle|drawwidth|errorno|fetchsize|fillcolor|fillstyle|firstelement|flags|fontcharset|fontsize|fractiondigits|
frxdatasession|gridlinecolor|gridlines|gridlinewidth|headerheight|helpcontextid|highlightbackcolor|highlightforecolor|highlightrowlinewidth|
highlightstyle|hours|hscrollsmallchange|imemode|increment|interval|itemdata|itemiddata|keyboardhighvalue|keyboardlowvalue|keysort|left|lineno|
listenertype|listindex|listitemid|lockcolumns|lockcolumnsleft|macdesktop|margin|maxheight|maxleft|maxlength|maxrecords|maxtop|maxwidth|
minheight|minwidth|mousepointer|numberofelements|oledragmode|oledropeffects|oledrophasdata|oledropmode|oledroptextinsertion|openviews|
orderdirection|outputtype|pagecount|pageorder|panel|partition|picturemargin|pictureposition|pictureselectiondisplay|picturespacing|readtimeout|
recordsourcetype|rotateflip|rotation|rowheight|rowsourcetype|scalemode|scrollbars|seconds|selectedbackcolor|selectedforecolor|
selecteditembackcolor|selecteditemforecolor|sellength|selstart|sendgdiplusimage|specialeffect|spinnerhighvalue|spinnerlowvalue|stacklevel|
stretch|strictdateentry|tabindex|taborientation|tabstretch|tabstyle|titlebar|topindex|topitemid|updatetype|usememosize|visualeffect|
vscrollsmallchange|whatsthishelpid|wheretype|windowstate|windowtype|
height|left|scalemode|tabindex|tabstop|width|
row_increment|margin_top|margin_bottom|
		ENDTEXT

		TEXT To This.cStringAttr Noshow Pretext 15
|alias|autocompsource|autocomptable|caption|childalias|childorder|columnwidths|comment|conflictcheckcmd|controlsource|conversionfunc|
currentcontrol|cursorschema|cursorsource|database|datasourcetype|datatype|datemark|deletecmd|deletecmddatasourcetype|details|disabledpicture|
displayvalue|downpicture|dragicon|dynamicalignment|dynamicbackcolor|dynamiccurrentcontrol|dynamicfontbold|dynamicfontitalic|dynamicfontname|
dynamicfontoutline|dynamicfontshadow|dynamicfontsize|dynamicfontstrikethru|dynamicfontunderline|dynamicforecolor|dynamicinputmask|
fetchmemocmdlist|fetchmemodatasourcetype|filter|format|headerclass|headerclasslibrary|initialselectedalias|insertcmd|insertcmddatasourcetype|
insertcmdrefreshcmd|insertcmdrefreshfieldlist|insertcmdrefreshkeyfieldlist|linecontents|lineslant|linkmaster|listitem|memberclass|
memberclasslibrary|memowindow|message|mouseicon|nulldisplay|oledragpicture|parentalias|passwordchar|pictureval|polypoints|printjobname|procedure|
recordsource|refreshcmddatasourcetype|refreshignorefieldlist|relationalexpr|rowsource|selectcmd|selectionnamespaces|seltext|statusbartext|tables|
tag|timestampfieldlist|tooltiptext|updatablefieldlist|updatecmd|updatecmddatasourcetype|updatecmdrefreshcmd|updatecmdrefreshfieldlist|
updatecmdrefreshkeyfieldlist|updategramschemalocation|updatenamelist|uservalue|windowlist|xmlname|xmlnamespace|xmlprefix|xmlschemalocation|
xsdfractiondigits|xsdmaxlength|xsdtotaldigits|xsdtype|
		ENDTEXT

		TEXT To This.cReadOnlyAttr Noshow Pretext 15
|activecolumn|activecontrol|activeform|activerow|autoclosetables|autoopentables|baseclass|buttons|class|classlibrary|columns|controlcount|
controls|count|cursorstatus|datasession|declass|declasslibrary|desktop|docked|dockposition|firstnestedtable|formcount|forms|gdiplusgraphics|
hwnd|integralheight|isloaded|ixmldomelement|leftcolumn|listcount|nestedinto|newindex|newitemid|nextsiblingtable|objects|oledragmode|
outputpagecount|pageheight|pageno|pages|pagetotal|pagewidth|parent|parentclass|parenttable|readcycle|readlock|readmouse|readsave|refreshalias|
relativecolumn|relativerow|releasetype|rowcolchange|showintaskbar|showwindow|som|splitbar|text|topindex|topitemid|updategram|viewportheight|
viewportleft|viewporttop|viewportwidth|whatsthisbutton|whatsthishelp|xmladapter|xmlconstraints|xmltable|xmltype|
		ENDTEXT

	Endproc

	* Destroy
	Procedure Destroy

		This.oContainer      = Null
		This.oBusinessObject = Null
		This.oDataObject     = Null
		This.oErrors         = Null
		This.oFieldList      = Null
		This.oRegex          = Null

		DoDefault()

	Endproc && Destroy

	* Render
	Procedure Render
		Lparameters toContainer

		Local lcAlias As String, ;
			lcCode As String, ;
			lcControlSource As String, ;
			lcSkipFields As String, ;
			llReturn As Boolean, ;
			loColApplyOrder As Object, ;
			loErr As Object, ;
			loField As Object, ;
			loFieldWrapper As Object

		* Debugout Time( 0 ), Program(), toContainer

		*-- New in ver 1.6.0 - Open table if cAlias points to something not already open
		m.lcAlias = Juststem ( This.cAlias )
		If ! Empty ( This.cAlias ) And ! Used ( m.lcAlias )
			m.llReturn = This.OpenTable()
			If ! m.llReturn
				Return .F.

			Endif && ! llReturn

		Endif && ! Empty (This.cAlias) And !Used (Juststem (This.cAlias))

		Try
			* This.cAlias = Juststem ( This.cAlias )
			This.cAlias = m.lcAlias

			This.oContainer = Iif ( Vartype ( m.toContainer ) == 'O', m.toContainer, This.oContainer )

			If Vartype ( This.oContainer ) == 'U'
				*-- DAE 2012-12-01 Se cambia el mensaje de error por el lanzamiento de un error.
				*--				 It changes the error message for the launch of an error.
				Error 'Must pass container object into Render() method, or set .oContainer property.'

			Else && Vartype ( This.oContainer ) == 'U'
				AddProperty ( This.oContainer, 'oRenderEngine', This ) && Temporary. This reference will be cleared out at the end of this method.

			Endif && Vartype ( This.oContainer ) == 'U'

			* This.cBodyMarkup = Nvl ( This.cBodyMarkup, This.GetBodyMarkupForAll() )
			If Empty ( This.cBodyMarkup ) Or Isnull ( This.cBodyMarkup )
				This.cBodyMarkup = This.GetBodyMarkupForAll()

			Endif && Empty( This.cBodyMarkup )

			If Vartype ( This.oRegex ) # 'O'
				This.oRegex = Createobject ('VBScript.RegExp')
				This.PrepareRegex()

			Endif && Vartype ( This.oRegex ) # 'O'

			This.PreProcessBodyMarkup()
			This.BuildMarkup()		&& Merges Header, Body, and Footer marker, and adds some special formatting to help with rendering.
			This.BuildFieldList()		&& Build a collection of controls to be rendered by parsing the cMarkup built in BuildMarkup().

			*-- Set default values for various class properties, if the user has not set any values to them...

			This.nControlLeft       = Iif ( This.lAddLabels, Nvl ( This.nControlLeft, Iif ( This.lLabelsAbove == .T., 20, 120 ) ), 20 )
			This.nFirstControlTop   = Nvl ( This.nFirstControlTop, Iif (This.lLabelsAbove == .T., 30, 10 ) )
			This.nHorizontalSpacing = Nvl ( This.nHorizontalSpacing, 15 ) && Only used when rendering on the same row with .row=increment = '0'

			If This.lAutoAdjustVerticalPositionAndHeight == .T.
				If This.lLabelsAbove == .T.
					This.nVerticalSpacing = Nvl ( This.nVerticalSpacing, 50 ) &&	Value is distance from the .Top of the last control to the the .Top of the next control

				Else && This.lLabelsAbove == .T.
					This.nVerticalSpacing = Nvl ( This.nVerticalSpacing, 30 )

				Endif && This.lLabelsAbove == .T.

			Else && This.lAutoAdjustVerticalPositionAndHeight == .T.
				If This.lLabelsAbove == .T.
					This.nVerticalSpacing = Nvl ( This.nVerticalSpacing, 30 ) &&	Value is distance from the BOTTOM of the last control to the the .Top of the next control

				Else && This.lLabelsAbove == .T.
					This.nVerticalSpacing = Nvl ( This.nVerticalSpacing, 15 )

				Endif && This.lLabelsAbove == .T.

			Endif && This.lAutoAdjustVerticalPositionAndHeight == .T.

			This.nVerticalSpacingNonControlSourceControls = Nvl ( This.nVerticalSpacingNonControlSourceControls, 10 )

			This.nNextControlTop    = This.nFirstControlTop
			This.nLastControlRight  = This.nControlLeft - This.nHorizontalSpacing
			This.aColumnWidths[ 1 ] = This.nColumnWidth

			This.cSkipFields = ' ' + Strtran ( This.cSkipFields, ',', ' ' ) + ' '

			*-- Loop over the FieldList collection to render each control, or execute embedded code...
			*-- DAE 2012-12-01 Optimizacion de performance.
			*--				 Performance optimization.
			m.lcSkipFields = Upper ( This.cSkipFields )
			For Each m.loField In This.oFieldList FoxObject
				m.lcControlSource = m.loField.ControlSource
				If Left ( m.lcControlSource, 1 ) + Right ( m.lcControlSource, 1 ) == '()' && If ControlSource element is wrapped in (), then it's to be executed as a VFP code block, Execute it!!
					m.lcCode = Substr ( m.lcControlSource, 2, Len ( m.lcControlSource ) - 2 )
					Try
						&lcCode.

					Catch To m.loErr
						DebugoutCls, m.lcCode
						This.AddError ( 'Error executing code block.', m.loField )

					Endtry

				Else && Left ( lcControlSource, 1 ) + Right ( lcControlSource, 1 ) == '()'
					If Empty ( m.lcControlSource ) Or Atc ( ' ' + m.lcControlSource + ' ', m.lcSkipFields ) == 0
						This.GenerateControl ( m.loField )

					Endif && Empty ( lcControlSource ) Or ! ( ' ' + Upper ( lcControlSource ) + ' ' $ lcSkipFields )

				Endif && Left ( lcControlSource, 1 ) + Right ( lcControlSource, 1 ) == '()'

			Endfor

			This.lRendered = .T. && This indicates that the Render method has been called and has completed.

			*-- Remove the reference to this Render Engine from the oContainer
			This.oContainer.oRenderEngine = Null
			Removeproperty ( This.oContainer, 'oRenderEngine' )

			Raiseevent ( This, 'AfterRender', This )

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

		Return ( This.nErrorCount = 0 )

	Endproc

	* AfterRender
	* Event
	Procedure AfterRender ( toEngine )

	Endproc && AfterRender

	* OpenTable
	Function OpenTable() As Boolean

		Local lRet, ;
			lcAlias As String, ;
			loException As Object

		* Debugout Time( 0 ), Program()

		*-- Ver 1.6.0: If cAlias is specified, but not open, then attempt to open it...

		Try
			Use ( This.cAlias ) Again In 0
			This.cAlias = Juststem ( This.cAlias ) && Now that it's open, trim off any path and extension
			m.lRet      = .T.

		Catch To m.loException
			* This.oErrors.Add (loException)
			This.AddError ( 'Error OpenTable.', Null, m.loException )

			m.lRet = .F.

		Endtry

		Return m.lRet

	Endfunc && OpenTable

	* BuildMarkup
	Procedure BuildMarkup() As Void

		Local lcImportHdr As String, ;
			loErr As Object
		* Debugout Time( 0 ), Program()

		Try
			*-- This method combines the Header, Body, and Footer markup together, and mixes in a
			*-- little extra markup between each section that will help the rendering process keep
			*-- track of where it is working.

			*-- The attribute :import-header, if present, will pull in header from the GetHeaderMarkup()
			* This.cBodyMarkup = Strtran ( This.cBodyMarkup, This.cAttributeNameDelimiterPattern + 'import-header', This.GetHeaderMarkup(), -1, -1, 1 )
			m.lcImportHdr = This.cAttributeNameDelimiterPattern + 'import-header'
			If ! Empty ( Atc ( m.lcImportHdr, Nvl ( This.cBodyMarkup, '' ) ) )
				This.cBodyMarkup = Strtran ( This.cBodyMarkup, m.lcImportHdr, This.GetHeaderMarkup(), -1, -1, 1 )

			Endif && ! Empty( At( lcImportHdr , This.cBodyMarkup )

			* This.cHeaderMarkup = Nvl (This.cHeaderMarkup, This.GetHeaderMarkup() )
			If Isnull ( This.cHeaderMarkup )
				This.cHeaderMarkup = This.GetHeaderMarkup()

			Endif && Empty( This.cHeaderMarkup )

			*-- See PreProcessBodyMarkup() method to see how it is preparied for use here
			* This.cFooterMarkup = Nvl (This.cFooterMarkup, This.GetFooterMarkup() )
			If Isnull ( This.cFooterMarkup )
				This.cFooterMarkup = This.GetFooterMarkup()

			Endif && Empty( This.cFooterMarkup )

			TEXT To This.cMarkup Noshow Textmerge

						(This.lInHeader = .t.) |
						<<Nvl( This.cHeaderMarkup, '' )>> |
						(This.lInHeader = .f.) |

						(This.nFirstControlTop = This.nLastControlTop + This.nFirstControlTop) |
						(This.nFieldsInCurrentColumn = 1) |
						(This.lInBody = .t.) |
						<<This.cBodyMarkup>> |
						(This.lInBody = .f.) |

						(This.nLastControlBottom = This.oContainer.Height) |
						(This.lInFooter = .t.) |
						<<Nvl( This.cFooterMarkup, '' )>> |
						(This.lInFooter = .f.) |
			ENDTEXT

			* This.cMarkup = Chrtran ( This.cMarkup, Chr(13) + Chr(10), ' ' )
			This.cMarkup = Chrtran ( This.cMarkup, This.cChr13 + This.cChr10, ' ' )

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

	Endproc && BuildMarkup

	* PropertyMatch
	Function PropertyMatch ( tcField, tcList ) As Boolean

		Local lcField As String, ;
			lcList As String, ;
			liCnt As Integer, ;
			liIdx As Integer, ;
			llSkip As Boolean

		* Debugout Time( 0 ), Program(), tcField, tcList

		m.lcField = Upper ( m.tcField )
		m.lcList  = Upper ( m.tcList )
		m.liCnt   = Getwordcount ( m.lcList, ', ' )
		For m.liIdx = 1 To m.liCnt
			If Like ( Alltrim ( Getwordnum ( m.lcList, m.liIdx, ', ') ), m.lcField )
				Return .T.

			Endif && Like ( Alltrim ( Getwordnum ( lcList, liIdx, ', ') ), lcField )

		Endfor

		Return .F.

	Endfunc && PropertyMatch

	* GetClassType
	Function GetClassType ( tcClassName, tcClass, tcClassLib, tcPrefix, tcDataType ) As String

		Local lcClassAux As String, ;
			llRet As Boolean, ;
			loErr As Object

		* Debugout Time( 0 ), Program(), tcClassName, tcClass, tcClassLib, tcPrefix

		Try
			*-- DAE 2012-12-01 Se unificaron todos los prefijos para los distintos tipos de clases en 3 caracteres.
			*-- 			 He unified all prefixes for different types of classes in 3 characters.
			m.llRet      = .T.
			m.lcClassAux = Lower ( m.tcClassName )
			Do Case
				Case m.lcClassAux == 'label'
					m.tcClass    = This.cLabelClass
					m.tcClassLib = Evl ( This.cLabelClassLib, This.cClassLib )
					m.tcPrefix   = 'lbl'

				Case m.lcClassAux == 'textbox'
					m.tcClass    = This.cTextboxClass
					m.tcClassLib = Evl ( This.cTextboxClassLib, This.cClassLib )
					m.tcPrefix   = 'txt'
					*-- These properties, if set, override the default class determined
					If Vartype (m.tcDataType) == 'C'
						Do Case
							Case m.tcDataType == 'C'
								m.tcClass    = Evl (This.cCharacterClass, m.tcClass)
								m.tcClassLib = Evl (This.cCharacterClassLib, m.tcClassLib)

							Case m.tcDataType == 'N'
								m.tcClass    = Evl (This.cNumericClass, m.tcClass)
								m.tcClassLib = Evl (This.cNumericClassLib, m.tcClassLib)

							Case m.tcDataType == 'D'
								m.tcClass    = Evl (This.cDateClass, m.tcClass)
								m.tcClassLib = Evl (This.cDateTimeClassLib, m.tcClassLib)

							Case m.tcDataType == 'T'
								m.tcClass    = Evl (This.cDateTimeClass, m.tcClass)
								m.tcClassLib = Evl (This.cDateTimeClassLib, m.tcClassLib)

						Endcase

					Endif

				Case m.lcClassAux == 'editbox'
					m.tcClass    = This.cEditboxClass
					m.tcClassLib = Evl ( This.cEditboxCLassLib, This.cClassLib )
					m.tcPrefix   = 'edt'

				Case m.lcClassAux == 'commandbutton'
					m.tcClass    = This.cCommandButtonClass
					m.tcClassLib = Evl ( This.cCommandButtonClassLib, This.cClassLib )
					m.tcPrefix   = 'cmd'

				Case m.lcClassAux == 'optiongroup'
					m.tcClass    = This.cOptionGroupClass
					m.tcClassLib = Evl ( This.cOptionGroupClassLib, This.cClassLib )
					m.tcPrefix   = 'opt'

				Case m.lcClassAux == 'checkbox'
					m.tcClass    = This.cCheckboxClass
					m.tcClassLib = Evl ( This.cCheckboxClassLib, This.cClassLib )
					m.tcPrefix   = 'chk'

				Case m.lcClassAux == 'combobox'
					m.tcClass    = This.cComboboxClass
					m.tcClassLib = Evl ( This.cComboboxClassLib, This.cClassLib )
					m.tcPrefix   = 'cbo'

				Case m.lcClassAux == 'listbox'
					m.tcClass    = This.cListboxClass
					m.tcClassLib = Evl ( This.cListboxClassLib, This.cClassLib )
					m.tcPrefix   = 'lst'

				Case m.lcClassAux == 'spinner'
					m.tcClass    = This.cSpinnerClass
					m.tcClassLib = Evl ( This.cSpinnerClassLib, This.cClassLib )
					m.tcPrefix   = 'spn'

				Case m.lcClassAux == 'grid'
					m.tcClass    = This.cGridClass
					m.tcClassLib = Evl ( This.cGridClassLib, This.cClassLib )
					m.tcPrefix   = 'grd'

				Case m.lcClassAux == 'image'
					m.tcClass    = This.cImageClass
					m.tcClassLib = Evl ( This.cImageClassLib, This.cClassLib )
					m.tcPrefix   = 'img'

				Case m.lcClassAux == 'timer'
					m.tcClass    = This.cTimerClass
					m.tcClassLib = Evl ( This.cTimerClassLib, This.cClassLib )
					m.tcPrefix   = 'tmr'

				Case m.lcClassAux == 'pageframe'
					m.tcClass    = This.cPageframeClass
					m.tcClassLib = Evl ( This.cPageframeClassLib, This.cClassLib )
					m.tcPrefix   = 'pgf'

				Case m.lcClassAux == 'line'
					m.tcClass    = This.cLineClass
					m.tcClassLib = Evl ( This.cLineClassLib, This.cClassLib )
					m.tcPrefix   = 'lne'

				Case m.lcClassAux == 'shape'
					m.tcClass    = This.cShapeClass
					m.tcClassLib = Evl ( This.cShapeClassLib, This.cClassLib )
					m.tcPrefix   = 'shp'

				Case m.lcClassAux == 'container'
					m.tcClass    = This.cContainerClass
					m.tcClassLib = Evl ( This.cContainerClassLib, This.cClassLib )
					m.tcPrefix   = 'cnt'

				Otherwise
					m.tcClass    = m.tcClassName
					m.tcClassLib = Evl ( m.tcClassLib, This.cClassLib )
					m.tcPrefix   = Left (m.tcClassName, 3 )
					m.llRet      = .F.

			Endcase

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

		Return m.llRet

	Endfunc && GetClassType

	* AddControl
	Function AddControl ( tcClass, tcClassLib, tcControlSourceField, tcDataType ) As Control

		Local lcBaseClass As String, ;
			lcClass As String, ;
			lcClassLib As String, ;
			lcClassLibType As String, ;
			lcControlName As String, ;
			lcDataType As String, ;
			lcPrefix As String, ;
			llDetect As Boolean, ;
			llNewObject As Boolean, ;
			loControl As Object, ;
			loErr As Object

		* Debugout Time( 0 ), Program(), tcClass, tcClassLib, tcControlSourceField

		Try

			m.lcClass    = m.tcClass
			m.lcClassLib = Evl ( m.tcClassLib, This.cClassLib )

			m.lcClass    = m.tcClass
			m.lcClassLib = m.tcClassLib
			m.lcDataType = m.tcDataType
			*-- DAE 2012-12-01 Refactorizacion se el codigo al metodo GetClassType.
			*-- 			 Refactoring is the code to the method GetClassType.
			m.llDetect = This.GetClassType ( @m.tcClass, @m.lcClass, @m.lcClassLib, @m.lcPrefix, @m.lcDataType )

			Try
				If ! Empty ( m.lcClassLib )
					m.lcClassLibType = Justext ( m.lcClassLib )
					If Atc ( m.lcClassLibType, 'prg fxp' ) > 0  And Atc ( m.lcClassLib, Set ( 'Procedure' ) ) == 0
						Set Procedure To ( m.lcClassLib ) Additive

					Endif && Atc( lcClassLibType, 'prg fxp' ) > 0  And Atc( lcClassLib, Set( 'Procedure' ) ) == 0

					If Atc ( m.lcClassLibType, 'vcx' ) > 0  And Atc ( m.lcClassLib, Set ( 'Classlib' ) ) == 0
						Set Classlib To ( m.lcClassLib ) Additive

					Endif && Atc( lcClassLibType, 'vcx' ) > 0  And Atc( lcClassLib, Set( 'Classlib' ) ) == 0

				Endif && ! Empty( lcClassLib )

				m.llNewObject      = This.oContainer.Newobject ( Sys ( 2015 ), m.lcClass, m.lcClassLib ) && Sys(2015) = Random name for object. Will rename below...
				This.nControlCount = This.nControlCount + 1

			Catch To m.loErr
				DebugoutCls, m.lcClass, m.lcClassLib
				m.llNewObject = .F.

			Endtry

			If m.llNewObject == .T.
				m.loControl = This.oContainer.Controls ( This.oContainer.ControlCount ) && The last control added. See above.
				*-- DAE 2012-12-01 Refactorizacion se el codigo al metodo GetClassType.
				*--				 Refactoring is the code to the method GetClassType.
				If ! m.llDetect
					This.GetClassType ( m.loControl.BaseClass, @m.lcClass, @m.lcClassLib, @m.lcPrefix, @m.lcDataType )

				Endif && ! llDetect

				m.lcControlName = m.lcPrefix + Iif ( ! Empty ( m.tcControlSourceField ), Strtran ( m.tcControlSourceField, '.', '_' ), '' ) + '_' + Transform ( This.nControlCount )

				Try
					m.loControl.Name = m.lcControlName

				Catch To m.loErr
					DebugoutCls, m.lcControlName

				Endtry

			Else && llNewObject == .T.
				m.loControl = Null

			Endif && llNewObject == .T.

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

		Return m.loControl

	Endfunc && AddControl

	* GenerateControl
	Procedure GenerateControl ( toField ) As Void

		Local lHasControlSource, ;
			laProperties[1], ;
			lcAlias As String, ;
			lcAttribute As String, ;
			lcBackupType As String, ;
			lcClassLib As String, ;
			lcControlClass As String, ;
			lcControlSource As String, ;
			lcDataSource As String, ;
			lcDataType As String, ;
			lcDataTypeAux As String, ;
			lcErrorMessage As String, ;
			lcJustStem As String, ;
			llIsMemoField As Boolean, ;
			llRender As Boolean, ;
			llSuccess As Boolean, ;
			llUsed As Boolean, ;
			ln As Number, ;
			lnControlMultiplier As Number, ;
			lnX As Number, ;
			loControl As Object, ;
			loEditButton As Object, ;
			loErr As Object, ;
			loLabel As Object, ;
			luData As Variant

		* Debugout Time( 0 ), Program(), toField, toColApplyOrder

		Assert ! Pemstatus ( m.toField, 'Name', 5 ) Or  m.toField.Name # 'lineFooter'

		m.lHasControlSource = Pemstatus ( m.toField, 'ControlSource', 5 ) And Vartype ( m.toField.ControlSource ) == 'C' And ! Empty ( m.toField.ControlSource )

		*-- Handle/Test the .render-if clause --------------------
		If Pemstatus ( m.toField, 'render_if', 5 )
			Try
				m.llRender = This.GetValue ( m.toField.render_if )

			Catch To m.loErr
				DebugoutCls
				If m.lHasControlSource
					m.lcErrorMessage = 'Error in .render-if clause for [' + m.toField.ControlSource + '].'

				Else && lHasControlSource
					m.lcErrorMessage = 'Error in .render-if clause.'

				Endif && lHasControlSource

				This.AddError ( m.lcErrorMessage, m.toField )
				AddProperty ( m.toField, 'class', 'DF_ErrorContainer' )
				AddProperty ( m.toField, 'cErrorMsg', m.lcErrorMessage )
				If ! Pemstatus ( m.toField, 'Width', 5 )
					AddProperty ( m.toField, 'width', 200 )

				Endif && ! Pemstatus (toField, 'Width', 5)
				AddProperty ( m.toField, 'controlsource', m.toField.ControlSource )
				m.loControl = This.AddControl ( 'DF_ErrorContainer', '', m.toField.ControlSource )
				This.StyleControl ( m.loControl, m.toField, toColApplyOrder )

			Endtry

			If !m.llRender
				If This.lLastControlRendered == .T. And This.GetValue ( m.toField.row_increment ) # 0
					This.nLastControlTop   = This.nNextControlTop + ((This.GetValue (m.toField.row_increment) - 1) * This.nVerticalSpacing)
					This.nNextControlTop   = This.nLastControlTop
					This.nLastControlRight = This.nControlLeft - This.nHorizontalSpacing

				Endif

				This.lLastControlRendered = .F.

				Return

			Endif

		Endif

		Try
			*-- DAE 2012-12-01 Valida que tenga ControlSource.
			*--				 Validates that have ControlSource.
			* If This.lValidateControlSource And Pemstatus ( toField, 'ControlSource', 5 ) And ! Empty ( toField.ControlSource )
			If This.lValidateControlSource And m.lHasControlSource
				m.lcAlias = Alias()

				Do Case
						*-- Does the referenced controlsource exist on the oDataObject?
					Case Vartype ( This.oDataObject ) == 'O' And Pemstatus ( This.oDataObject, m.toField.ControlSource, 5 ) && Binding on an Object
						m.lcControlSource = This.cDataObjectRef + '.' + m.toField.ControlSource
						m.lcDataSource    = 'This.oDataObject.' + m.toField.ControlSource

						*-- Does the referenced controlsource exist in cAlias?
					Case ! Empty ( This.cAlias ) And Used (This.cAlias) And ! Empty ( Field ( m.toField.ControlSource, This.cAlias ) )
						m.lcControlSource = This.cAlias + '.' + m.toField.ControlSource
						m.lcDataSource    = m.lcControlSource
						m.llIsMemoField   = This.IsMemoField ( This.cAlias, m.toField.ControlSource )
						*-- Does the referenced controlsource exist in the current work area ?
					Case ! Empty ( m.lcAlias ) And ! Empty ( Field ( m.toField.ControlSource, m.lcAlias ) )
						m.lcControlSource = m.lcAlias + '.' + m.toField.ControlSource
						m.lcDataSource    = m.lcControlSource
						m.llIsMemoField   = This.IsMemoField ( m.lcAlias, m.toField.ControlSource )
						*-- If none of the above were true, let's see if the referenced controlsource is a defined variable, or a Cursor.Field
					Otherwise
						*-- Probably a Cursor.Field controlsource
						m.llIsMemoField   = This.IsMemoField ( Getwordnum ( m.toField.ControlSource, 1, '.' ), Getwordnum ( m.toField.ControlSource, 2, '.' ) )
						m.lcControlSource = m.toField.ControlSource
						m.lcDataSource    = m.lcControlSource

				Endcase

				Try
					m.luData     = Evaluate ( m.lcDataSource )
					m.lcDataType = Vartype ( m.luData )
					m.llSuccess  = .T.
					Try
						m.lcDataTypeAux = Type ( m.lcDataSource )
						m.lcDataType    = m.lcDataTypeAux

					Catch
					Endtry

				Catch To m.loErr
					DebugoutCls, m.lcControlSource, m.lcDataSource
					*-- If all of the above failed to give us any data, then render an error label in this spot.
					m.lcErrorMessage = 'Controlsource [' + m.toField.ControlSource + '] not found.'
					This.AddError ( m.lcErrorMessage, m.toField )
					AddProperty ( m.toField, 'class', 'DF_ErrorContainer' )
					AddProperty ( m.toField, 'cErrorMsg', m.lcErrorMessage )
					If !Pemstatus ( m.toField, 'Width', 5 )
						AddProperty ( m.toField, 'width', 200 )

					Endif

					AddProperty ( m.toField, 'controlsource', m.lcControlSource )

				Endtry

			Else && Pemstatus ( toField, 'ControlSource', 5 ) And ! Empty ( toField.ControlSource )
				If Pemstatus ( m.toField, 'DataType', 5 ) And ! Empty ( m.toField.DataType )
					m.lcDataType = m.toField.DataType
					m.llSuccess  = .T.

				Endif && Pemstatus( toField, 'DataType', 5 ) And ! Empty( toField.DataType )


			Endif && Pemstatus ( toField, 'ControlSource', 5 ) And ! Empty ( toField.ControlSource )

			*-- If we were able to resolve the Controlsource, let's analyze what the original source was
			If m.llSuccess
				m.lcJustStem = Juststem ( m.lcDataSource )
				m.llUsed     = Used ( m.lcJustStem )
				Do Case
					Case Type ( m.lcJustStem ) == 'O'
						m.lcBackupType = 'Object'

						* Case llUsed And '.' $ lcDataSource
					Case m.llUsed And Atc ( '.', m.lcDataSource ) > 0
						m.lcBackupType = 'Alias'

						* Case ! ('.' $ lcDataSource And llUsed)
					Case ! ( m.llUsed And Atc ( '.', m.lcDataSource ) > 0 )
						m.lcBackupType = 'Property'

					Otherwise
						m.lcBackupType = ''

				Endcase

			Else && llSuccess
				m.lcDataType   = ''
				m.lcBackupType = ''

			Endif && llSuccess

			* If ! Empty ( toField.ControlSource )
			If m.lHasControlSource
				*-- Note: These baseclasses will be re-mapped to specific classes from the RenderEngine properties in the AddControl() method call below.
				Do Case
					Case m.llIsMemoField Or m.lcDataType == 'M'
						m.lcControlClass = 'DF_MemoFieldEditBox'

					Case m.lcDataType == 'L'
						m.lcControlClass = 'checkbox'

					Otherwise
						m.lcControlClass = 'textbox'

				Endcase

			Else && ! Empty ( toField.ControlSource )
				m.lcControlClass = ''

				* Endif && ! Empty ( toField.ControlSource )
			Endif && lHasControlSource

			*-- Override default class, if one is specified in attributes
			If Pemstatus (m.toField, 'Class', 5)
				m.lcControlClass = m.toField.Class

			Endif && Pemstatus (toField, 'Class', 5)

			*-- Handle ClassLib assignment in the attribute list...
			If Pemstatus (m.toField, 'ClassLibrary', 5)
				m.lcClassLib = m.toField.ClassLibrary

			Else && Pemstatus (toField, 'ClassLibrary', 5)
				m.lcClassLib = This.cClassLib

			Endif && Pemstatus (toField, 'ClassLibrary', 5)

			*-- Create the control, (also adds it to the continer)
			If ! Empty ( m.lcControlClass )
				m.loControl = This.AddControl ( m.lcControlClass, m.lcClassLib, m.toField.ControlSource, m.lcDataType )

				If Vartype ( m.loControl ) == 'O' And Pemstatus (m.loControl, 'oRenderEngine', 5)
					m.loControl.oRenderEngine = This

				Endif && Pemstatus(loControl, 'oRenderEngine', 5)

				*-- If control could not be created, show an error container...
				If Vartype ( m.loControl ) # 'O'
					m.llSuccess = .F.
					* If Vartype( toField.ControlSource ) == 'C'
					If m.lHasControlSource
						m.lcErrorMessage = 'Error in Class/ClassLibrary settings for [' + m.toField.ControlSource + '].'

					Else && Vartype( toField.ControlSource ) == 'C'
						m.lcErrorMessage = 'Error in Class/ClassLibrary.'

					Endif && Vartype( toField.ControlSource ) == 'C'

					This.AddError ( m.lcErrorMessage, m.toField )
					If Pemstatus ( m.toField, 'Class', 5 )
						AddProperty ( m.toField, 'originalclass', Getpem ( m.toField, 'Class' )  )
					Endif
					AddProperty ( m.toField, 'class', 'DF_ErrorContainer' )
					If Pemstatus ( m.toField, 'classlibrary', 5 )
						AddProperty ( m.toField, 'originalclasslibrary', Getpem ( m.toField, 'classlibrary' )  )
					Endif
					AddProperty ( m.toField, 'classlibrary', This.ClassLibrary )
					AddProperty ( m.toField, 'cErrorMsg', m.lcErrorMessage )
					If ! Pemstatus ( m.toField, 'Width', 5 )
						AddProperty ( m.toField, 'width', 200 )

					Endif && ! Pemstatus ( toField, 'Width', 5 )

					AddProperty ( m.toField, 'controlsource', m.lcControlSource )
					m.loControl = This.AddControl ( 'DF_ErrorContainer', m.toField.ClassLibrary, m.toField.ControlSource, m.lcDataType )

				Endif && Vartype ( loControl ) # 'O'

				Assert Vartype ( m.loControl ) == 'O' Message 'loControl Not is an object'

				AddProperty ( m.loControl, 'DataType', m.lcDataType )

				*-- Set ControlSource on loControl
				Try
					*-- DAE 2012-12-01 Valida que tenga ControlSource.
					*--				 Validates that have ControlSource.
					If Pemstatus ( m.loControl, 'ControlSource', 5 )
						m.loControl.ControlSource = m.lcControlSource
						If m.lHasControlSource
							AddProperty ( m.loControl, 'cControlSource', m.toField.ControlSource )

						Endif && lHasControlSource

					Endif && Pemstatus ( loControl, 'ControlSource', 5 )

					If m.llSuccess
						This.BackupData ( m.lcDataSource, m.luData, m.lcBackupType )

					Endif && llSuccess

				Catch
				Endtry

				This.StyleControl ( m.loControl, m.toField ) && (Will also add the label)

				If This.lGenerateEditButtonsForMemoFields And ( m.llIsMemoField Or ( Pemstatus ( m.toField, 'ShowEditButton', 5 ) And This.GetValue ( m.toField.ShowEditButton ) ) )
					m.ln               = m.loControl.Anchor
					m.loControl.Anchor = 0
					m.loControl.Width  = m.loControl.Width - 20
					m.loControl.Anchor = m.ln
					m.loEditButton     = This.AddControl ('DF_EditButton', '', '')
					AddProperty ( m.loEditButton, 'oEditBox', m.loControl )
					m.loEditButton .Visible = .T.
					m.loEditButton .Top = This.nLastControlTop + 2
					m.loEditButton .Left = This.nLastControlRight - 18
					Do Case
						Case Inlist (m.ln, 4, 6)
							m.loEditButton.Anchor = 4

						Case Inlist (m.ln, 8, 9, 10, 11, 13, 15)
							m.loEditButton.Anchor = 8

						Case Inlist (m.ln, 12, 14)
							m.loEditButton.Anchor = 12

					Endcase
				Endif

			Endif && ! Empty (lcControlClass)

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

	Endproc

	* IsMemoField
	Procedure IsMemoField ( tcCursor As String, tcField As String ) As Boolean

		Local laFieldsFromAlias[1], ;
			llIsMemoField As Boolean, ;
			lnFieldFromArray As Number

		* Debugout Time( 0 ), Program(), tcCursor, tcField

		If Empty ( m.tcCursor ) Or Empty ( m.tcField )
			Return .F.

		Endif && Empty (tcCursor) Or Empty (tcField)

		Try
			Afields (laFieldsFromAlias, m.tcCursor)
			m.lnFieldFromArray = Ascan ( m.laFieldsFromAlias, Upper ( m.tcField ) )
			If m.laFieldsFromAlias [m.lnFieldFromArray + 1 ] == 'M'
				m.llIsMemoField = .T.

			Endif && laFieldsFromAlias[lnFieldFromArray + 1] == 'M'

		Catch
			m.llIsMemoField = .F.

		Endtry

		Return m.llIsMemoField

	Endproc && IsMemoField

	* StyleControl
	Procedure StyleControl ( toControl, toField ) As Void

		Local laProperties[1], ;
			lcAttribute As String, ;
			lcBaseClass As String, ;
			lcClass As String, ;
			llIsHL As Boolean, ;
			lnColumnTest As Number, ;
			lnControlMultiplier As Number, ;
			lnHeight As Number, ;
			lnX As Number, ;
			loErr As Object, ;
			loLabel As Object

		* Debugout Time( 0 ), Program(), toControl, toField, toColApplyOrder

		*-- This procedure will set the top, left, width, height, and apply the attributes to the control,
		*-- Format the control and update a few class properties to manage flow
		* With toControl
		m.lcClass = Lower ( m.toControl.Class )
		m.llIsHL  = ( m.lcClass == 'df_horizontalline' )
		* If lcClass # 'df_horizontalline'
		If ! m.llIsHL
			This.AssignHeight ( m.toControl, m.toField )

		Endif && lcClass # 'df_horizontalline'

		This.ManageColumn ( m.toControl, m.toField )
		This.AssignTop ( m.toControl, m.toField )

		* If lcClass # 'df_horizontalline' && Special handling for this control in Do Case below. Can't apply Left or Width here.
		If ! m.llIsHL && Special handling for this control in Do Case below. Can't apply Left or Width here.
			This.AssignWidth ( m.toControl, m.toField )
			This.AssignLeft ( m.toControl, m.toField )

		Endif && lcClass # 'df_horizontalline'

		m.lcBaseClass = Lower ( m.toControl.BaseClass )
		Do Case
			Case m.lcBaseClass == 'commandbutton'
				This.StyleCommandButton ( m.toControl, m.toField )

			Case m.lcBaseClass == 'checkbox'
				This.StyleCheckbox ( m.toControl, m.toField )

				* Case lcClass == 'df_horizontalline' Or ( PemStatus( toField, 'lFitToParent', 5 ) And toField.lFitToParent )
			Case m.llIsHL Or ( Pemstatus ( m.toField, 'lFitToParent', 5 ) And m.toField.lFitToParent )
				If ! m.llIsHL
					m.lnHeight = m.toField.Height

				Endif && ! llIsHL

				This.StyleHorizontalLine ( m.toControl, m.toField )

				If ! m.llIsHL
					AddProperty ( m.toField, 'Height', m.lnHeight )

				Endif

		Endcase

		If Pemstatus ( m.toControl, 'Visible', 5 )
			m.toControl.Visible = .T.

		Endif

		This.ApplyAttributes ( m.toControl, m.toField )

		If m.lcClass == 'df_errorcontainer'
			AddProperty ( m.toControl, 'Enabled', .T. ) && This is to ensure that error container is rendered as ENABLED

		Endif && lcClass = 'df_errorcontainer'

		*-- For Checkboxes, may need to change the Left property if Alignment is set for caption on left side
		If m.lcBaseClass == 'checkbox'
			If m.toControl.Alignment == 1
				m.toControl.Left = m.toControl.Left - m.toControl.Width + 14

			Endif && .Alignment == 1

		Endif && lcBaseClass == 'checkbox'

		*-- Create a label for any control that has a ControlSource property (skip Checkboxes, they are already handled above)
		If Pemstatus ( m.toControl, 'ControlSource', 5 ) And ! Empty ( m.toField.ControlSource ) And m.lcBaseClass # 'checkbox' And ( This.lAddLabels And m.toField.lAddLabel )

			m.loLabel = This.GenerateLabel ( m.toControl, m.toField )
			*-- DAE 2012-12-01 Nombre del control asociado al Label.
			*--				 Name associated with the Label control.
			AddProperty ( m.loLabel, 'cControlRef', m.toControl.Name )
			*-- Setting .row-increment = '0' will force this control to be generated on same row as last control, so
			*-- we need to shift the label and control over to the right by the width of the label
			Try
				If This.GetValue ( m.toField.row_increment ) == 0 And ! This.lLabelsAbove
					m.toControl.Left = m.toControl.Left + m.loLabel.Width
					m.loLabel.Left   = m.loLabel.Left + m.loLabel.Width

				Endif

			Catch To m.loErr
				DebugoutCls

			Endtry

			AddProperty (m.toControl, 'cLabelcaption', m.loLabel.Caption ) && Added this in 1.6.3
			AddProperty (m.toControl, 'cLabelRef', m.loLabel.Name )

		Endif && Pemstatus ( toControl, 'ControlSource', 5 ) And !Empty ( toField.ControlSource ) And lcBaseClass # 'checkbox'

		If This.lResizeContainer == .T.
			This.ResizeContainer ( m.toControl )

		Endif && This.lResizeContainer == .T.

		*-- Test is we need to widen the current column
		m.lnColumnTest = ( m.toControl.Left + m.toControl.Width ) - ( This.GetColumnLeft() + This.aColumnWidths[ This.nColumnCount ] )

		If m.lnColumnTest > 0
			This.aColumnWidths[ This.nColumnCount ] = This.aColumnWidths[ This.nColumnCount ] + m.lnColumnTest

		Endif && lnColumnTest > 0

		*-- Store some values to class properties so the rendering flow can continue from here for the next control
		This.nFieldsInCurrentColumn = This.nFieldsInCurrentColumn + 1
		This.nLastControlTop        = m.toControl.Top
		This.nLastControlBottom     = m.toControl.Top + m.toControl.Height + This.GetValue ( m.toField.margin_bottom )
		This.nLastControlLeft       = m.toControl.Left
		This.nLastControlRight      = m.toControl.Left + m.toControl.Width + Iif ( Pemstatus ( m.toField, 'margin_right', 5 ), This.GetValue ( m.toField.margin_right ), 0 )
		This.lLastControlRendered   = .T.

		* Endwith

	Endproc && StyleControl

	* ApplyAttributes
	Procedure ApplyAttributes ( toControl, toField ) As Void

		Local laProperties[1], ;
			lcAttribute As String, ;
			lcAux As String, ;
			lcObjectName As String, ;
			lcObjectProperty As String, ;
			lcPath As String, ;
			lcRefName As String, ;
			liCnt As Integer, ;
			llApplied As Boolean, ;
			llHasAnchor As Boolean, ;
			llHasAttr As Boolean, ;
			llHasOrder As Boolean, ;
			llValue As Boolean, ;
			lnAnchor As Number, ;
			lnX As Number, ;
			loCtlAux As Object, ;
			loCtlToAddRef As Object, ;
			loErr As Object, ;
			loProperty As Object, ;
			loRenderOrder As Object, ;
			lxValue

		* Debugout Time( 0 ), Program(), toControl, toField, toColApplyOrder

		* External Array toColApplyOrder

		Try
			*-- Apply any attributes that were set by the user in cMarkup for this control

			m.llHasAnchor = Pemstatus ( m.toControl, 'Anchor', 5 )
			*!*	* llHasOrder = Pemstatus ( toField, '__apply_order', 5 )
			*!*	llHasOrder = Vartype ( toColApplyOrder ) == 'O'

			*!*	If llHasOrder
			*!*		* liCnt = toField.__apply_order.Count
			*!*		liCnt = toColApplyOrder.Count

			*!*	Else && llHasOrder
			*!*		liCnt = Amembers ( laProperties, toField )

			*!*	Endif && llHasOrder

			m.lnX           = 0
			m.loRenderOrder = Getpem ( m.toField, 'oRenderOrder' )
			For Each m.loProperty In m.toField.oRenderOrder FoxObject
				m.lnX         = m.lnX + 1
				m.lcAttribute = Lower (m.loProperty)
				m.lxValue     = Getpem ( m.toField, m.lcAttribute )

				Do Case
						* Case lcAttribute $ 'class controlsource top left col_span margin_bottom margin_left margin_right margin_top render_if row_increment row_span'
						* Case lcAttribute $ 'class controlsource top left'
					Case Atc ( m.lcAttribute, 'class classlibrary controlsource top left' ) > 0
						Loop && Do not apply these attribute, as we've already dealt with them in the AssignXXXXX() methods.

					Case m.lcAttribute == 'set_focus'
						Try
							m.llValue = This.GetValue (m.lxValue)
							If m.llValue == .T.
								AddProperty ( This.oContainer, 'DF_oSetFocus', m.toControl )

							Endif && llValue == .T.

						Catch To m.loErr
							DebugoutCls

						Endtry
						Loop

				Endcase

				If m.llHasAnchor
					m.lnAnchor         = m.toControl.Anchor
					m.toControl.Anchor = 0

				Endif && llHasAnchor

				If Atc ( m.toControl.BaseClass, 'grid optiongroup' ) > 0 And Atc ( '__', m.lcAttribute ) > 0
					m.lcAux            = Strtran ( m.lcAttribute, '__', '.' )
					m.lcObjectName     = Juststem ( m.lcAux )
					m.lcObjectProperty = Justext ( m.lcAux )
					Try

						m.loCtlAux = Getpem ( m.toControl, m.lcObjectName )
						AddProperty ( m.loCtlAux, m.lcObjectProperty, Getpem ( m.toField, m.lcAttribute ) )

					Catch To m.loErr
						DebugoutCls, m.lcAttribute, m.lcAux, m.lcObjectName, m.lcObjectProperty
						Try
							AddProperty ( m.loCtlAux, m.lcObjectProperty, Evaluate ( m.toField.&lcAttribute. ) )

						Catch To m.loErr
							DebugoutCls, m.lcAttribute, m.lcAux, m.lcObjectName, m.lcObjectProperty
							Try
								AddProperty ( m.loCtlAux, m.lcObjectProperty, m.toField.&lcAttribute. )

							Catch To m.loErr
								DebugoutCls, m.lcAttribute, m.lcAux, m.lcObjectName, m.lcObjectProperty

							Endtry

						Endtry

					Endtry

					Loop

				Endif

				*-- JRN 9/13/2012 . Only use EVAL if the attribute value begins with ' ('
				*-- the special text put in place by ParseField
				*-- Todo: Per JRN, need to add code to handle these special ones:
				*-- Here's the list of properties that PEME uses that can look numeric but must be character.
				*-- 'CAPTION', 'COLUMNWIDTHS', 'COMMENT', 'DISPLAYVALUE', 'FORMAT', 'INPUTMASK', 'TAG', 'TOOLTIPTEXT', 'VALUE'

				m.llHasAttr = Pemstatus ( m.toControl, m.lcAttribute, 5 )
				m.llApplied = .F.

				* Assert Upper( lcAttribute ) # 'INPUTMASK'

				If m.llHasAttr And Vartype ( m.lxValue ) == 'C' And Left ( m.lxValue, 2 ) == ' ('
					Try
						m.toControl.&lcAttribute. = Evaluate ( m.lxValue )
						m.llApplied               = .T.

					Catch To m.loErr
						DebugoutCls, m.lcAttribute, m.lxValue
						m.lxValue = Substr ( m.lxValue, 3, Len ( m.lxValue ) - 3 )

					Endtry

				Endif && lHasAttr And Vartype ( lxValue ) == 'C' And Left (lxValue, 2) == ' ('

				If m.llHasAttr And ! m.llApplied
					Try
						If m.lcAttribute # 'reference'
							m.toControl.&lcAttribute. = m.lxValue
							m.llApplied               = .T.

						Else
							If ! Empty ( m.lxValue ) And Atc ( '/', m.lxValue ) > 0
								m.lcRefName     = Substr ( m.lcAux, Ratc ( '/', m.lxValue ) + 1 )
								m.lcPath        =  'toControl.' + Strtran ( Strtran ( m.lxValue, '../', 'parent.' ), '.' + m.lcRefName, '' )
								m.loCtlToAddRef = Evaluate ( m.lcPath )
								AddProperty ( m.loCtlToAddRef, m.lcRefName, m.toControl )

							Endif && ! Empty( lxValue ) And Atc( '/', lxValue ) > 0

						Endif

					Catch To m.loErr
						DebugoutCls, m.lcAttribute, m.lxValue

					Endtry

				Endif && llHasAttr And ! llApplied

				If m.llHasAnchor And Lower ( m.lcAttribute ) # 'anchor'
					m.toControl.Anchor = m.lnAnchor

				Endif && llHasAnchor And Lower (lcAttribute) # 'anchor'

			Endfor

		Catch To m.loErr
			DebugoutCls
			Throw

		Finally
			m.loCtlToAddRef = Null

		Endtry

	Endproc

	* StyleHorizontalLine
	Procedure StyleHorizontalLine ( toControl, toField ) As Void

		Local lnAnchor As Number

		* Debugout Time( 0 ), Program(), toControl, toField

		*-- Special handling for DF_HorizontalLine Class ---------------
		* With toControl
		m.lnAnchor         = m.toControl.Anchor
		m.toControl.Anchor = 0
		AddProperty (m.toField, 'Height', 0)
		m.toField.oRenderOrder.Add ( 'Height' )

		If m.toControl.Left == -1 && If still at the default value (from its class definition)
			m.toControl.Left = This.nHorizontalLineLeft

		Endif && toControl.Left == -1

		If m.toControl.Width == 10000 && If still at the default value (from its class definition)
			m.toControl.Width = Max ( This.oContainer.Width - 2 * m.toControl.Left, 1 )

		Endif && .Width == 10000

		m.toControl.Anchor = m.lnAnchor

		*Endwith

	Endproc && StyleHorizontalLine

	* StyleCheckbox
	Procedure StyleCheckbox ( toControl, toField ) As Void

		Local lnAnchor As Number

		* Debugout Time( 0 ), Program(), toControl, toField

		*-- Slightly special handling for Checkboxes, mostly dealing with whether caption is on the left vs. right of the checkbox
		* With toControl
		m.lnAnchor            = m.toControl.Anchor
		m.toControl.Anchor    = 0
		m.toControl.Caption   = This.GetLabelCaption (m.toControl, m.toField)
		m.toControl.WordWrap  = .T.
		m.toControl.Height    = This.nCheckboxHeight
		m.toControl.Alignment = This.nCheckBoxAlignment && Read default value from class property. Passed attribute may override
		m.toControl.Anchor    = m.lnAnchor

		* Endwith

	Endproc && StyleCheckbox

	* StyleCommandButton
	Procedure StyleCommandButton
		Lparameters toControl, toField

		Local lnAnchor As Number

		* Debugout Time( 0 ), Program(), toControl, toField

		* With toControl
		m.lnAnchor         = m.toControl.Anchor
		m.toControl.Anchor = 0
		m.toControl.Height = This.nCommandButtonHeight
		m.toControl.Anchor = m.lnAnchor

		* Endwith

	Endproc && StyleCommandButton

	* AssignTop
	Procedure AssignTop
		Lparameters toControl, toField, tnSpacing

		Local lnSpacing As Number, ;
			loErr As Object, ;
			luValue As Variant

		* Debugout Time( 0 ), Program(), toControl, toField, tnSpacing

		*-- For controls like commandbutton, line, checkbox, picture, etc, we only need a small amount of vertical space between this control and the previous control
		If ! Pemstatus (m.toControl, 'ControlSource', 5) Or Lower ( m.toControl.BaseClass ) == 'checkbox'
			m.lnSpacing = This.nVerticalSpacingNonControlSourceControls

		Else && ! Pemstatus (toControl, 'ControlSource', 5) Or Lower ( toControl.BaseClass ) == 'checkbox'
			m.lnSpacing = This.nVerticalSpacing

		Endif && ! Pemstatus (toControl, 'ControlSource', 5) Or Lower ( toControl.BaseClass ) == 'checkbox'

		* With toControl
		*-- Setting .row-increment = '0' will force this control to be generated on same row as last control.
		If Pemstatus ( m.toField, 'row', 5 )
			Try
				m.toControl.Top = This.nFirstControlTop + ((This.GetValue (m.toField.Row) - 1) * (m.lnSpacing + This.nControlHeight))

			Catch To m.loErr
				DebugoutCls
				This.AddError ('Error in row attribute value.', m.toField)

			Endtry

		Else && Pemstatus ( toField, 'row', 5 )
			Try
				If This.nFieldsInCurrentColumn == 1 And !This.lInFooter && If we are working on the first control in the column.
					m.toControl.Top = This.nFirstControlTop

				Else && This.nFieldsInCurrentColumn == 1 And !This.lInFooter
					*-- See what row-increment is. Default is 1. User might have requested 0, or more than 1...
					m.luValue = This.GetValue (m.toField.row_increment)
					If m.luValue == 0
						m.toControl.Top = This.nLastControlTop

					Else && luValue == 0
						m.toControl.Top = This.nLastControlBottom + m.lnSpacing + ((m.luValue - 1) * (m.lnSpacing + This.nControlHeight))

					Endif && luValue == 0

				Endif && This.nFieldsInCurrentColumn == 1 And !This.lInFooter

			Catch To m.loErr
				DebugoutCls
				This.AddError ('Error in row-increment attribute value.', m.toField)

			Endtry

		Endif && Pemstatus ( toField, 'row', 5 )

		*-- Override with setting from markup attribute, if specified.
		If Pemstatus ( m.toField, 'top', 5 )
			Try
				m.toControl.Top = This.GetValue (m.toField.Top)

			Catch To m.loErr
				DebugoutCls
				This.AddError ('Error in Top attribute value.', m.toField)

			Endtry

		Endif && Pemstatus ( toField, 'top', 5 )

		*-- Add any .margin-top spacing that was set in attributes
		If Vartype ( m.tnSpacing ) == 'L'
			Try
				m.toControl.Top = m.toControl.Top + This.GetValue (m.toField.margin_top)

			Catch To m.loErr
				DebugoutCls
				This.AddError ('Error in margin-top attribute value.', m.toField)

			Endtry

		Endif && Vartype ( tnSpacing ) == 'L'

		* Endwith

	Endproc && AssignTop

	* AssignLeft
	Procedure AssignLeft
		Lparameters toControl, toField

		Local loErr As Object, ;
			lvValue

		* Debugout Time( 0 ), Program(), toControl, toField

		Try
			*-- First, assign a default Left value
			m.toControl.Left = This.nControlLeft + This.GetColumnLeft()

			*-- Setting .row = "0" will force this control to be generated on same row as last control, and off the the right of the last control
			Try
				If This.GetValue ( m.toField.row_increment ) == 0
					m.toControl.Left = This.nLastControlRight + This.nHorizontalSpacing

				Endif && This.GetValue ( toField.row_increment ) == 0

			Catch To m.loErr
				DebugoutCls

			Endtry

			*-- Override with setting from markup attribute, if specified.
			If Pemstatus ( m.toField, 'left', 5 )
				Try
					m.lvValue        = This.GetValue ( m.toField.Left )
					m.toControl.Left = m.lvValue

				Catch To m.loErr
					DebugoutCls
					This.AddError ('Error in Left attribute value.', m.toField)

				Endtry

			Endif && Pemstatus ( toField, 'left', 5 )

			*-- Adjust for .margin-left as specified in markup attributes
			Try
				m.lvValue        = This.GetValue ( m.toField.margin_left )
				m.toControl.Left = m.toControl.Left + m.lvValue

			Catch To m.loErr
				DebugoutCls
				This.AddError ('Error in margin-left attribute value.', m.toField)

			Endtry

			If Pemstatus ( m.toField, 'centered', 5 )
				m.toControl.Left = ( This.oContainer.Width - m.toControl.Width ) / 2

			Endif && Pemstatus ( toField, 'centered', 5 )

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

	Endproc && AssignLeft

	* AssignWidth
	Procedure AssignWidth
		Lparameters toControl, toField

		Local lHasAnchor, ;
			lcBaseClass As String, ;
			lnAnchor As Number, ;
			loErr As Object

		* Debugout Time( 0 ), Program(), toControl, toField

		*-- This method assigns a default Width value, from Render Engine logic and properties, but,
		*-- this value may be overridden by the user in their markup syntax. If so, it will applied in
		*-- the ApplyAfftributes method() call which occurs later.

		Try
			m.lHasAnchor = Pemstatus ( m.toControl, 'Anchor', 5 )

			If m.lHasAnchor
				m.lnAnchor         = m.toControl.Anchor
				m.toControl.Anchor = 0

			Endif

			If Pemstatus ( m.toControl, 'controlsource', 5 )
				m.lcBaseClass = Lower (m.toControl.BaseClass)
				Do Case
					Case Pemstatus (m.toControl, 'ControlSource', 5) And Empty ( m.toControl.ControlSource )
						*-- Do nothing

					Case Atc ( m.toControl.DataType, 'N' ) > 0
						m.toControl.Width = Int (This.nNumericFieldTextboxWidth)

					Case Atc ( m.toControl.DataType, 'D' ) > 0
						m.toControl.Width = Int (This.nDateFieldTextboxWidth)

					Case  Atc ( m.toControl.DataType, 'T' ) > 0
						m.toControl.Width = Int (This.nDateTimeFieldTextboxWidth)

					Case m.lcBaseClass == 'checkbox'
						m.toControl.Width = Int (This.nCheckBoxWidth)

					Case m.lcBaseClass == 'textbox'
						m.toControl.Width = This.nTextBoxWidth

					Case m.lcBaseClass == 'editbox'
						m.toControl.Width = This.nEditBoxWidth

					Otherwise
						m.toControl.Width = This.nControlWidth
				Endcase

			Endif && Pemstatus ( toControl, 'controlsource', 5 )

			If m.lHasAnchor
				m.toControl.Anchor = m.lnAnchor

			Endif

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

	Endproc && AssignWidth

	* AssignHeight
	Procedure AssignHeight
		Lparameters toControl, toField

		Local lnControHeightMultiplier As Number, ;
			loErr As Exception

		* Debugout Time( 0 ), Program(), toControl, toField

		Try
			*-- This method assigns a default Height value, from Render Engine logic and properties, but,
			*-- this value may be overridden by the user in their markup syntax. If so, it will applied in
			*-- the ApplyAfftributes method() call which occurs later.

			* With toControl
			*-- Slight override to the height if the user passed in a height attribute...
			*-- (We need it to be an integer increment of This.nVerticalSpacing, so let's do a little rounding...)
			m.lnControHeightMultiplier = 1
			If Pemstatus ( m.toField, 'height', 5 ) And This.lAutoAdjustVerticalPositionAndHeight == .T.
				Try
					m.lnControHeightMultiplier = Int ( ( This.GetValue ( m.toField.Height ) - This.nControlHeight ) / This.nVerticalSpacing ) + 1
					m.toControl.Height         = This.nControlHeight + ( m.lnControHeightMultiplier - 1 ) * This.nVerticalSpacing

				Catch To m.loErr
					DebugoutCls
					This.AddError ('Error in height attribute value.', m.toField)

				Endtry

			Else && Pemstatus ( toField, 'height', 5 ) And This.lAutoAdjustVerticalPositionAndHeight == .T.
				If Lower ( m.toControl.BaseClass ) == 'checkbox'
					m.toControl.Height = This.nCheckboxHeight

				Else && Lower (toControl.BaseClass) == 'checkbox'
					m.toControl.Height = This.nControlHeight && + (lnControHeightMultiplier - 1) * This.nVerticalSpacing

				Endif && Lower ( toControl.BaseClass ) == 'checkbox'

			Endif && Pemstatus ( toField, 'height', 5 ) And This.lAutoAdjustVerticalPositionAndHeight == .T.

			* Endwith

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

	Endproc && AssignHeight

	* GetColumnLeft
	Procedure GetColumnLeft

		Local lnColumnLeft As Number, ;
			lnX As Number, ;
			loErr As Object

		* Debugout Time( 0 ), Program()

		Try
			*-- Calculate Left position, considering which "column" we are in...
			m.lnColumnLeft = 0
			For m.lnX = 1 To Alen ( This.aColumnWidths ) - 1
				m.lnColumnLeft = m.lnColumnLeft + This.aColumnWidths[ m.lnX ]

			Endfor

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

		Return m.lnColumnLeft

	Endproc && GetColumnLeft

	* ManageColumn
	Procedure ManageColumn
		Lparameters toControl, toField

		Local lnColumn As Number, ;
			lnControlHeight As Number, ;
			lnMarginBottom As Number, ;
			lnMarginTop As Number, ;
			lnRowIncrement As Number, ;
			lnX As Number, ;
			loErr As Exception

		* Debugout Time( 0 ), Program(), toControl, toField

		Try

			*-- Manage which column we are working in ----------------
			If Pemstatus ( m.toField, 'column', 5 )
				Try
					m.lnColumn = This.GetValue (m.toField.Column)
					If m.lnColumn > This.nColumnCount
						This.nColumnCount           = m.lnColumn
						This.nFieldsInCurrentColumn = 1

					Endif && lnColumn > This.nColumnCount

				Catch To m.loErr
					DebugoutCls
					This.AddError ('Error in column attribute value.', m.toField)

				Endtry

			Else && Pemstatus ( toField, 'column', 5 )

				m.lnRowIncrement = Cast ( This.GetValue ( m.toField.row_increment, 'row_increment' ) As N )
				Assert Vartype ( m.lnRowIncrement ) == 'N'

				m.lnMarginTop = Cast ( This.GetValue ( m.toField.margin_top, 'margin_top'  ) As N )
				Assert Vartype ( m.lnMarginTop ) == 'N'

				m.lnMarginBottom =  Cast ( This.GetValue ( m.toField.margin_bottom, 'margin_bottom' ) As N )
				Assert Vartype ( m.lnMarginBottom ) == 'N'
				m.lnControlHeight = Cast ( ( This.nLastControlBottom + This.nVerticalSpacing + m.toControl.Height + m.lnMarginTop  + m.lnMarginBottom ) As N )
				Assert Vartype ( m.lnControlHeight ) == 'N'

				If m.lnRowIncrement > 0 And  m.lnControlHeight > This.nColumnHeight
					This.nColumnCount           = This.nColumnCount + 1
					This.nFieldsInCurrentColumn = 1

				Endif

			Endif && Pemstatus ( toField, 'column', 5 )

			*-- Update column widths array, and set default column width for any new columns.
			Dimension This.aColumnWidths[ This.nColumnCount ]
			For m.lnX = 1 To Alen (This.aColumnWidths)
				If Vartype ( This.aColumnWidths[ m.lnX ] ) == 'L'
					This.aColumnWidths[ m.lnX ] = This.nColumnWidth

				Endif && Vartype (This.aColumnWidths[lnX]) == 'L'

			Endfor

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

	Endproc && ManageColumn

	* ResizeContainer
	Procedure ResizeContainer
		Lparameters toControl

		Local lHasAnchor, ;
			laControls[1], ;
			lnAnchor As Number, ;
			lnX As Number, ;
			loControl As Object, ;
			loErr As Object, ;
			loFixList As Collection

		* Debugout Time( 0 ), Program(), toControl

		Try
			m.lHasAnchor = Pemstatus ( m.toControl, 'Anchor', 5 )

			If m.lHasAnchor
				m.lnAnchor         = m.toControl.Anchor
				m.toControl.Anchor = 0

			Endif

			m.loFixList = Createobject ( 'Collection' )

			*-- Store each control who use value 4 (bottom) in its anchor setting. Need to clear this, then reset it
			*-- after the container is resized.
			For Each m.loControl In This.oContainer.Controls
				If Pemstatus ( m.loControl, 'Anchor', 5 ) And Inlist ( m.loControl.Anchor, 4, 5, 6, 7, 12, 14, 15 )
					m.loFixList.Add ( m.loControl )
					m.loControl.Anchor = m.loControl.Anchor - 4

				Endif && Inlist ( loControl.Anchor, 4, 5, 6, 7, 12, 14, 15 )

			Endfor

			*-- Adjust container Width and Height if the current control size and positions falls off the container size
			* With toControl
			If Pemstatus ( This.oContainer, 'Width', 5 )
				This.oContainer.Width = Max ( This.oContainer.Width, m.toControl.Left + m.toControl.Width + 8 )

			Endif && Pemstatus ( This.oContainer, 'Width', 5 )

			If Pemstatus ( This.oContainer, 'Height', 5 )
				This.oContainer.Height = Max ( This.oContainer.Height, m.toControl.Top + m.toControl.Height + 8 )

			Endif && Pemstatus ( This.oContainer, 'Height', 5 )

			* Endwith

			For Each m.loControl In m.loFixList FoxObject
				m.loControl.Anchor = m.loControl.Anchor + 4

			Endfor

			If m.lHasAnchor
				m.toControl.Anchor = m.lnAnchor

			Endif

		Catch To m.loErr
			DebugoutCls
			Throw

		Finally
			m.loFixList = Null

		Endtry

	Endproc && ResizeContainer

	* GenerateLabel
	Procedure GenerateLabel
		Lparameters toControl, toField

		Local laProperties[1], ;
			lcAttribute As String, ;
			lcFullAttribute As String, ;
			lcToolTipText As String, ;
			lcibute As String, ;
			lnX As Number, ;
			loErr As Object, ;
			loLabel As Label

		* Debugout Time( 0 ), Program(), toControl, toField

		Try

			m.loLabel = This.AddControl ( This.cLabelClass, This.cLabelClassLib, m.toField.ControlSource )

			If This.lLabelsAbove == .F.
				m.loLabel.Top       = m.toControl.Top + 4
				m.loLabel.Alignment = 1

			Else && This.lLabelsAbove == .F.
				m.loLabel.Top = m.toControl.Top - 18

			Endif && This.lLabelsAbove == .F.

			m.loLabel.AutoSize = .T.
			m.loLabel.Caption  = This.GetLabelCaption ( m.toControl, m.toField )
			m.loLabel.Visible  = .T.

			If Pemstatus ( m.toField, 'ToolTipText', 5 )
				m.lcToolTipText = Getpem ( m.toField, 'ToolTipText' )
				If ! Empty ( m.lcToolTipText )
					m.loLabel.ToolTipText = m.lcToolTipText

				Endif && ! Empty( lcToolTipText )

			Endif && Pemstatus( toField, 'ToolTipText', 5 )

			If This.lLabelsAbove == .F.
				m.loLabel.Left = m.toControl.Left - This.nHorizontalLabelGap - m.loLabel.Width && The locates it properly in case the Caption was set in an attribute

			Else && This.lLabelsAbove == .F.
				If m.loLabel.Alignment == 1
					m.loLabel.Left = m.toControl.Left + m.toControl.Width - m.loLabel.Width

				Else && loLabel.Alignment == 1
					m.loLabel.Left = m.toControl.Left

				Endif && loLabel.Alignment == 1

			Endif && This.lLabelsAbove == .F.

			*-- Apply any attributes that were set by the user for this field
			For m.lnX = 1 To Amembers ( laProperties, m.toField )
				m.lcFullAttribute = m.laProperties[ m.lnX ]
				If Atc ( 'label_', m.lcFullAttribute ) > 0 && And Lower ( Getwordnum ( lcFullAttribute, 1, '_' ) ) == 'label'
					m.lcAttribute = Getwordnum ( m.lcFullAttribute, 2, '_' )
					Try
						m.loLabel.&lcAttribute. = Getpem ( m.toField, m.lcFullAttribute )

					Catch To m.loErr
						DebugoutCls, m.lcAttribute, m.lcFullAttribute
						Try
							m.loLabel.&lcAttribute. = Evaluate ( m.toField.&lcFullAttribute. )

						Catch To m.loErr
							DebugoutCls, m.lcAttribute, m.lcFullAttribute
							Try
								m.loLabel.&lcAttribute. = m.toField.&lcFullAttribute.

							Catch To m.loErr
								DebugoutCls, m.lcAttribute, m.lcFullAttribute
								m.loLabel.Caption   = 'Error in cMarkup for this label'
								m.loLabel.ForeColor = Rgb(255, 0, 0)
								m.loLabel.Tag       = m.lcAttribute

							Endtry

						Endtry

					Endtry

				Endif && Lower ( Getwordnum (lcFullAttribute, 1, '_' ) ) == 'label'

			Endfor

			If This.lLabelsAbove == .F.
				m.loLabel.Left = m.toControl.Left - This.nHorizontalLabelGap - m.loLabel.Width && The locates it properly in case the Caption was set in an attribute

			Else && This.lLabelsAbove == .F.
				If m.loLabel.Alignment == 1
					m.loLabel.Left = m.toControl.Left + m.toControl.Width - m.loLabel.Width

				Else && loLabel.Alignment == 1
					m.loLabel.Left = m.toControl.Left

				Endif && loLabel.Alignment == 1

			Endif && This.lLabelsAbove == .F.

			AddProperty ( m.toField, 'label_left', m.loLabel.Left ) && Save calculate Left to Field info

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

		Return m.loLabel

	Endproc && GenerateLabel

	* GetLabelCaption
	Procedure GetLabelCaption
		Lparameters toControl, toField

		Local lcCaption As String

		* Debugout Time( 0 ), Program(), toControl, toField

		m.lcCaption = ''

		If Pemstatus ( m.toField, 'caption', 5 )
			m.lcCaption = m.toField.Caption

		Else && Pemstatus (toField, 'caption', 5)
			If Pemstatus ( m.toControl, 'ControlSource', 5 )
				m.lcCaption = m.toControl.ControlSource

			Else && Pemstatus (toControl, 'ControlSource', 5)
				If Pemstatus ( m.toField, 'ControlSource', 5 )
					m.lcCaption = m.toField.ControlSource

				Endif && Pemstatus (toField, 'ControlSource', 5)

			Endif && Pemstatus (toControl, 'ControlSource', 5)

			If Atc ( '.', m.lcCaption ) > 0
				m.lcCaption = Justext ( m.lcCaption )

			Endif && '.' $ lcCaption

			m.lcCaption = Strtran ( Proper ( m.lcCaption), '_', ' ' )

		Endif && Pemstatus (toField, 'caption', 5)

		Return m.lcCaption

	Endproc && GetLabelCaption

	* ParseField
	Procedure ParseField
		Lparameters tcParam, toForm, toCnt

		Local laPositions[1], ;
			lcAttribute As String, ;
			lcAttributeAux As String, ;
			lcControlSource As String, ;
			lcErrorMsg As String, ;
			lcErrorMsgLarge As String, ;
			lcParamAux As String, ;
			lcProperty As String, ;
			lcValue As String, ;
			lcValueAux As String, ;
			llAddToCnt As Boolean, ;
			llAddToFrm As Boolean, ;
			llExp As Boolean, ;
			llHasCodeLine As Boolean, ;
			lnLenDelName As Number, ;
			lnLenDelPat As Number, ;
			lnLenDelPatVal As Number, ;
			lnX As Number, ;
			loErr As Exception, ;
			loException As Object, ;
			loField As Object, ;
			loFieldWrapper As Object, ;
			loMatch As Object, ;
			loMatches As Object, ;
			loRegEx As 'VBScript.RegExp', ;
			loRenderOrder As Collection, ;
			lvValue

		* Debugout Time( 0 ), Program(), tcParam, toForm, toCnt

		Try
			*-- DAE 2012-12-01 Se agregaron 2 parametros para capturar los atributos del form y del container.
			*--			 2 parameters were added to capture the attributes of the form and the container.

			*-- This method parses the field definition string passed in (i.e one of the items from cMarkup)
			*-- to return an oField object containing properties of each attribute in the item

			m.llAddToFrm = Vartype ( m.toForm ) == 'O'
			m.llAddToCnt = Vartype ( m.toCnt ) == 'O'
			m.tcParam    = Alltrim ( m.tcParam, 1, ' ', This.cChr9 )
			m.loRegEx    = This.oRegex

			m.lcParamAux = Left ( m.tcParam, 1 )
			Do Case
				Case m.lcParamAux == This.cAttributeNameDelimiterPattern
					m.lcControlSource = ''

				Case m.lcParamAux + Right ( m.tcParam, 1 ) == '()' && This allows FoxPro code to be executed, if it's store as a string in the ControlSource area. No attributes should follow on this line!!
					m.llHasCodeLine   = .T.
					m.lcControlSource = m.tcParam

				Otherwise
					* (here is the 1.5.0 version): lcControlSource = GetWordNum(tcParam, 1, ' .,' + Chr(9)) && There might be no whitespace after property name and before first colon
					* New in 1.6.0:
					m.lcControlSource = Getwordnum ( m.tcParam, 1, ' ' + This.cChr9 + This.cChr13 ) && There must be at least one whitespace or newline after controlsource and before first attribute

			Endcase

			m.loField = Createobject ( 'Empty' )
			AddProperty ( m.loField, 'ControlSource', m.lcControlSource )
			AddProperty ( m.loField, 'row_increment', 1 ) && Go ahead and add this default to every object. Mayb be overridden in the attributes
			AddProperty ( m.loField, 'margin_top', 0 ) && etc
			AddProperty ( m.loField, 'margin_bottom', 0 ) && etc
			AddProperty ( m.loField, 'margin_left', 0 ) && etc
			AddProperty ( m.loField, 'margin_right', 0 ) && etc
			AddProperty ( m.loField, 'lAddLabel', This.lAddLabels ) && etc

			*-- DAE 2012-12-01 Collecion para preservar el orden de aplicacion de las propiedades seg�n el markup.
			*-- Si estoy utilizando una grilla y quiero modificar los datos de una columna o
			*-- el header tengo que respetar el ordern de aplicacion si la grila no tiene creadas las columnas.
			*-- 			 Collecion to preserve the order of application of the properties as the markup.
			*-- If I am using a grid and I want to change a column or
			*-- the header I have to respect the ordern grila apply where no columns created.
			m.loRenderOrder = Createobject ('Collection')
			AddProperty ( m.loField, 'oRenderOrder', m.loRenderOrder )

			m.loMatches = m.loRegEx.Execute ( m.tcParam )

			If Vartype ( m.loMatches ) == 'O' And ! m.llHasCodeLine
				Dimension m.laPositions[ m.loMatches.Count + 1 ] && Create and array of position matches ------------
				m.lnX = 1
				For Each m.loMatch In m.loMatches FoxObject
					m.laPositions[ m.lnX ] = m.loMatch.FirstIndex
					m.lnX                  = m.lnX + 1

				Endfor

				m.laPositions[ m.lnX ] = Len ( m.tcParam )
				*-- Loop over regex matches to pull out attribute/value pairs
				m.lnX            = 1
				m.lnLenDelName   = Len ( This.cAttributeNameDelimiterPattern )
				m.lnLenDelPatVal = m.lnLenDelName + Len ( This.cAttributeValueDelimiterPattern )

				For Each m.loMatch In m.loMatches FoxObject
					m.lcAttribute = Alltrim ( m.loMatch.Value, 1, This.cChr9, This.cChr10, This.cChr13, This.cChr32 )
					m.lcAttribute = Substr ( m.lcAttribute, m.lnLenDelName + 1, Len ( m.lcAttribute ) - m.lnLenDelPatVal )
					m.lcAttribute = Alltrim ( m.lcAttribute, 1, This.cChr9, This.cChr10, This.cChr13, This.cChr32 )
					m.lcAttribute = Strtran ( m.lcAttribute, '-', '_') && Must convert dashes to underscores, as dashes are not allowed in Property names

					* Assert Upper( lcAttribute) # 'INPUTMASK'

					m.lcValue = Rtrim ( Left ( m.tcParam, m.laPositions[ m.lnX + 1 ] ) ) && Trim off everything to the right, starting at the NEXT match pos.
					m.lcValue = Substr ( m.lcValue, m.loMatch.FirstIndex + Len ( m.loMatch.Value ) + 1 ) && Now pull out the value
					m.lcValue = Alltrim ( m.lcValue, ' ', ',', This.cChr9, This.cChr10, This.cChr13, This.cFieldDelimiterPattern ) && Trim off delimiters and white space


					*** JRN 9/13/2012 Properties to be EVAL'd are wrapped in ' (' + lcValue + ')'
					*** note that they can be EVAL'd directly; the parentheses do not hurt
					m.lcValueAux = Left ( m.lcValue, 1 )
					Do Case
						Case m.lcValueAux == '('
							m.lcValue = ' ' + m.lcValue
							*-- DAE 2012-12-01 Detecta si es una expresi�n ejecutable para no evaluarla antes de tiempo.
							*-- 			 Detects whether an expression is not executable for evaluating early.
							If Right ( m.lcValue, 1 ) == ')'
								m.llExp = .T.

							Endif
						Case Atc ( m.lcValueAux, '.0123456789-' ) > 0 And Atc ( m.lcAttribute, 'caption columnwidths comment displayvalue format inputmask tag tooltiptext value' ) == 0 && allows numbers and logicals
							m.lcValue = ' (' + m.lcValue + ')'

							* Case Substr ( lcValue, 2, 1 ) $ '.0123456789-' && old style numbers and logicals in quotes
						Case Atc ( Substr ( m.lcValue, 2, 1 ), '.0123456789-' ) > 0 And Atc ( m.lcAttribute, 'caption columnwidths comment displayvalue format inputmask tag tooltiptext value' ) == 0 && old style numbers and logicals in quotes
							m.lcValue = ' (' + Substr ( m.lcValue, 2, Len ( m.lcValue ) - 2 ) + ')'

						Case Inlist ( m.lcValueAux, ["], ['], '[' )
							*-- DAE 2012-12-01 Si el primer caracter es el inicio de una cadena elimina los delimitadores de cadena.
							*-- Esta modificaci�n permite incluir cadenas en el markup sin delimitadores.
							*--				 If the first character is the beginning of a chain eliminates string delimiters.
							*-- This modification allows to include in the markup strings without delimiters.
							m.lcValue = Substr ( m.lcValue, 2, Len ( m.lcValue ) - 2 ) && Trim off first and last char, which would be some kind of string symbol

						Otherwise

					Endcase

					*-- If a dot is used in the attribute, we'll flag it will 2 underscores, as this is the format for a property assignment
					*-- on a child object within the current control (i.e. optiongroup)
					m.lcAttribute = Strtran ( m.lcAttribute, '.', '__' )

					*-- If this attribute name matches a property name on the RenderEngine class, then apply it to the RenderEngine.
					Try

						* If lcAttribute # 'class' And Pemstatus ( This, lcAttribute, 5 )
						If Atc ( '|' + m.lcAttribute + '|', '|class|classlibrary|' ) == 0 And Pemstatus ( This, m.lcAttribute, 5 )
							m.lvValue = This.GetValue ( m.lcValue, m.lcAttribute )
							AddProperty ( This, m.lcAttribute, m.lvValue )

						Endif

					Catch To m.loErr
						DebugoutCls, m.lcValue, m.lcAttribute, m.lvValue

					Endtry

					*-- If this attribute name begins with "form_" and matches a property on the oConatiner, then apply it to the oContainer's form.
					If m.llAddToFrm And Atc ( 'form_', m.lcAttribute ) > 0
						m.lcProperty = Strtran ( m.lcAttribute, 'form_', '', -1, -1, 1 )
						Try
							m.lvValue = This.GetValue ( m.lcValue, m.lcProperty )
							AddProperty ( m.toForm, m.lcProperty, m.lvValue )
							m.toForm.oRenderOrder.Add ( m.lcProperty )

						Catch To m.loErr
							DebugoutCls, m.tcParam, m.lcAttributeAux, m.lcProperty, m.lcValue, m.lvValue

						Endtry

					Endif && llAddToFrm And 'form_' $ lcAttributeAux

					*-- If this attribute name begins with "container_" and matches a property on the oConatiner, then apply it to the oContainer.
					If m.llAddToCnt And Atc ( 'container_', m.lcAttribute ) > 0
						m.lcProperty = Strtran ( m.lcAttribute, 'container_', '', -1, -1, 1 )
						Try
							m.lvValue = This.GetValue ( m.lcValue, m.lcProperty )
							AddProperty ( m.toCnt, m.lcProperty, m.lvValue )
							m.toCnt.oRenderOrder.Add ( m.lcProperty )

						Catch To m.loErr
							DebugoutCls, m.tcParam, m.lcAttributeAux, m.lcProperty, m.lcValue, m.lvValue

						Endtry

					Endif && llAddToCnt And 'container_' $ lcAttributeAux

					Try
						* If ! lcAttribute $ '__apply_order'
						If m.llExp
							AddProperty ( m.loField, m.lcAttribute, m.lcValue )

						Else && llExp
							m.lvValue = This.GetValue ( m.lcValue, m.lcAttribute )
							AddProperty ( m.loField, m.lcAttribute, m.lvValue )

						Endif && llExp

						m.loField.oRenderOrder.Add ( m.lcAttribute ) && Store this property in the oRenderOrder collection. Used in ApplyAttributes to apply in the same order as they appear in the markup.

						m.lnX = m.lnX + 1

					Catch To m.loErr
						DebugoutCls, m.tcParam, m.lcAttribute, m.lcValue, m.lvValue
						m.lcErrorMsg = 'Error on [' + m.lcControlSource + ']. Attribute: ' + m.lcAttribute + ' Value: ' + m.lcValue && + ' Message: ' + loErr.Message  + ' ' + Transform( loErr.Lineno ) + ' ' + loErr.LineContents
						AddProperty ( m.loField, 'class', 'label' )
						* AddProperty ( loField, 'class', 'DE_ErrorContainer' )
						* AddProperty ( loField, 'caption', 'Error in cMarkup. ' + tcParam )
						AddProperty ( m.loField, 'caption', m.lcErrorMsg )
						AddProperty ( m.loField, 'forecolor', ' (Rgb(255,0,0))' )
						AddProperty ( m.loField, 'fontbold', .T. )
						AddProperty ( m.loField, 'wordwrap', .T. )
						AddProperty ( m.loField, 'width', 500 )
						m.lcErrorMsgLarge = m.lcErrorMsg  + CR + 'Error in cMarkup. ' + m.tcParam + CR + m.loErr.Message
						AddProperty ( m.loField, 'ToolTipText', m.lcErrorMsgLarge )
						AddProperty ( m.loField, 'cToolTipText', m.lcErrorMsgLarge )
						AddProperty ( m.loField, 'cErrorMsg', m.lcErrorMsgLarge )
						AddProperty ( m.loField, 'row_increment', 1 ) && Go ahead and add this default to every object. Mayb be overridden in the attributes
						AddProperty ( m.loField, 'margin_top', 0 ) && etc
						AddProperty ( m.loField, 'margin_bottom', 0 ) && etc
						AddProperty ( m.loField, 'margin_left', 0 ) && etc
						AddProperty ( m.loField, 'margin_right', 0 ) && etc

						This.AddError ( m.lcErrorMsg, m.loField, m.loErr )

					Endtry

				Endfor

			Endif

		Catch To m.loErr
			DebugoutCls
			Throw

		Endtry

		Return m.loField

	Endproc && ParseField

	* EscapeForRegex
	Procedure EscapeForRegex
		Lparameters tcString

		Local lcString As String

		* Debugout Time( 0 ), Program(), tcString

		m.lcString = m.tcString

		m.lcString = Strtran (m.tcString, '\', '\\')
		m.lcString = Strtran (m.lcString, '+', '\+')
		m.lcString = Strtran (m.lcString, '.', '\.')
		m.lcString = Strtran (m.lcString, '|', '\|')
		m.lcString = Strtran (m.lcString, '{', '\{')
		m.lcString = Strtran (m.lcString, '}', '\}')
		m.lcString = Strtran (m.lcString, '[', '\[')
		m.lcString = Strtran (m.lcString, ']', '\]')
		m.lcString = Strtran (m.lcString, '(', '\(')
		m.lcString = Strtran (m.lcString, ')', '\)')
		m.lcString = Strtran (m.lcString, '$', '\$')

		m.lcString = Strtran (m.lcString, '^', '\^')
		m.lcString = Strtran (m.lcString, ';', '\;')
		m.lcString = Strtran (m.lcString, '-', '\-')
		m.lcString = Strtran (m.lcString, '?', '\?')
		m.lcString = Strtran (m.lcString, '*', '\*')

		Return m.lcString

	Endproc && EscapeForRegex

	* TrimIt
	Procedure TrimIt
		Lparameters tcString

		* Debugout Time( 0 ), Program(), tcString

		Return Alltrim ( m.tcString, 1, ' ', This.cChr9 )

	Endproc && TrimIt

	* GetValue
	Procedure GetValue
		Lparameters tuExpression, tcAttributeName

		Local lcAtt As String, ;
			liWordIdx As Integer, ;
			lvRet

		* Debugout Time( 0 ), Program(), tuExpression, tcAttributeName

		If Vartype (m.tuExpression) == 'C' And Left (m.tuExpression, 2) == ' ('
			m.lvRet = Evaluate ( m.tuExpression )

		Else && Vartype (tuExpression) == 'C' And Left (tuExpression, 2) == ' ('
			m.lvRet = m.tuExpression

		Endif && Vartype (tuExpression) == 'C' And Left (tuExpression, 2) == ' ('

		*-- DAE 2012-12-01 Convierte el tipo de dato al esperado por el atributo.
		*-- 			 Converts the data type expected by the attribute.
		If ! Empty ( m.tcAttributeName )
			If Atc ( '__',  m.tcAttributeName ) > 0
				m.liWordIdx = Getwordcount ( m.tcAttributeName, '__' )
				m.lcAtt     = Getwordnum ( m.tcAttributeName, m.liWordIdx, '__' )

			Else && '__' $ tcAttributeName
				m.lcAtt = m.tcAttributeName

			Endif && '__' $ tcAttributeName

			m.lcAtt = '|' + Lower ( m.lcAtt ) + '|'
			Do Case
				Case Atc ( m.lcAtt, This.cNumericAttr ) > 0
					m.lvRet = Cast ( m.lvRet As N )

				Case Atc ( m.lcAtt, This.cLogicalAttr ) > 0
					m.lvRet = Cast ( m.lvRet As L )

				Case Atc ( m.lcAtt, This.cStringAttr ) > 0

			Endcase

		Endif && ! Empty ( tcAttributeName )

		Return m.lvRet

	Endproc && GetValue

	* PreProcessBodyMarkup
	Procedure PreProcessBodyMarkup

		Local lcField As String, ;
			lcImportHdr As String, ;
			lcMarkup As String, ;
			lnCount As Number, ;
			lnLastPos As Number, ;
			lnLenFieldDelimiterPattern As Number, ;
			lnPos As Number, ;
			lnX As Number, ;
			loField As Object

		* Debugout Time( 0 ), Program()

		*-- This method iterates over the fields in just the cBodyMarkup, and calls off the ParseField()
		*-- method to do a little trick before the real rendering happens... The purpose of this is that we want
		*-- to process any field definitions from the cBodyMarkup which are attempting to set RenderEngine class
		*-- properties by using identically named attributes.

		m.lnCount   = 1
		m.lnLastPos = 1

		*-- The attribute :import-header, if present, will pull in header from the GetHeaderMarkup()
		m.lcImportHdr = This.cAttributeNameDelimiterPattern + 'import-header'
		If ! Empty ( Atc ( m.lcImportHdr, This.cBodyMarkup ) )
			This.cBodyMarkup = Strtran ( This.cBodyMarkup, m.lcImportHdr, This.GetHeaderMarkup(), -1, -1, 1 )

		Endif && ! Empty( At( lcImportHdr , This.cBodyMarkup )

		m.lcMarkup                   = This.cBodyMarkup
		m.lnLenFieldDelimiterPattern = Len ( This.cFieldDelimiterPattern )

		*-- Read notes at the top of this mehtod to see what this loop does...
		*-- DAE 2012-12-01 Refactor de estilo del bucle para agregar claridad a la lectura.
		*-- Refactor loop style to add clarity to the reading.
		m.lnPos = Atc ( This.cFieldDelimiterPattern, m.lcMarkup, m.lnCount )
		Do While m.lnPos # 0
			m.lcField = Substr ( m.lcMarkup, m.lnLastPos, m.lnPos - m.lnLastPos )
			If ! Empty (m.lcField)
				m.loField = This.ParseField ( m.lcField )

			Endif && ! Empty (lcField)

			m.lnLastPos = m.lnPos + m.lnLenFieldDelimiterPattern
			m.lnCount   = m.lnCount + 1
			m.lnPos     = Atc ( This.cFieldDelimiterPattern, m.lcMarkup, m.lnCount )

		Enddo

	Endproc && PreProcessBodyMarkup

	* BuildFieldList
	Procedure BuildFieldList

		Local lcField As String, ;
			lcMarkup As String, ;
			lnCount As Number, ;
			lnLastPos As Number, ;
			lnLenFieldDelimiterPattern As Number, ;
			lnPos As Number, ;
			lnX As Number, ;
			loCntDummy As Object, ;
			loField As Object, ;
			loFormDummy As Object, ;
			loRenderOrder As 'Collection'

		* Debugout Time( 0 ), Program()

		*-- This method parses the cMarkup items to convert each item into an object in This.oFieldList collection.
		*-- To omit any property from processing, include it in the cSkipFields property.

		m.lcMarkup  = This.cMarkup
		m.lnCount   = 1
		m.lnLastPos = 1
		*-- DAE 2012-12-01 Objetos que capturan las propiedades que se van a modificar del formulario y del contenedor.
		*-- Objects that capture the properties that are to modify the form and container.
		m.loFormDummy   = Createobject ( 'Empty' )
		m.loRenderOrder = Createobject ('Collection')
		AddProperty ( m.loFormDummy, 'oRenderOrder', m.loRenderOrder )

		m.loCntDummy    = Createobject ( 'Empty' )
		m.loRenderOrder = Createobject ('Collection')
		AddProperty ( m.loCntDummy, 'oRenderOrder', m.loRenderOrder )

		m.lnLenFieldDelimiterPattern = Len ( This.cFieldDelimiterPattern )

		*-- DAE 2012-12-01 Refactor de estilo del bucle para agregar claridad a la lectura.
		*--				 Refactor loop style to add clarity to the reading.
		m.lnPos = Atc ( This.cFieldDelimiterPattern, m.lcMarkup, m.lnCount )
		Do While m.lnPos # 0
			m.lcField = Substr ( m.lcMarkup, m.lnLastPos, m.lnPos - m.lnLastPos )
			If ! Empty ( m.lcField )
				m.loField = This.ParseField ( m.lcField, @m.loFormDummy, @m.loCntDummy )
				This.oFieldList.Add ( m.loField )

			Endif && ! Empty( lcField )

			m.lnLastPos = m.lnPos + m.lnLenFieldDelimiterPattern
			m.lnCount   = m.lnCount + 1
			m.lnPos     = Atc ( This.cFieldDelimiterPattern, m.lcMarkup, m.lnCount )

		Enddo

		*-- DAE 2012-12-01 Aplica las propiedades al form y al contenedor.
		*-- 			 Applies the properties to form and the container.
		If Pemstatus ( This.oContainer, 'Parent', 5 ) And Type ( 'This.oContainer.Parent' ) == 'O'
			This.ApplyAttributes ( This.oContainer.Parent, m.loFormDummy )

		Endif && Pemstatus( This.oContainer, 'Parent', 5 ) And Vartype( This.oContainer.Parent ) == 'O'

		This.ApplyAttributes ( This.oContainer, m.loCntDummy )

	Endproc

	* CopyObject
	Procedure CopyObject
		Lparameters toObject

		Local laProps[1], ;
			lcPropName As String, ;
			lcType As String, ;
			lnI As Number, ;
			loNewObject As Object

		* Debugout Time( 0 ), Program(), toObject

		*-- From. http.//www.berezniker.com/content/pages/visual-foxpro/shallow-copy-object
		m.lcType = Vartype ( m.toObject )
		If m.lcType == 'X' Or m.lcType # 'O' Or Isnull ( m.toObject ) && or Type('toObject.Class') <> 'U' && For Empty class only and Not Null only
			Return Null

		Endif && lcType = 'X' Or lcType # 'O' Or Isnull ( toObject )

		m.loNewObject = Createobject ('Empty')

		For m.lnI = 1 To Amembers (laProps, m.toObject, 0)
			m.lcPropName = Lower (m.laProps[m.lnI])
			If m.lcPropName # 'parent'
				If Type ([toObject.] + m.lcPropName, 1) == 'A'
					AddProperty (m.loNewObject, m.lcPropName + '[1]', Null)
					= Acopy (toObject.&lcPropName, m.loNewObject.&lcPropName. )

				Else && Type ([toObject.] + lcPropName, 1) == 'A'
					AddProperty ( m.loNewObject, m.lcPropName, Getpem ( m.toObject, m.lcPropName ) )

				Endif && Type ([toObject.] + lcPropName, 1) == 'A'

			Endif && lcPropName # 'parent's

		Endfor

		Return m.loNewObject

	Endproc && CopyObject

	* RestoreData
	Procedure RestoreData

		Local lcControlSource As String, ;
			lcCursor As String, ;
			lcField As String, ;
			lcType As String, ;
			lnX As Number, ;
			luData As Variant

		* Debugout Time( 0 ), Program()

		*-- The original value of each control was saved into This.aBackup[] array so it nca be restored,
		*-- if this method is called by the consumer of this class.

		For m.lnX = 1 To Alen ( This.aBackup, 1)

			If ! Empty (This.aBackup[m.lnX, 1])
				m.lcControlSource = This.aBackup[m.lnX, 1]
				m.luData          = This.aBackup[m.lnX, 2]
				m.lcType          = This.aBackup[m.lnX, 3]

				If Atc ( m.lcType, 'Object Property' ) > 0
					Store m.luData To &lcControlSource.

				Else && lcType $ 'Object Property'
					m.lcField  = Justext (m.lcControlSource)
					m.lcCursor = Juststem (m.lcControlSource)
					Replace &lcField. With m.luData In &lcCursor.

				Endif && Atc ( lcType, 'Object Property' ) > 0

			Endif && ! Empty (This.aBackup[lnX, 1])

		Endfor

	Endproc && RestoreData

	* BackupData
	Procedure BackupData
		Lparameters tcControlSource, tuValue, tcType

		* Debugout Time( 0 ), Program(), tcControlSource, tuValue, tcType

		*-- Each time a control is added to the container, we will make a copy of the original value
		*-- so the value can be restored later if the consumer of this class calls RestoreData().

		Dimension This.aBackup[This.nControlCount, 3]

		This.aBackup[This.nControlCount, 1] = m.tcControlSource
		This.aBackup[This.nControlCount, 2] = m.tuValue
		This.aBackup[This.nControlCount, 3] = m.tcType

	Endproc && BackupData

	* AddError
	Procedure AddError
		Lparameters tcMessage, toField, toException

		Local loError As 'Empty'
		* Debugout Time( 0 ), Program(), tcMessage, toField, toException

		This.nErrorCount = This.nErrorCount + 1

		m.loError = Createobject ('Empty')
		AddProperty (m.loError, 'cMsg', m.tcMessage)
		AddProperty (m.loError, 'oField', This.CopyObject (m.toField))
		AddProperty (m.loError, 'oException', This.CopyObject (m.toException))
		This.oErrors.Add (m.loError)

	Endproc && AddError

	* GetErrorsAsString
	Procedure GetErrorsAsString

		Local lcString As String, ;
			loError As Object

		* Debugout Time( 0 ), Program()

		m.lcString = 'There were ' + Transform ( This.nErrorCount ) + ' Error(s) rendering the controls.' + This.cChr13 + This.cChr13

		For Each m.loError In This.oErrors FoxObject
			If Pemstatus ( m.loError, 'oField', 5 ) And ! Isnull ( m.loError.oField ) And Pemstatus ( m.loError.oField, 'ControlSource', 5 )
				m.lcString = m.lcString + '[' + m.loError.oField.ControlSource + ']. '

			Else && ! Isnull ( loError.oField ) And Pemstatus( loError.oField, 'ControlSource', 5 )
				m.lcString = m.lcString + '[]. '

			Endif && ! Isnull ( loError.oField ) And Pemstatus( loError.oField, 'ControlSource', 5 )

			m.lcString = m.lcString + m.loError.cMsg + This.cChr13

		Endfor

		Return m.lcString

	Endproc && GetErrorsAsString

	* GetHeaderMarkup
	Procedure GetHeaderMarkup

		Local lc1 As String, ;
			lc2 As String, ;
			lcMarkup As String

		* Debugout Time( 0 ), Program()

		m.lc1 = This.cAttributeNameDelimiterPattern
		m.lc2 = This.cAttributeValueDelimiterPattern

		TEXT To m.lcMarkup Noshow Textmerge

			<<lc1>>class 		<<lc2>> 'label'
			<<lc1>>render-if 	<<lc2>> (!Empty(this.cHeading))
			<<lc1>>caption 		<<lc2>> (this.cHeading)
			<<lc1>>name 		<<lc2>> 'lblHeading'
			<<lc1>>top 			<<lc2>> 10
			<<lc1>>left 		<<lc2>> 10
			<<lc1>>fontsize 	<<lc2>> (this.nHeadingFontSize)
			<<lc1>>fontbold 	<<lc2>> .f.
			<<lc1>>autosize 	<<lc2>> .t. |

			<<lc1>>class 		<<lc2>> 'DF_HorizontalLine'
			<<lc1>>render-if 	<<lc2>> (!Empty(this.cHeading))
			<<lc1>>margin-top 	<<lc2>> -10
			<<lc1>>left 		<<lc2>> 10

		ENDTEXT

		Return m.lcMarkup

	Endproc && GetHeaderMarkup

	* GetBodyMarkupForAll
	Procedure GetBodyMarkupForAll

		Local laFields[1], ;
			laObjFields[1], ;
			laProperties[1], ;
			laRelations[1, 5], ;
			lcBodyMarkup As String, ;
			lcCaption As String, ;
			lcClass As String, ;
			lcClassLibrary As String, ;
			lcComment As String, ;
			lcField As String, ;
			lcFontName As String, ;
			lcFontStyle As String, ;
			lcFormat As String, ;
			lcInputMask As String, ;
			lcKey As String, ;
			lcMember As String, ;
			lcPKTag As String, ;
			lcPkExp As String, ;
			lcPropertyFromObject As String, ;
			lcSkipFields As String, ;
			lcText As String, ;
			liIdx As Integer, ;
			liJdx As Integer, ;
			liKeyIndex As Integer, ;
			llHasCaption As Boolean, ;
			llInDB As Boolean, ;
			lnAverageCharacterWidth As Number, ;
			lnCnt As Number, ;
			lnExtraLen As Number, ;
			lnFontSize As Number, ;
			lnLen As Number, ;
			lnLength As Number, ;
			lnPos As Number, ;
			lnTagno As Number, ;
			lnWidth As Number, ;
			lnX As Number, ;
			loAux As Object, ;
			loColFields As 'Collection', ;
			loColFieldsAux As Collection, ;
			loField As 'Empty'

		#Define TM_AVECHARWIDTH     6

		* Debugout Time( 0 ), Program()

		*-- Loop through all properties on oDataObject and all fields on cALias to build BodyMarkup for all fields,
		*-- skipping any fields that are listed in cSksipFields

		m.lcSkipFields  = Upper ( This.cSkipFields )
		m.loColFields   = Createobject ( 'Collection' )
		This.cFontStyle = 'N'
		This.cFontName  = 'Segoe UI'
		This.nFontSize  = 9
		This.nExtraLen  = 1.7

		This.GetBodyMarkupForAllGenerateFieldsFromDataObject ( @m.loColFields, m.lcSkipFields )

		This.GetBodyMarkupForAllGenerateFieldsFromCursor ( @m.loColFields, m.lcSkipFields )

		This.nWidth           = 0
		This.nWidthTot        = 0
		This.nWidthCnt        = 0
		This.nCaptionWidth    = 0
		This.nCaptionWidthTot = 0
		This.nCaptionWidthCnt = 0

		m.lnCnt = m.loColFields.Count
		Dimension m.laObjFields[ m.lnCnt ]
		Store Null To m.laObjFields
		m.liIdx = 1
		For Each m.loField In m.loColFields FoxObject
			m.laObjFields[ m.liIdx ] = m.loField
			m.liIdx                  = m.liIdx + 1

		Next

		This.GetBodyMarkupForAllSortFields ( @m.laObjFields )

		m.lcBodyMarkup = This.GetBodyMarkupForAllGetMarkup ( @m.laObjFields )

		If This.nWidthCnt > 0
			This.nColumnWidth = (This.nWidthTot / This.nWidthCnt ) + (This.nCaptionWidthTot / This.nCaptionWidthCnt )

		Endif

		m.loColFields = Null

		Return m.lcBodyMarkup

	Endproc && GetBodyMarkupForAll

	* GetBodyMarkupForAllSortFields
	Procedure GetBodyMarkupForAllSortFields ( taObjFields )

		External Array taObjFields

		Local liIdx As Integer, ;
			liJdx As Integer, ;
			lnCnt As Number, ;
			loAux As Object

		m.lnCnt = Alen ( m.taObjFields )
		For m.liIdx = 1 To m.lnCnt
			For m.liJdx = 1 To m.lnCnt - m.liIdx
				If m.taObjFields[ m.liJdx ].nOrder > m.taObjFields[ m.liJdx + 1 ].nOrder
					m.loAux                      = m.taObjFields[ m.liJdx  ]
					m.taObjFields[ m.liJdx  ]    = m.taObjFields[ m.liJdx + 1]
					m.taObjFields[ m.liJdx + 1 ] = m.loAux

				Endif && taObjFields[ liJdx ].nOrder > taObjFields[ liJdx + 1 ].nOrder

			Next

		Next

	Endproc && GetBodyMarkupForAllSortFields

	* GetBodyMarkupForAllGetMarkup
	Procedure GetBodyMarkupForAllGetMarkup ( taObjFields )

		External Array taObjFields

		Local lcBodyMarkup As String, ;
			lcMarkupAux As String, ;
			liKdx As Integer, ;
			lnCnt As Number, ;
			loField As Object

		m.lnCnt        = Alen ( m.taObjFields )
		m.lcBodyMarkup = ''
		For m.liKdx = 1 To m.lnCnt
			m.loField = m.taObjFields[ m.liKdx ]
			This.HookGetBodyMarkupForAllCustomiseField ( m.loField )
			m.lcMarkupAux  = This.GetBodyMarkupForAllGetMarkupField ( m.loField )
			m.lcBodyMarkup = m.lcBodyMarkup + m.lcMarkupAux

		Endfor

		Return m.lcBodyMarkup

	Endproc && GetBodyMarkupForAllGetMarkup

	* GetBodyMarkupForAllGetMarkupField
	Function GetBodyMarkupForAllGetMarkupField ( toField ) As String

		Local lcBodyMarkup As String, ;
			lcCaption As String, ;
			lcClass As String, ;
			lcClassLibrary As String, ;
			lcComment As String, ;
			lcFormat As String, ;
			lcInputMask As String, ;
			lcMember As String, ;
			lcText As String, ;
			liIdx As Integer, ;
			llHasCaption As Boolean, ;
			lnAverageCharacterWidth As Number, ;
			lnLen As Number, ;
			lnLength As Number, ;
			lnWidth As Number

		m.lcBodyMarkup = m.toField.cName + ' ' + This.cAttributeNameDelimiterPattern + 'lForceReadOnly ' + This.cAttributeValueDelimiterPattern + Transform ( m.toField.lForceReadOnly )
		If m.toField.lReadOnly
			m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'ReadOnly ' + This.cAttributeValueDelimiterPattern + ' .T. '

		Endif && loField.lReadOnly

		m.lcClass = m.toField.cDisplayClass
		If ! Empty (  m.lcClass )
			m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'class ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcClass

		Else && ! Empty (  lcClass )
			If Atc ( m.toField.cDataType, 'DT' ) > 0 And ! Empty ( This.cDatepickerClass )
				m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'class ' + This.cAttributeValueDelimiterPattern + ' ' + This.cDatepickerClass

			Endif && Atc( toField.cDataType, 'DT' ) > 0 And ! Empty( This.cDatepickerClass )

		Endif && ! Empty( lcClass )

		m.lcClassLibrary = m.toField.cDisplayClassLibrary
		If ! Empty ( m.lcClassLibrary )
			m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'ClassLibrary ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcClassLibrary

		Else && ! Empty ( lcClassLibrary )
			If Atc ( m.toField.cDataType, 'DT' ) > 0 And ! Empty ( This.cDatepickerClassLib )
				m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'class ' + This.cAttributeValueDelimiterPattern + ' ' + This.cDatepickerClassLib

			Endif && Atc( loField.cDataType, 'DT' ) > 0 And ! Empty( This.cDatepickerClassLib )

		Endif && ! Empty( lcClassLibrary )

		m.lcCaption = m.toField.cCaption
		If ! Empty ( m.lcCaption )
			m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'Caption ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcCaption

		Endif && ! Empty( lcCaption )

		m.lcFormat = m.toField.cFormat
		If ! Empty ( m.lcFormat )
			m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'Format ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcFormat

		Endif && ! Empty( lcFormat )

		m.lcInputMask = m.toField.cInputMask
		If ! Empty ( m.lcInputMask )
			m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'InputMask ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcInputMask

		Endif && ! Empty( lcInputMask )

		* lcComment = loField.cComment
		m.lcComment = Iif ( Vartype ( m.toField.cComment ) == 'C' And ! Empty ( m.toField.cComment ), m.toField.cComment, m.lcCaption )
		If Vartype ( m.lcComment ) == 'C' And ! Empty ( m.lcComment )
			m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'ToolTipText ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcComment
			m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'cCueText ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcComment

		Endif && ! Empty( cComment )

		m.llHasCaption = ! Empty ( m.lcCaption )
		m.lcComment    = m.toField.cComment
		m.lnLength     = m.toField.nLength

		Do Case
			Case ! Empty ( m.lcInputMask )
				m.lcText = m.lcInputMask
				m.lnLen  = Len ( m.lcInputMask )

			Case m.llHasCaption
				m.lcText = m.lcCaption
				m.lnLen  = Len ( m.lcCaption )

			Otherwise
				If ! Isnull ( m.lnLength )
					m.lcText = Replicate ( 'M', m.lnLength )
					m.lnLen  = m.lnLength

				Endif && ! Isnull ( m.lnLength )

		Endcase

		* Fix en los casos que el Caption es mas chico que el tama�o del campo.
		If m.lnLen < m.lnLength
			m.lcText = Replicate ( 'M', m.lnLength )
			m.lnLen  = m.lnLength

		Endif && lnLen < lnLength

		m.lcText = Replicate ( 'M', m.lnLen )

		If ! Empty ( m.lcText ) And m.toField.cDataType # 'M'
			m.lnLen                   = Max ( Txtwidth ( m.lcText, This.cFontName, This.nFontSize, This.cFontStyle ), m.lnLen )
			m.lnAverageCharacterWidth = Fontmetric ( TM_AVECHARWIDTH, This.cFontName, This.nFontSize, This.cFontStyle )
			AddProperty ( m.toField, 'nWidth', Ceiling ( m.lnAverageCharacterWidth * ( m.lnLen + This.nExtraLen ) ) )
			m.lnWidth      = Iif ( m.toField.nWidth > _Screen.Width, 150, m.toField.nWidth )
			m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'Width ' + This.cAttributeValueDelimiterPattern + ' ' + Transform ( m.lnWidth )

			If m.llHasCaption
				m.lcText = m.lcCaption
				m.lnLen  = Len ( m.lcCaption )

				m.lnLen                   = Max ( Txtwidth ( m.lcText, This.cFontName, This.nFontSize, This.cFontStyle ), m.lnLen )
				m.lnAverageCharacterWidth = Fontmetric ( TM_AVECHARWIDTH, This.cFontName, This.nFontSize, This.cFontStyle )
				AddProperty ( m.toField, 'nCaptionWidth', Ceiling ( m.lnAverageCharacterWidth * ( m.lnLen + This.nExtraLen ) ) )

			Endif

		Else && ! Empty ( lcText ) And loField.cDataType # 'M'
			If ! Isnull ( m.toField.nWidth )
				m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'Width ' + This.cAttributeValueDelimiterPattern + ' ' + Transform ( m.toField.nWidth )

			Endif && ! Isnull ( loField.nWidth )

		Endif && ! Empty ( lcText ) And loField.cDataType # 'M'

		If ! Isnull ( m.toField.nWidth )
			If m.toField.nWidth > This.nWidth
				This.nWidth = m.toField.nWidth

			Endif

			This.nWidthTot = This.nWidthTot + m.toField.nWidth
			This.nWidthCnt = This.nWidthCnt + 1

		Endif

		* TODO: Calculate caption width.
		If ! Isnull ( m.toField.nCaptionWidth )
			If m.toField.nCaptionWidth > This.nCaptionWidth
				This.nCaptionWidth = m.toField.nCaptionWidth

			Endif && loField.nCaptionWidth > this.nCaptionWidth

			This.nCaptionWidthTot = This.nCaptionWidthTot + m.toField.nCaptionWidth
			This.nCaptionWidthCnt = This.nCaptionWidthCnt + 1

		Endif && ! Isnull ( loField.nCaptionWidth )

		For m.liIdx = 1 To Amembers ( laMembers, m.toField )
			TEXT To m.lcMember Textmerge Noshow Pretex 15
				|<<laMembers[ liIdx ]>>|
			ENDTEXT

			If Atc ( m.lcMember, This.cDefaultFieldMembers ) == 0
				m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + laMembers[ m.liIdx ] + ' ' + This.cAttributeValueDelimiterPattern + ' ' + Transform ( Getpem ( m.toField, laMembers[ m.liIdx ] ) )

			Endif && Atc ( m.lcMember, This.cDefaultFieldMembers ) == 0

		Next

		m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cFieldDelimiterPattern

		Return m.lcBodyMarkup

	Endfunc && GetBodyMarkupForAllGetMarkupField

	* GetBodyMarkupForAllGenerateFieldsFromDataObject
	Procedure GetBodyMarkupForAllGenerateFieldsFromDataObject ( toColFields, tcSkipFields  )

		Local lcKey As String, ;
			lcPropertyFromObject As String, ;
			lnX As Number, ;
			loField As 'Empty'

		If ! Isnull ( This.oDataObject )
			For m.lnX = 1 To Amembers ( laProperties, This.oDataObject )
				m.lcPropertyFromObject = laProperties[ m.lnX ]
				If Atc ( m.lcPropertyFromObject, m.tcSkipFields ) == 0
					m.lcKey   = Upper ( m.lcPropertyFromObject )
					m.loField = Createobject ( 'Empty' )
					AddProperty ( m.loField, 'cName', m.lcPropertyFromObject )
					AddProperty ( m.loField, 'lReadOnly', Iif ( m.lcKey == 'ID', .T., .F. ) )
					AddProperty ( m.loField, 'lForceReadOnly', Iif ( m.lcKey == 'ID', .T., .F. ) )
					AddProperty ( m.loField, 'cDisplayClass', '' )
					AddProperty ( m.loField, 'cDisplayClassLibrary', '' )
					AddProperty ( m.loField, 'cCaption', m.lcPropertyFromObject )
					AddProperty ( m.loField, 'cFormat', '' )
					AddProperty ( m.loField, 'cInputMask', '' )
					AddProperty ( m.loField, 'nWidth', Null )
					AddProperty ( m.loField, 'nCaptionWidth', Null )
					AddProperty ( m.loField, 'cComment', Null )
					AddProperty ( m.loField, 'nLength', Null )
					AddProperty ( m.loField, 'cDataType', Vartype ( Getpem ( This.oDataObject, m.lcPropertyFromObject ) ) )
					AddProperty ( m.loField, 'nOrder', m.lnX )
					m.toColFields.Add ( m.loField, m.lcKey )

				Endif && Atc ( lcPropertyFromObject, tcSkipFields ) == 0

			Endfor

		Endif && ! Isnull ( This.oDataObject )

	Endproc && GetBodyMarkupForAllGenerateFieldsFromDataObject

	* GetBodyMarkupForAllGenerateFieldsFromCursor
	Procedure GetBodyMarkupForAllGenerateFieldsFromCursor ( toColFields, tcSkipFields )

		Local laRelations[1], ;
			lcCaption As String, ;
			lcClass As String, ;
			lcClassLibrary As String, ;
			lcComment As String, ;
			lcField As String, ;
			lcFormat As String, ;
			lcInputMask As String, ;
			lcKey As String, ;
			lcPKTag As String, ;
			lcPkExp As String, ;
			lcPropertyFromObject As String, ;
			liKeyIndex As Integer, ;
			llInDB As Boolean, ;
			lnPos As Number, ;
			lnTagno As Number, ;
			lnX As Number, ;
			loField As 'Empty'

		If ! Empty ( This.cAlias )
			m.llInDB = ! Empty ( CursorGetProp ( 'Database', This.cAlias ) )

			m.lcPkExp = ''
			If m.llInDB And ! Empty ( Dbc() )
				Adbobjects ( laRelations, 'Relation' )
				* Formato
				* [ 1 ] Origen
				* [ 2 ] Destino
				* [ 3 ] FK (Origen)
				* [ 4 ] PK (Destino)
				* [ 5 ] "Vacio"

				m.lcPKTag = DBGetProp ( This.cAlias, 'TABLE', 'PrimaryKey' )

				If ! Empty ( m.lcPKTag )
					Select (This.cAlias)
					m.lnTagno = Tagno ( m.lcPKTag )
					If ! Empty ( m.lnTagno )
						m.lcPkExp = Lower ( Sys ( 14, m.lnTagno ) )

					Endif &&  ! Empty( lnTagno )

				Endif && ! Empty ( lcPKTag )

			Else && llInDB
				Dimension m.laRelations[ 1, 5 ]

			Endif && llInDB

			For m.lnX = 1 To Afields ( laFields, This.cAlias )
				m.lcPropertyFromObject = laFields[ m.lnX, 1 ]

				If Atc ( m.lcPropertyFromObject, m.tcSkipFields ) == 0

					m.lcKey      = Upper ( m.lcPropertyFromObject )
					m.liKeyIndex = m.toColFields.GetKey ( m.lcKey )
					If Empty ( m.liKeyIndex )
						m.loField = Createobject ( 'Empty' )
						AddProperty ( m.loField, 'cName', m.lcPropertyFromObject )
						AddProperty ( m.loField, 'lReadOnly', Iif ( m.lcKey == 'ID', .T., .F. ) )
						AddProperty ( m.loField, 'lForceReadOnly', Iif ( m.lcKey == 'ID', .T., .F. ) )
						AddProperty ( m.loField, 'cDisplayClass', '' )
						AddProperty ( m.loField, 'cDisplayClassLibrary', '' )
						AddProperty ( m.loField, 'cCaption', m.lcPropertyFromObject )
						AddProperty ( m.loField, 'cFormat', '' )
						AddProperty ( m.loField, 'cInputMask', '' )
						AddProperty ( m.loField, 'nWidth', Null )
						AddProperty ( m.loField, 'nCaptionWidth', Null )
						AddProperty ( m.loField, 'cComment', Null )
						* AddProperty ( loField, 'nLength', laFields[ lnX, 3 ] ) && + laFields[ lnX, 4 ]
						m.toColFields.Add ( m.loField, m.lcKey )

					Else
						m.loField = m.toColFields.Item[ m.liKeyIndex ]

					Endif

					AddProperty ( m.loField, 'nLength', laFields[ m.lnX, 3 ] ) && + laFields[ lnX, 4 ]
					AddProperty ( m.loField, 'lReadOnly', Iif ( m.lcKey == 'ID', .T., .F. ) )
					AddProperty ( m.loField, 'nOrder', m.lnX )

					If m.llInDB && And ! Empty( Dbc() )

						m.lcField      = This.cAlias + '.' + m.lcPropertyFromObject
						If Lower ( m.lcPropertyFromObject ) == m.lcPkExp
							AddProperty ( m.loField, 'lReadOnly', .T. )

						Endif

						m.lcClass = DBGetProp ( m.lcField, 'FIELD', 'DisplayClass' )
						If ! Empty ( m.lcClass )
							AddProperty ( m.loField, 'cDisplayClass', m.lcClass )

						Else
							m.lnPos = Ascan ( m.laRelations, '', 1, 1, 2 )
							If m.lnPos # 0

							Endif

						Endif && ! Empty( lcClass )

						m.lcClassLibrary = DBGetProp ( m.lcField, 'FIELD', 'DisplayClassLibrary' )
						If ! Empty ( m.lcClassLibrary )
							AddProperty ( m.loField, 'cDisplayClassLibrary', m.lcClassLibrary )

						Endif && ! Empty( lcClassLibrary )

						m.lcCaption = DBGetProp ( m.lcField, 'FIELD', 'Caption' )
						If ! Empty ( m.lcCaption )
							AddProperty ( m.loField, 'cCaption', m.lcCaption )

						Endif && ! Empty( lcCaption )

						m.lcFormat = DBGetProp ( m.lcField, 'FIELD', 'Format' )
						If ! Empty ( m.lcFormat )
							AddProperty ( m.loField, 'cFormat', m.lcFormat )

						Endif && ! Empty( lcFormat )

						m.lcInputMask = DBGetProp ( m.lcField, 'FIELD', 'InputMask' )
						If ! Empty ( m.lcInputMask )
							AddProperty ( m.loField, 'cInputMask', m.lcInputMask )

						Endif && ! Empty( lcInputMask )

						m.lcComment = DBGetProp ( m.lcField, 'FIELD', 'Comment' )
						If ! Empty ( m.lcComment  )
							AddProperty ( m.loField, 'cComment', m.lcComment )

						Endif && ! Empty( lcInputMask )

					Else && llInDB
						AddProperty ( m.loField, 'cCaption', m.lcPropertyFromObject )

					Endif && llInDB

					AddProperty ( m.loField, 'cDataType', laFields[ m.lnX, 2 ] )

				Endif Atc ( m.lcPropertyFromObject, m.tcSkipFields ) == 0

			Endfor

		Endif && ! Empty (This.cAlias)

	Endproc && GetBodyMarkupForAllGenerateFieldsFromCursor

	*!*		* GetBodyMarkupForAll
	*!*		* TODO: Modularizar
	*!*		Procedure GetBodyMarkupForAll

	*!*			Local laFields[1], ;
	*!*				laObjFields[1], ;
	*!*				laProperties[1], ;
	*!*				laRelations[1, 5], ;
	*!*				lcBodyMarkup As String, ;
	*!*				lcCaption As String, ;
	*!*				lcClass As String, ;
	*!*				lcClassLibrary As String, ;
	*!*				lcComment As String, ;
	*!*				lcField As String, ;
	*!*				lcFontName As String, ;
	*!*				lcFontStyle As String, ;
	*!*				lcFormat As String, ;
	*!*				lcInputMask As String, ;
	*!*				lcKey As String, ;
	*!*				lcMember As String, ;
	*!*				lcPKTag As String, ;
	*!*				lcPkExp As String, ;
	*!*				lcPropertyFromObject As String, ;
	*!*				lcSkipFields As String, ;
	*!*				lcText As String, ;
	*!*				liIdx As Integer, ;
	*!*				liJdx As Integer, ;
	*!*				liKdx As Integer, ;
	*!*				liKeyIndex As Integer, ;
	*!*				llHasCaption As Boolean, ;
	*!*				llInDB As Boolean, ;
	*!*				lnAverageCharacterWidth As Number, ;
	*!*				lnCaptionWidth As Number, ;
	*!*				lnCaptionWidthCnt As Number, ;
	*!*				lnCaptionWidthTot As Number, ;
	*!*				lnCnt As Number, ;
	*!*				lnExtraLen As Number, ;
	*!*				lnFontSize As Number, ;
	*!*				lnLen As Number, ;
	*!*				lnLength As Number, ;
	*!*				lnPos As Number, ;
	*!*				lnTagno As Number, ;
	*!*				lnWidth As Number, ;
	*!*				lnWidthCnt As Number, ;
	*!*				lnWidthTot As Number, ;
	*!*				lnX As Number, ;
	*!*				loAux As Object, ;
	*!*				loColFields As 'Collection', ;
	*!*				loColFieldsAux As Collection, ;
	*!*				loField As 'Empty'

	*!*			#Define TM_AVECHARWIDTH     6

	*!*			* Debugout Time( 0 ), Program()

	*!*			*-- Loop through all properties on oDataObject and all fields on cALias to build BodyMarkup for all fields,
	*!*			*-- skipping any fields that are listed in cSksipFields

	*!*			m.lcBodyMarkup = ''
	*!*			m.lcSkipFields = Upper ( This.cSkipFields )
	*!*			m.loColFields  = Createobject ( 'Collection' )
	*!*			m.lcFontStyle  = 'N'
	*!*			m.lcFontName   = 'Segoe UI'
	*!*			m.lnFontSize   = 9
	*!*			m.lnExtraLen   = 1.7

	*!*			If ! Isnull ( This.oDataObject )
	*!*				For m.lnX = 1 To Amembers ( laProperties, This.oDataObject )
	*!*					m.lcPropertyFromObject = m.laProperties[ m.lnX ]
	*!*					If ! Atc ( m.lcPropertyFromObject, m.lcSkipFields ) > 0
	*!*						m.lcKey   = Upper ( m.lcPropertyFromObject )
	*!*						m.loField = Createobject ( 'Empty' )
	*!*						AddProperty ( m.loField, 'cName', m.lcPropertyFromObject )
	*!*						AddProperty ( m.loField, 'lReadOnly', Iif ( m.lcKey == 'ID', .T., .F. ) )
	*!*						AddProperty ( m.loField, 'lForceReadOnly', Iif ( m.lcKey == 'ID', .T., .F. ) )
	*!*						AddProperty ( m.loField, 'cDisplayClass', '' )
	*!*						AddProperty ( m.loField, 'cDisplayClassLibrary', '' )
	*!*						AddProperty ( m.loField, 'cCaption', m.lcPropertyFromObject )
	*!*						AddProperty ( m.loField, 'cFormat', '' )
	*!*						AddProperty ( m.loField, 'cInputMask', '' )
	*!*						AddProperty ( m.loField, 'nWidth', Null )
	*!*						AddProperty ( m.loField, 'nCaptionWidth', Null )
	*!*						AddProperty ( m.loField, 'cComment', Null )
	*!*						AddProperty ( m.loField, 'nLength', Null )
	*!*						AddProperty ( m.loField, 'cDataType', Vartype ( Getpem ( This.oDataObject, m.lcPropertyFromObject ) ) )
	*!*						AddProperty ( m.loField, 'nOrder', m.lnX )
	*!*						m.loColFields.Add ( m.loField, m.lcKey )

	*!*					Endif && ! Atc ( lcPropertyFromObject, lcSkipFields ) > 0

	*!*				Endfor

	*!*			Endif && ! Isnull ( This.oDataObject )

	*!*			If ! Empty ( This.cAlias )

	*!*				m.llInDB = ! Empty ( CursorGetProp ( 'Database', This.cAlias ) )

	*!*				m.lcPkExp = ''
	*!*				If m.llInDB And ! Empty ( Dbc() )
	*!*					Adbobjects ( laRelations, 'Relation' )
	*!*					* Formato
	*!*					* [ 1 ] Origen
	*!*					* [ 2 ] Destino
	*!*					* [ 3 ] FK (Origen)
	*!*					* [ 4 ] PK (Destino)
	*!*					* [ 5 ] "Vacio"

	*!*					m.lcPKTag = DBGetProp ( This.cAlias, 'TABLE', 'PrimaryKey' )

	*!*					If ! Empty ( m.lcPKTag )
	*!*						Select (This.cAlias)
	*!*						m.lnTagno = Tagno ( m.lcPKTag )
	*!*						If ! Empty ( m.lnTagno )
	*!*							m.lcPkExp = Lower ( Sys ( 14, m.lnTagno ) )

	*!*						Endif &&  ! Empty( lnTagno )

	*!*					Endif && ! Empty ( lcPKTag )

	*!*				Else && llInDB
	*!*					Dimension m.laRelations[ 1, 5 ]

	*!*				Endif && llInDB


	*!*				For m.lnX = 1 To Afields ( laFields, This.cAlias )
	*!*					m.lcPropertyFromObject = m.laFields[ m.lnX, 1 ]

	*!*					If Atc ( m.lcPropertyFromObject, m.lcSkipFields ) == 0

	*!*						m.lcKey      = Upper ( m.lcPropertyFromObject )
	*!*						m.liKeyIndex = m.loColFields.GetKey ( m.lcKey )
	*!*						If Empty ( m.liKeyIndex )
	*!*							m.loField = Createobject ( 'Empty' )
	*!*							AddProperty ( m.loField, 'cName', m.lcPropertyFromObject )
	*!*							AddProperty ( m.loField, 'lReadOnly', Iif ( m.lcKey == 'ID', .T., .F. ) )
	*!*							AddProperty ( m.loField, 'lForceReadOnly', Iif ( m.lcKey == 'ID', .T., .F. ) )
	*!*							AddProperty ( m.loField, 'cDisplayClass', '' )
	*!*							AddProperty ( m.loField, 'cDisplayClassLibrary', '' )
	*!*							AddProperty ( m.loField, 'cCaption', m.lcPropertyFromObject )
	*!*							AddProperty ( m.loField, 'cFormat', '' )
	*!*							AddProperty ( m.loField, 'cInputMask', '' )
	*!*							AddProperty ( m.loField, 'nWidth', Null )
	*!*							AddProperty ( m.loField, 'nCaptionWidth', Null )
	*!*							AddProperty ( m.loField, 'cComment', Null )
	*!*							* AddProperty ( loField, 'nLength', laFields[ lnX, 3 ] ) && + laFields[ lnX, 4 ]
	*!*							m.loColFields.Add ( m.loField, m.lcKey )

	*!*						Else
	*!*							m.loField = m.loColFields.Item[ m.liKeyIndex ]

	*!*						Endif

	*!*						AddProperty ( m.loField, 'nLength', m.laFields[ m.lnX, 3 ] ) && + laFields[ lnX, 4 ]
	*!*						AddProperty ( m.loField, 'lReadOnly', Iif ( m.lcKey == 'ID', .T., .F. ) )
	*!*						AddProperty ( m.loField, 'nOrder', m.lnX )

	*!*						If m.llInDB && And ! Empty( Dbc() )

	*!*							m.lcField      = This.cAlias + '.' + m.lcPropertyFromObject
	*!*							If Lower ( m.lcPropertyFromObject ) == m.lcPkExp
	*!*								AddProperty ( m.loField, 'lReadOnly', .T. )

	*!*							Endif

	*!*							m.lcClass = DBGetProp ( m.lcField, 'FIELD', 'DisplayClass' )
	*!*							If ! Empty ( m.lcClass )
	*!*								AddProperty ( m.loField, 'cDisplayClass', m.lcClass )

	*!*							Else
	*!*								m.lnPos = Ascan ( m.laRelations, '', 1, 1, 2 )
	*!*								If m.lnPos # 0

	*!*								Endif

	*!*							Endif && ! Empty( lcClass )

	*!*							m.lcClassLibrary = DBGetProp ( m.lcField, 'FIELD', 'DisplayClassLibrary' )
	*!*							If ! Empty ( m.lcClassLibrary )
	*!*								AddProperty ( m.loField, 'cDisplayClassLibrary', m.lcClassLibrary )

	*!*							Endif && ! Empty( lcClassLibrary )

	*!*							m.lcCaption = DBGetProp ( m.lcField, 'FIELD', 'Caption' )
	*!*							If ! Empty ( m.lcCaption )
	*!*								AddProperty ( m.loField, 'cCaption', m.lcCaption )

	*!*							Endif && ! Empty( lcCaption )

	*!*							m.lcFormat = DBGetProp ( m.lcField, 'FIELD', 'Format' )
	*!*							If ! Empty ( m.lcFormat )
	*!*								AddProperty ( m.loField, 'cFormat', m.lcFormat )

	*!*							Endif && ! Empty( lcFormat )

	*!*							m.lcInputMask = DBGetProp ( m.lcField, 'FIELD', 'InputMask' )
	*!*							If ! Empty ( m.lcInputMask )
	*!*								AddProperty ( m.loField, 'cInputMask', m.lcInputMask )

	*!*							Endif && ! Empty( lcInputMask )

	*!*							m.lcComment = DBGetProp ( m.lcField, 'FIELD', 'Comment' )
	*!*							If ! Empty ( m.lcComment  )
	*!*								AddProperty ( m.loField, 'cComment', m.lcComment )

	*!*							Endif && ! Empty( lcInputMask )

	*!*						Else && llInDB
	*!*							AddProperty ( m.loField, 'cCaption', m.lcPropertyFromObject )

	*!*						Endif && llInDB

	*!*						AddProperty ( m.loField, 'cDataType', m.laFields[ m.lnX, 2 ] )

	*!*					Endif Atc ( m.lcPropertyFromObject, m.lcSkipFields ) == 0

	*!*				Endfor

	*!*			Endif && ! Empty (This.cAlias)

	*!*			m.lnWidth           = 0
	*!*			m.lnWidthTot        = 0
	*!*			m.lnWidthCnt        = 0
	*!*			m.lnCaptionWidth    = 0
	*!*			m.lnCaptionWidthTot = 0
	*!*			m.lnCaptionWidthCnt = 0

	*!*			m.lnCnt = m.loColFields.Count
	*!*			Dimension m.laObjFields[ m.lnCnt ]
	*!*			Store Null To m.laObjFields
	*!*			m.liIdx = 1
	*!*			For Each m.loField In m.loColFields FoxObject
	*!*				m.laObjFields[ m.liIdx ] = m.loField
	*!*				m.liIdx                  = m.liIdx + 1

	*!*			Next

	*!*			For m.liIdx = 1 To m.lnCnt
	*!*				For m.liJdx = 1 To m.lnCnt - m.liIdx
	*!*					If m.laObjFields[ m.liJdx ].nOrder > m.laObjFields[ m.liJdx + 1 ].nOrder
	*!*						m.loAux                      = m.laObjFields[ m.liJdx  ]
	*!*						m.laObjFields[ m.liJdx  ]    = m.laObjFields[ m.liJdx + 1]
	*!*						m.laObjFields[ m.liJdx + 1 ] = m.loAux

	*!*					Endif

	*!*				Next

	*!*			Next

	*!*			* loColFieldsAux.KeySort = 2
	*!*			* For Each loField In loColFields FoxObject
	*!*			For m.liKdx = 1 To m.lnCnt
	*!*				m.loField = m.laObjFields[ m.liKdx ]
	*!*				This.HookGetBodyMarkupForAllCustomiseField ( m.loField )

	*!*				m.lcBodyMarkup = m.lcBodyMarkup + m.loField.cName
	*!*				m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'lForceReadOnly ' + This.cAttributeValueDelimiterPattern + Transform ( m.loField.lForceReadOnly )
	*!*				If m.loField.lReadOnly
	*!*					m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'ReadOnly ' + This.cAttributeValueDelimiterPattern + ' .T. '

	*!*				Endif && loField.lReadOnly

	*!*				m.lcClass = m.loField.cDisplayClass
	*!*				If ! Empty (  m.lcClass )
	*!*					m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'class ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcClass

	*!*				Else && ! Empty (  lcClass )
	*!*					If Atc ( m.loField.cDataType, 'DT' ) > 0 And ! Empty ( This.cDatepickerClass )
	*!*						m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'class ' + This.cAttributeValueDelimiterPattern + ' ' + This.cDatepickerClass

	*!*					Endif && Atc( loField.cDataType, 'DT' ) > 0 And ! Empty( This.cDatepickerClass )

	*!*				Endif && ! Empty( lcClass )

	*!*				m.lcClassLibrary = m.loField.cDisplayClassLibrary
	*!*				If ! Empty ( m.lcClassLibrary )
	*!*					m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'ClassLibrary ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcClassLibrary

	*!*				Else && ! Empty ( lcClassLibrary )
	*!*					If Atc ( m.loField.cDataType, 'DT' ) > 0 And ! Empty ( This.cDatepickerClassLib )
	*!*						m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'class ' + This.cAttributeValueDelimiterPattern + ' ' + This.cDatepickerClassLib

	*!*					Endif && Atc( loField.cDataType, 'DT' ) > 0 And ! Empty( This.cDatepickerClassLib )

	*!*				Endif && ! Empty( lcClassLibrary )

	*!*				m.lcCaption = m.loField.cCaption
	*!*				If ! Empty ( m.lcCaption )
	*!*					m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'Caption ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcCaption

	*!*				Endif && ! Empty( lcCaption )

	*!*				m.lcFormat = m.loField.cFormat
	*!*				If ! Empty ( m.lcFormat )
	*!*					m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'Format ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcFormat

	*!*				Endif && ! Empty( lcFormat )

	*!*				m.lcInputMask = m.loField.cInputMask
	*!*				If ! Empty ( m.lcInputMask )
	*!*					m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'InputMask ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcInputMask

	*!*				Endif && ! Empty( lcInputMask )

	*!*				* lcComment = loField.cComment
	*!*				m.lcComment = Iif ( Vartype ( m.loField.cComment ) == 'C' And ! Empty ( m.loField.cComment ), m.loField.cComment, m.lcCaption )
	*!*				If Vartype ( m.lcComment ) == 'C' And ! Empty ( m.lcComment )
	*!*					m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'ToolTipText ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcComment
	*!*					m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'cCueText ' + This.cAttributeValueDelimiterPattern + ' ' + m.lcComment

	*!*				Endif && ! Empty( cComment )

	*!*				m.llHasCaption = ! Empty ( m.lcCaption )
	*!*				m.lcComment    = m.loField.cComment
	*!*				m.lnLength     = m.loField.nLength

	*!*				Do Case
	*!*					Case ! Empty ( m.lcInputMask )
	*!*						m.lcText = m.lcInputMask
	*!*						m.lnLen  = Len ( m.lcInputMask )

	*!*					Case m.llHasCaption
	*!*						m.lcText = m.lcCaption
	*!*						m.lnLen  = Len ( m.lcCaption )

	*!*					Otherwise
	*!*						If ! Isnull ( m.lnLength )
	*!*							m.lcText = Replicate ( 'M', m.lnLength )
	*!*							m.lnLen  = m.lnLength

	*!*						Endif

	*!*				Endcase

	*!*				* Fix en los casos que el Caption es mas chico que el tama�o del campo.
	*!*				If m.lnLen < m.lnLength
	*!*					m.lcText = Replicate ( 'M', m.lnLength )
	*!*					m.lnLen  = m.lnLength

	*!*				Endif && lnLen < lnLength

	*!*				m.lcText = Replicate ( 'M', m.lnLen )

	*!*				If ! Empty ( m.lcText ) And m.loField.cDataType # 'M'
	*!*					m.lnLen                   = Max ( Txtwidth ( m.lcText, m.lcFontName, m.lnFontSize, m.lcFontStyle ), m.lnLen )
	*!*					m.lnAverageCharacterWidth = Fontmetric ( TM_AVECHARWIDTH, m.lcFontName, m.lnFontSize, m.lcFontStyle )
	*!*					* lnWidth = Ceiling( lnAverageCharacterWidth * ( lnLen + lnExtraLen ) )
	*!*					AddProperty ( m.loField, 'nWidth', Ceiling ( m.lnAverageCharacterWidth * ( m.lnLen + m.lnExtraLen ) ) )
	*!*					m.lnWidth      = Iif ( m.loField.nWidth > _Screen.Width, 150, m.loField.nWidth )
	*!*					m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'Width ' + This.cAttributeValueDelimiterPattern + ' ' + Transform ( m.lnWidth )

	*!*					If m.llHasCaption
	*!*						m.lcText = m.lcCaption
	*!*						m.lnLen  = Len ( m.lcCaption )

	*!*						m.lnLen                   = Max ( Txtwidth ( m.lcText, m.lcFontName, m.lnFontSize, m.lcFontStyle ), m.lnLen )
	*!*						m.lnAverageCharacterWidth = Fontmetric ( TM_AVECHARWIDTH, m.lcFontName, m.lnFontSize, m.lcFontStyle )
	*!*						AddProperty ( m.loField, 'nCaptionWidth', Ceiling ( m.lnAverageCharacterWidth * ( m.lnLen + m.lnExtraLen ) ) )

	*!*					Endif

	*!*				Else && ! Empty ( lcText ) And loField.cDataType # 'M'
	*!*					If ! Isnull ( m.loField.nWidth )
	*!*						m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + 'Width ' + This.cAttributeValueDelimiterPattern + ' ' + Transform ( m.loField.nWidth )

	*!*					Endif && ! Isnull ( loField.nWidth )

	*!*				Endif && ! Empty ( lcText ) And loField.cDataType # 'M'

	*!*				If ! Isnull ( m.loField.nWidth )
	*!*					If m.loField.nWidth > m.lnWidth
	*!*						m.lnWidth = m.loField.nWidth

	*!*					Endif

	*!*					m.lnWidthTot = m.lnWidthTot + m.loField.nWidth
	*!*					m.lnWidthCnt = m.lnWidthCnt + 1

	*!*				Endif

	*!*				* TODO: Calculate caption width.
	*!*				If ! Isnull ( m.loField.nCaptionWidth )
	*!*					If m.loField.nCaptionWidth > m.lnCaptionWidth
	*!*						m.lnCaptionWidth = m.loField.nCaptionWidth

	*!*					Endif && loField.nCaptionWidth > lnCaptionWidth

	*!*					m.lnCaptionWidthTot = m.lnCaptionWidthTot + m.loField.nCaptionWidth
	*!*					m.lnCaptionWidthCnt = m.lnCaptionWidthCnt + 1

	*!*				Endif && ! Isnull ( loField.nCaptionWidth )

	*!*				For m.liIdx = 1 To Amembers ( laMembers, m.loField )
	*!*					m.lcMember = laMembers[ m.liIdx ]
	*!*					If ! ( '|' + Upper ( m.lcMember ) + '|' $ '|CAPTION|CCAPTION|CCOMMENT|CCUETEXT|CDATATYPE|CDISPLAYCLASS|CDISPLAYCLASSLIBRARY|CFORMAT|CINPUTMASK|CLASS|CLASSLIBRARY|FORMAT|INPUTMASK|LFORCEREADONLY|LREADONLY|NCAPTIONWIDTH|NWIDTH|PARENT|TOOLTIPTEXT|WIDTH|' )
	*!*						m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cAttributeNameDelimiterPattern + m.lcMember + ' ' + This.cAttributeValueDelimiterPattern + ' ' + Transform ( Getpem ( m.loField, m.lcMember ) )

	*!*					Endif

	*!*				Next

	*!*				m.lcBodyMarkup = m.lcBodyMarkup + ' ' + This.cFieldDelimiterPattern

	*!*			Endfor

	*!*			If m.lnWidthCnt > 0
	*!*				This.nColumnWidth = (m.lnWidthTot / m.lnWidthCnt ) + (m.lnCaptionWidthTot / m.lnCaptionWidthCnt )

	*!*			Endif

	*!*			m.loColFields = Null

	*!*			* Debugout Time(), Program(), '[Markup]', m.lcBodyMarkup
	*!*			Return m.lcBodyMarkup

	*!*			If .F.

	*!*				*!*



	*!*				*!*	lcCaption = DBGetProp ( lcField, 'FIELD', 'Caption' )
	*!*				*!*	lcInputMask = DBGetProp ( lcField, 'FIELD', 'InputMask' )
	*!*				*!*	llHasCaption = ! Empty( lcCaption )
	*!*				*!*	lcComment = DBGetProp ( lcField, 'FIELD', 'Comment' )
	*!*				*!*	lnLength = laFields[ lnX, 3 ] && + laFields[ lnX, 4 ]

	*!*				*!*	Do Case
	*!*				*!*		Case llHasCaption
	*!*				*!*			lcText = lcCaption
	*!*				*!*			lnLen = Len( lcCaption )

	*!*				*!*		Case ! Empty( lcInputMask )
	*!*				*!*			lcText = lcInputMask
	*!*				*!*			lnLen = Len( lcInputMask )

	*!*				*!*		Otherwise
	*!*				*!*			lcText = Replicate( 'M', lnLength )
	*!*				*!*			lnLen = lnLength

	*!*				*!*	Endcase

	*!*				*!*	* Fix en los casos que el Caption es mas chico que el tama�o del campo.
	*!*				*!*	If lnLen < lnLength
	*!*				*!*		lcText = Replicate( 'M', lnLength )
	*!*				*!*		lnLen = lnLength

	*!*				*!*	EndIf && lnLen < lnLength

	*!*				*!*	lnLen = Max( Txtwidth( lcText, lcFontName, lnFontSize, lcFontStyle ), lnLen )
	*!*				*!*	lnAverageCharacterWidth = Fontmetric( TM_AVECHARWIDTH, lcFontName, lnFontSize, lcFontStyle )
	*!*				*!*	AddProperty( loField, 'nWidth', Ceiling( lnAverageCharacterWidth * ( lnLen + lnExtraLen ) ) )

	*!*				*!*	If llHasCaption
	*!*				*!*		lcText = lcCaption
	*!*				*!*		lnLen = Len( lcCaption )

	*!*				*!*		lnLen = Max( Txtwidth( lcText, lcFontName, lnFontSize, lcFontStyle ), lnLen )
	*!*				*!*		lnAverageCharacterWidth = Fontmetric( TM_AVECHARWIDTH, lcFontName, lnFontSize, lcFontStyle )
	*!*				*!*		AddProperty( loField, 'nCaptionWidth', Ceiling( lnAverageCharacterWidth * ( lnLen + lnExtraLen ) ) )

	*!*				*!*	Endif

	*!*				*!*	nWidth
	*!*				*!*	nCaptionWidth



	*!*				*!*	KeyCalMonthForm
	*!*				*!*	'Controls\prg\Datepicker.prg'


	*!*				*!*		.ToolTipText = '<<loField.cToolTipText>>'
	*!*				*!*		.cCueText = '<<loField.cToolTipText>>'

	*!*			Endif

	*!*		Endproc && GetBodyMarkupForAll

	* HookGetBodyMarkupForAllCustomiseField
	Procedure HookGetBodyMarkupForAllCustomiseField ( toField )
		* Hook all fields

	Endproc

	* GetFooterMarkup
	Procedure GetFooterMarkup

		Local lc1 As String, ;
			lc2 As String, ;
			lcMarkup As String

		* Debugout Time( 0 ), Program()

		m.lc1 = This.cAttributeNameDelimiterPattern
		m.lc2 = This.cAttributeValueDelimiterPattern

		TEXT To m.lcMarkup Noshow Textmerge

			<<lc1>>class <<lc2>> 'DF_HorizontalLine'
			<<lc1>>name <<lc2>> 'lineFooter'
			<<lc1>>left <<lc2>> 10
			<<lc1>>anchor <<lc2>> 14 |

			<<lc1>>class <<lc2>> 'DF_SaveButton'
			<<lc1>>name <<lc2>> 'cmdSave'
			<<lc1>>width <<lc2>> 80
			<<lc1>>left <<lc2>> (This.oContainer.Width - 200)
			<<lc1>>anchor <<lc2>> 12 |

			<<lc1>>class <<lc2>> 'DF_CancelButton'
			<<lc1>>name <<lc2>> 'cmdCancel'
			<<lc1>>width <<lc2>> 80
			<<lc1>>left <<lc2>> (This.oContainer.Width - 100)
			<<lc1>>row-increment <<lc2>> 0
			<<lc1>>anchor <<lc2>> 12 |

		ENDTEXT

		Return m.lcMarkup

	Endproc && GetFooterMarkup

	* GetPopupFormBodyMarkup
	Procedure GetPopupFormBodyMarkup

		Local lc1 As String, ;
			lc2 As String, ;
			lcMarkup As String

		* Debugout Time( 0 ), Program()

		m.lc1 = This.cAttributeNameDelimiterPattern
		m.lc2 = This.cAttributeValueDelimiterPattern

		TEXT To m.lcMarkup Noshow Textmerge

			__ControlSource__
				<<lc1>>class <<lc2>> '<<This.cPopupFormEditboxClass>>'
				<<lc1>>left <<lc2>> 10
				<<lc1>>width <<lc2>> <<This.nPopupFormEditboxWidth>>
				<<lc1>>height <<lc2>> <<This.nPopupFormEditboxHeight>>
				<<lc1>>anchor <<lc2>> 15
				<<lc1>>label.caption <<lc2>> '' |
		ENDTEXT

		Return m.lcMarkup

	Endproc && GetPopupFormBodyMarkup

	* PrepareRegex
	Procedure PrepareRegex

		Local lcBodyMarkup As String, ;
			lcPattern As String, ;
			loMathes As Object

		* Debugout Time( 0 ), Program()

		*-- This method will analyze the cBodyMarkup block and attempt to assess wheter it uses the origial : and => delimiters
		*-- for attributes and values, or the newer . and = delimiters. If neither of these appear to be used, it will use the
		*-- properties for cAttributeNameDelimiterPattern and cAttributeValueDelimiterPattern from the class properties.

		*--	'\w*\.?-?\w*\s*' + ;&& This one only supported one dash. New one supports mulitples dashes in attribute names

		*-- DAE 2012-11-30 Add support for syntax. Object.Object.Attribute for adding grids and modify their attributes
		m.lcPattern = '\w*(\.?[-?\w*])*\s*' && .Pattern allows for '-' in attribute name. Will be converted to underscore, when stored as a poperty on toField object

		With This.oRegex && .Pattern allows for '-' in attribute name. Will be converted to underscore, when stored as a poperty on toField object
			.IgnoreCase = .T.
			.Global     = .T.
			.MultiLine  = .T.

		Endwith

		m.lcBodyMarkup      = Nvl ( This.cBodyMarkup, '' )
		This.oRegex.Pattern = '(:' + m.lcPattern + '=>\s*)'
		m.loMathes          = This.oRegex.Execute ( m.lcBodyMarkup )

		If m.loMathes.Count > 0 && First, look for the original markup delimieters (: and =>)
			This.cAttributeNameDelimiterPattern  = ':'
			This.cAttributeValueDelimiterPattern = '=>'

		Else && loMathes.Count > 0
			This.oRegex.Pattern = '(.' + m.lcPattern + '=\s*)'
			m.loMathes          = This.oRegex.Execute ( m.lcBodyMarkup ) && First, look for the newer markup delimieters (. and =)
			If m.loMathes.Count > 0
				This.cAttributeNameDelimiterPattern  = '.'
				This.cAttributeValueDelimiterPattern = '='

			Endif && loMathes.Count > 0

		Endif && loMathes.Count > 0

		*-- If about two test had not mathes, then use whatever properties are on the class
		If m.loMathes.Count == 0
			This.oRegex.Pattern	= '(' + This.EscapeForRegex ( This.cAttributeNameDelimiterPattern ) + m.lcPattern + This.EscapeForRegex ( This.cAttributeValueDelimiterPattern ) + '\s*)'

		Endif && loMathes.Count == 0

	Endproc && PrepareRegex

Enddefine

* DF_ErrorContainer
Define Class DF_ErrorContainer As Container

	BorderStyle = 1
	BackStyle   = 0
	BorderWidth = 1
	BorderColor = Rgb(255, 0, 0)

	cErrorMsg     = ''
	DataType      = ''
	row_increment = 1
	ControlSource = ''

	Add Object lblError As Label With ;
		Top = 5, ;
		Left = 5, ;
		AutoSize = .T., ;
		FontName = 'Arial', ;
		FontSize = 9, ;
		Name = 'lblError', ;
		ForeColor = Rgb(255, 0, 0), ;
		Caption = ''

	* cErrorMsg_Assign
	Protected Procedure cErrorMsg_Assign
		Lparameters tcMessage

		* Debugout Time( 0 ), Program(), tcMessage

		This.lblError.Caption = m.tcMessage

		This.Refresh()

	Endproc && cErrorMsg_Assign

Enddefine && DF_ErrorContainer

* DF_ResultButton
Define Class DF_ResultButton As CommandButton

	* Click
	Procedure Click

		* Debugout Time( 0 ), Program()

		AddProperty ( Thisform, 'cReturn', This.Tag )

		If Thisform.WindowType == 0 && If Modeless, Release the form.
			Thisform.Release()

		Else && Thisform.WindowType == 0
			Thisform.Hide() && If Modal, just hide the form. Calling code should release it when ready to do so.

		Endif && Thisform.WindowType == 0

	Endproc && Click

	* Init
	Procedure Init
		* Debugout Time( 0 ), Program()
		This.Tag = This.Caption

	Endproc && Init

	* Caption_Assign
	Protected Procedure Caption_Assign
		Lparameters tcCaption

		* Debugout Time( 0 ), Program(), tcCaption

		This.Caption = m.tcCaption
		This.Tag     = Strtran (m.tcCaption, '\<', '')

	Endproc && Caption_Assign

Enddefine && DF_ResultButton

* DF_SaveButton
Define Class DF_SaveButton As DF_ResultButton

	Width   = 50
	Height  = 30
	Default = .T.
	Caption = 'Save'

	* Init
	Procedure Init

		* Debugout Time( 0 ), Program()

		AddProperty (Thisform, 'lSaveClicked', .F.)
		This.Caption = Nvl (This.Parent.oRenderEngine.cSaveButtonCaption, This.Caption)
		DoDefault()

	Endproc && Init

	* Click
	Procedure Click

		Local lcSaveCommand As String, ;
			llReturn As Boolean, ;
			loErr As Object, ;
			loRenderEngine As Object

		* Debugout Time( 0 ), Program()

		m.loRenderEngine = Thisform.oRenderEngine

		*-- If a BusinessObject and Save method is assigned, call the BO to save the data
		If Vartype (m.loRenderEngine.oBusinessObject) = 'O' And !Empty (m.loRenderEngine.cBusinessObjectSaveMethod)
			m.lcSaveCommand = 'llReturn = Thisform.oRenderEngine.oBusinessObject.' + m.loRenderEngine.cBusinessObjectSaveMethod

			Try
				&lcSaveCommand.

			Catch To m.loErr
				DebugoutCls
				Messagebox ('Error calling ' + m.loRenderEngine.cBusinessObjectSaveMethod + ' on oBusinessObject.', 0, 'Error:')

			Endtry

		Else
			m.llReturn = .T.

		Endif

		If m.llReturn = .T.
			Thisform.lSaveClicked = .T.
			Thisform.Save() && Call this in case the Form classs has code in it, and so any BindEvent can be triggered.
			DoDefault()

		Else
			This.SaveError()

		Endif

	Endproc && Click


	* SaveError
	Procedure SaveError

		Local loRenderEngine As Object

		* Debugout Time( 0 ), Program()

		m.loRenderEngine = Thisform.oRenderEngine

		If m.loRenderEngine.lShowSaveErrors = .T.
			Messagebox (m.loRenderEngine.cSaveErrorMsg, 0, m.loRenderEngine.cSaveErrorCaption)

		Endif

	Endproc && SaveError

Enddefine && DF_SaveButton

* CommandButton
Define Class DF_EditButton As CommandButton

	Caption = '...'
	Width   = 20
	Height  = 20

	* Click
	Procedure Click
		This.oEditBox.SetFocus()
		This.oEditBox.EditData()

	Endproc && Click

Enddefine && CommandButton

* DF_CancelButton
Define Class DF_CancelButton As DF_ResultButton

	Width   = 50
	Height  = 30
	Cancel  = .T.
	Caption = 'Cancel'

	* Init
	Procedure Init

		* Debugout Time( 0 ), Program()

		This.Caption = Nvl (This.Parent.oRenderEngine.cCancelButtonCaption, This.Caption)
		AddProperty (Thisform, 'lCancelClicked', .F.)
		DoDefault()

	Endproc && Init

	* Click
	Procedure Click

		* Debugout Time( 0 ), Program()

		If Pemstatus (Thisform, 'lRestoreDataOnCancel', 5) And Thisform.lRestoreDataOnCancel = .T.
			Thisform.RestoreData()

		Endif

		Thisform.lCancelClicked = .T.

		DoDefault()

	Endproc && Click

Enddefine && DF_CancelButton

* DF_Label
Define Class DF_Label As Label

	BackStyle = 0 && Transparent

Enddefine && DF_Label


* DF_Checkbox
Define Class DF_Checkbox As Checkbox

	BackStyle = 0 && Transparent

Enddefine && DF_Checkbox

* DF_HorizontalLine
Define Class DF_HorizontalLine As Line

	Height = 0
	Left   = -1
	Width  = 10000
	Anchor = 10

Enddefine && DF_HorizontalLine

* DF_MemoFieldEditBox
Define Class DF_MemoFieldEditBox As EditBox

	cDF_Class          = 'DynamicForm'
	cEditboxClass      = 'Editbox'
	cSaveButtonCaption = 'OK'
	nEditBoxWidth      = 500
	nEditboxHeight     = 300
	nEditboxAnchor     = 15
	oRenderEngine      = Null && Will be set by GenerateControl() method in DF Render Engine
	lIsPopUp           = .F.
	lInFocus           = .F.

	* DblClick
	Procedure DblClick
		This.EditData()

	Endproc && DblClick

	* EditData
	Procedure EditData

		Local lcBodyMarkup As String, ;
			lcDF_Class As String, ;
			loForm As Object, ;
			loParentForm As Object, ;
			loParentRenderEngine As Object

		* Debugout Time( 0 ), Program()

		If ! This.lIsPopUp
			m.lcDF_Class = This.cDF_Class
			Do Case
				Case ! Upper (m.lcDF_Class) == Upper ('DynamicForm')

				Case Thisform.Desktop == .T.
					m.lcDF_Class = Thisform.Class

				Case Thisform.ShowWindow # 0
					m.lcDF_Class = Thisform.Class

			Endcase

			m.loForm = Createobject ( This.cDF_Class )

			*-- Theses setting control the visual layout of the pop-up form...
			m.loForm.Caption = 'Edit ' + This.cLabelCaption

			m.loParentRenderEngine = This.oRenderEngine

			*-- Pass properties from original Render Engine to new form...
			With m.loParentRenderEngine
				m.loForm.oDataObject               = .oDataObject
				m.loForm.oBusinessObject           = .oBusinessObject
				m.loForm.cDataObjectRef            = .cDataObjectRef
				m.loForm.cBusinessObjectSaveMethod = .cBusinessObjectSaveMethod
				m.loForm.cAlias                    = .cAlias

			Endwith

			m.lcBodyMarkup = Nvl ( m.loParentRenderEngine.cPopupFormBodyMarkup, m.loParentRenderEngine.GetPopupFormBodyMarkup() )
			* lcBodyMarkup = Strtran(lcBodyMarkup, '__ControlSource__', This.ControlSource, 1, 1 ,1)
			m.lcBodyMarkup = Strtran ( m.lcBodyMarkup, '__ControlSource__', This.cControlSource + CR + m.loParentRenderEngine.cAttributeNameDelimiterPattern + 'lIsPopUp' + m.loParentRenderEngine.cAttributeValueDelimiterPattern + '.T.', 1, 1, 1 )
			* lcBodyMarkup = Strtran( lcBodyMarkup, 'Thisform.oDataObject.', '', 1, 1000 ,1)
			m.loForm.cBodyMarkup                                     = m.lcBodyMarkup
			m.loForm.oRenderEngine.lGenerateEditButtonsForMemoFields = .F.

			*-- Find the parent form so we can center new form in parent form
			m.loParentForm = This.Parent
			Do While Lower ( m.loParentForm.BaseClass ) # 'form'
				m.loParentForm = m.loParentForm.Parent

			Enddo

			* m.loForm.Show ( 0, loParentForm )
			m.loForm.Show ( Thisform.WindowType, m.loParentForm )

		Endif && ! This.lIsPopUp

	Endproc && EditData

	* Destroy
	Protected Procedure Destroy
		* Debugout Time( 0 ), Program()

		This.oRenderEngine = Null
		DoDefault()

	Endproc && Destroy

	* MouseEnter
	Procedure MouseEnter ( nButton, nShift, nXCoord, nYCoord )
		* Debugout Time( 0 ), Program(), nButton, nShift, nXCoord, nYCoord
		If ! This.lInFocus
			This.MousePointer = 15

		Endif

	Endproc && MouseEnter

	* MouseLeave
	Procedure MouseLeave ( nButton, nShift, nXCoord, nYCoord )
		* Debugout Time( 0 ), Program(), nButton, nShift, nXCoord, nYCoord
		This.MousePointer =	0

	Endproc && MouseLeave

	* GotFocus
	Procedure GotFocus()
		* Debugout Time( 0 ), Program()
		This.lInFocus     = .T.
		This.MousePointer =	0

	Endproc && GotFocus

	* LostFocus
	Procedure LostFocus()
		This.lInFocus = .F.

	Endproc && LostFocus

Enddefine && DF_MemoFieldEditBox

* DynamicFormDeskTop
Define Class DynamicFormDeskTop As DynamicForm
	Desktop = .T.

Enddefine && DynamicFormDeskTop

* DynamicFormShowWindow
Define Class DynamicFormShowWindow As DynamicForm
	ShowWindow = 1

Enddefine && DynamicFormShowWindow
