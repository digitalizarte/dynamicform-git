#Define DebugoutCls Debugout Time(0), Program(), m.loErr.Message, m.loErr.Details, m.loErr.ErrorNo, m.loErr.LineContents, m.loErr.StackLevel, m.loErr.Procedure, m.loErr.Lineno

Private loJob, loObject
Private lnPrice, laOptions[1], lnOption

loObject = Createobject ('Empty')
AddProperty (m.loObject, 'id', '12345')
AddProperty (m.loObject, 'first_name', 'Joe')
AddProperty (m.loObject, 'mid_init', 'N.')
AddProperty (m.loObject, 'last_name', 'Coderman')
AddProperty (m.loObject, 'ad_type', 'Banner')
AddProperty (m.loObject, 'notes', 'This man came here and wrote some codez.')
AddProperty (m.loObject, 'still_here', .F.)
AddProperty (m.loObject, 'has_laptop', .T.)
AddProperty (m.loObject, 'bool_1', .T.)
AddProperty (m.loObject, 'bool_2', .T.)
AddProperty (m.loObject, 'bool_3', .T.)
AddProperty (m.loObject, 'weight', 185)

*-- Example of a Private variables that can be bound to also
lnPrice  = 107.15
lnOption = 2

*-- Array of options/values to display in the ComboBox defined in cFieldList above (note that it's declared Private above ---
Dimension m.laOptions[3]
laOptions[1] = 'Banner'
laOptions[2] = 'Placard'
laOptions[3] = 'Name Tag'

If !Used ('Jobs')
	Use Demos\Jobs In 0
Endif

Select Jobs
Goto Top
Scatter Name m.loJob Memo

Set Procedure To DynamicForm Additive

? ' '
? ' '
? ' '

*clear
Wait Window  'Test: execute_code_test' Noclear Nowait
Try
	Do execute_code_test
Catch To loErr
	DebugoutCls
Endtry
Wait Window  'Test: largedemo1' Noclear Nowait
Try
	Do largedemo1 With .F., ':class => "label" :caption => "Thanks!!" | :class => "DF_HorizontalLine"', '', ':class => "DF_CancelButton"'
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: passing_a_container_to_the_render_engine_test' Noclear Nowait
Try
	Do passing_a_container_to_the_render_engine_test
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: attributes_with_numbers_passed_as_strings_test' Noclear Nowait
Try
	Do attributes_with_numbers_passed_as_strings_test
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: jrn_syntax_test' Noclear Nowait
Try
	Do jrn_syntax_test
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: bind_to_all_fields_on_an_alias_test' Noclear Nowait
Try
	Do bind_to_all_fields_on_an_alias_test
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: modeless_form_test' Noclear Nowait
Try
	Do modeless_form_test
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: minheight_and_minwidth_test' Noclear Nowait
Try
	Do minheight_and_minwidth_test
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: binding_errors_test' Noclear Nowait
Try
	Do binding_errors_test
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: largedemo1(.T.)' Noclear Nowait
Try
	Do largedemo1 With .T.
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: largedemo1(.F.)' Noclear Nowait
Try
	Do largedemo1 With .F.
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: largedemo1(.T., "", "", ":class => [DF_CancelButton]" )' Noclear Nowait
Try
	Do largedemo1 With .T., '', '', ':class => "DF_CancelButton"'
Catch To loErr
	DebugoutCls
Endtry

Wait Window  'Test: largedemo1(.T., ":class => [DF_CancelButton]", "", "" )' Noclear Nowait
Try
	Do largedemo1 With .T., ':class => "DF_CancelButton"', '', ''
Catch To loErr
	DebugoutCls
Endtry

Wait Clear

*---------------------------------------------------------------------------------------
Procedure passing_a_container_to_the_render_engine_test

	Local lcBodyMarkup As String, ;
		llResult As Boolean, ;
		loContainer As 'container', ;
		loEngine As 'DynamicFormRenderEngine'

	PrintTestName()

	loEngine = Createobject ('DynamicFormRenderEngine')

	lcBodyMarkup           = 'job_num :enabled => .F. | cust_num | status | job_start |'
	loEngine.cFooterMarkup = ''

	loEngine.cBodyMarkup  = lcBodyMarkup
	loEngine.oDataObject  = m.loJob
	loEngine.lLabelsAbove = .T.

	loContainer = Createobject ('container')
	llResult    = loEngine.Render (loContainer)

	PrintResult (llResult)

Endproc


*---------------------------------------------------------------------------------------
Procedure modeless_form_test

	Local lcBodyMarkup As String, ;
		llResult As Boolean, ;
		loForm As 'DynamicForm'

	PrintTestName()

	loForm                            = Createobject ('DynamicForm')
	lcBodyMarkup                      = BigFieldList()
	loForm.cBodyMarkup                = lcBodyMarkup
	loForm.cHeading                   = gcTestName
	loForm.oRenderEngine.lLabelsAbove = .T.
	loForm.oDataObject                = m.loObject && Set the data object that the form fields bind to

	llResult = loForm.Render()

	loForm.lClearEventsOnClose = .T.
	loForm.Show(0)

	Read Events

	PrintResult (llResult)

Endproc

*---------------------------------------------------------------------------------------
Procedure jrn_syntax_test

	Local lcBodyMarkup As String, ;
		llResult As Boolean, ;
		loForm As 'DynamicForm'

	PrintTestName()

	loForm                            = Createobject ('DynamicForm')
	lcBodyMarkup                      = BigFieldList()
	lcBodyMarkup                      = Strtran (lcBodyMarkup, ':', '.')
	lcBodyMarkup                      = Strtran (lcBodyMarkup, '=>', '=')
	loForm.cHeading                   = gcTestName
	loForm.oRenderEngine.lLabelsAbove = .T.
	loForm.oDataObject                = m.loObject && Set the data object that the form fields bind to

	llResult = loForm.Render (lcBodyMarkup )
	PrintResult (llResult)
	loForm.Show()


Endproc
*---------------------------------------------------------------------------------------
Procedure attributes_with_numbers_passed_as_strings_test

	Local lcBodyMarkup As String, ;
		llResult As Boolean, ;
		loForm As 'DynamicForm'

	Private lcTest

	PrintTestName()

	lcTest = ''
	loForm = Createobject ('DynamicForm')

	loForm.cHeading                   = gcTestName
	loForm.oRenderEngine.lLabelsAbove = .T.

	TEXT To lcBodyMarkup Noshow

			:class => 'label' :caption => 'This next textbox should be pushed down and in very far due to margin-top and  margin-left '
				:width => 500 :height => 100 :wordwrap => .t. |

			lcTest :left => '10' :margin-left => '200' :top => '70' :margin-top => '200' :margin-bottom => '200' |

			:class => 'label' :caption => 'This label should be pushed down due to margin-bottom of previous control'
				:width => 500 :height => 100 :wordwrapt => .t.

	ENDTEXT

	llResult = loForm.Render (lcBodyMarkup)
	PrintResult (llResult)
	loForm.Show()

Endproc

*---------------------------------------------------------------------------------------
Procedure bind_to_all_fields_on_an_alias_test

	Local lcBodyMarkup As String, ;
		llResult As Boolean, ;
		loForm As 'DynamicForm'

	PrintTestName()

	loForm                             = Createobject ('DynamicForm')
	loForm.cHeading                    = gcTestName
	loForm.cAlias                      = 'Jobs'
	loForm.oRenderEngine.nColumnHeight = 400

	llResult = loForm.Render()
	PrintResult (llResult)
	loForm.Show()

Endproc

*---------------------------------------------------------------------------------------
Procedure minheight_and_minwidth_test

	Local lcBodyMarkup As String, ;
		llResult As Boolean, ;
		loForm As 'DynamicForm'

	PrintTestName()

	loForm          = Createobject ('DynamicForm')
	loForm.cHeading = gcTestName
	lcBodyMarkup    = 'Month | llProj :Caption => "Projections?"'

	TEXT To lcBodyMarkup Noshow

		:class => 'label' :width => 300 :height => 100 :wordwrap => .t.
				:caption => 'This test tests the MinWidth and MinHeight for the form, so the form should render much larger than
										its normal resize-to-fit-controls behaviour.'

	ENDTEXT

	loForm.MinHeight = 700
	loForm.MinWidth  = 700

	llResult = loForm.Render (lcBodyMarkup)
	PrintResult (llResult)
	loForm.Show()

Endproc

*---------------------------------------------------------------------------------------
Procedure execute_code_test

	Local lcBodyMarkup As String, ;
		llResult As Boolean, ;
		loForm As 'DynamicForm'

	PrintTestName()

	loForm = Createobject ('DynamicForm')

	TEXT To lcBodyMarkup Noshow

		(MessageBox('Executing some code from markup.')) |
		.cHeading = (gcTestName) |
		(This.nLastControlBottom = 200) |
		.class = 'label'
			.width = 300
			.height = 100
			.wordwrap = .t.
			.caption = 'This test executes some FoxPro code.' |
		(? Chr(13) + Chr(13) + Chr(13) + 'Did you see a messagebox?') |



	ENDTEXT

	loForm.MinHeight = 700
	loForm.MinWidth  = 700

	llResult = loForm.Render (lcBodyMarkup)
	PrintResult (llResult)
	loForm.Show()

Endproc

*---------------------------------------------------------------------------------------
Procedure binding_errors_test

	Local lcBodyMarkup As String, ;
		llResult As Boolean, ;
		loForm As 'DynamicForm'
	*:Global PrintResult[1]
	Private lcTest

	PrintTestName()

	loForm          = Createobject ('DynamicForm')
	loForm.cHeading = gcTestName
	lcBodyMarkup    = 'lnMonth | llProj'

	TEXT To lcBodyMarkup Noshow
		llNotDefined_1 |
		llNotDefined_2 :width => 200 |
		:class => 'label' :width => 300 :height => 100 :wordwrap => .t.
				:caption => ('This markup attempts to bind to private vars which are not defined. There should be' +
											'two red error boxes on the form.')

	ENDTEXT

	llResult = loForm.Render (lcBodyMarkup)
	PrintResult (loForm.oRenderEngine.nErrorCount = 2)
	loForm.Show()

Endproc



*---------------------------------------------------------------------------------------
Procedure largedemo1

	Lparameters tlLabelsAbove, tcHeaderMarkup, tcBodymarkup, tcFooterMarkup

	Local llResult As Boolean, ;
		loForm As 'DynamicForm'

	PrintTestName()

	loForm = Createobject ('DynamicForm')

	loForm.Caption     = 'Edit data'
	loForm.oDataObject = m.loObject && Set the data object that the form fields bind to
	loForm.cHeading    = 'cHeading appears here, if set.'

	loForm.oRenderEngine.lLabelsAbove = tlLabelsAbove && Generate label above each control (default is to the left of the control) (default is .F., which meaans "inline with controls")

	*-- Set Header markup -------------------------
	If Vartype (tcHeaderMarkup) = 'C'
		loForm.cHeaderMarkup = tcHeaderMarkup
	Endif

	*-- Set Body markup -------------------------
	If Empty (tcBodymarkup)
		loForm.oRenderEngine.cBodyMarkup =  BigFieldList()
	Else
		loForm.oRenderEngine.cBodyMarkup =  tcBodymarkup
	Endif

	*-- Set Footer markup -------------------------
	If Vartype (tcFooterMarkup) = 'C'
		loForm.cFooterMarkup = tcFooterMarkup
	Endif

	llResult = loForm.Render()

	If llResult = .T.
		?? 'Passed.'
		loForm.Show(1) && 1 = Modal.  If you want to use Modeless, you'll need make a Read Events call here after showing form.
	Else
		?? '*** Failed.***'
		Messagebox (loForm.oRenderEngine.GetErrorsAsString(), 0, 'Notice:')
		loForm.Show(1)
	Endif

	If Vartype (loForm) = 'O' And Lower (loForm.cReturn) = 'save'
	Else
	Endif

	Release loForm

Endproc



*=======================================================================================
Procedure BigFieldList

	Local lcBodyMarkup As String

	TEXT To lcBodyMarkup Noshow

			id					:enabled => .f.
									:fontbold => .t.
									:label.FontBold => .t. |

			ad_type 		:class => 'combobox'
									:RowSource => 'laOptions'
									:RowSourceType => 5
									:top => 150|

			bool_3			:caption => 'You can specify BOLD captions.'
									:FontBold => '.t.'
									:width => '200'
									:margin-left => 50 |

			first_name	|
			mid_init		:row-increment => 0 |
			last_name		:row-increment => 0 |

			notes				:class => 'editbox'
									:width => 400
									:height => 80
									:anchor => 10
									:margin-left => 1
									:margin-top => 1
									:margin-bottom => 1 |

			lnPrice			:label.caption => 'List Price'
									:label.alignment => 1	|

			weight			:row-increment  => 0
									:label.alignment => 1 |

			lnOption		:class => 'optiongroup'
									:caption => 'Color options:'
									:buttoncount => 2
									:width => 200
									:height => 60
									:option1.caption => 'Red with orange stripes'
									:option1.autosize => .t.
									:option2.caption => 'Purple with black dots'
									:option2.autosize => .t. |

			:class => 'label' :caption => 'Thank you for trying DynamicForm.'
											  :autosize => .t.
												:render-if => (Day(Date()) > 0)
												:column => 1

	ENDTEXT

	Return lcBodyMarkup


Endproc


*---------------------------------------------------------------------------------------
Procedure PrintResult (tlResult)

	If tlResult
		?? ' passed.'
	Else
		?? '*** Failed.***'
	Endif


Endproc

*---------------------------------------------------------------------------------------
Procedure PrintTestName

	Public gcTestName

	gcTestName = Lower (Getwordnum (Sys(16, 2), 2))

	? gcTestName + '...'


Endproc
