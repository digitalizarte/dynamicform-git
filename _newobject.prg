#Define CR Chr( 13 )
* _Newobject
Procedure _Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22, tvParameter23, tvParameter24, tvParameter25, tvParameter26 )

	* Newobject
	Local lnPcounts As Number, ;
		loErr1 As Exception, ;
		loErr2 As Exception, ;
		loErr3 As Exception, ;
		loNewObject As Object

	Assert Vartype( tcClass ) == 'C' Message 'tcClass no es una cadena.'
	Assert Vartype( tcModule ) == 'C' Message 'tcModule no es una cadena.'
	Assert File( tcModule ) Message 'tcModule no se encuentra.'

	lnPcounts = Pcount()

	Try

		Do Case
			Case Inlist( lnPcounts, 1, 2, 3 )
				loNewObject = Createobject( tcClass )

			Case lnPcounts == 4
				loNewObject = Createobject( tcClass, tvParameter1 )

			Case lnPcounts == 5
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2 )

			Case lnPcounts == 6
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3 )

			Case lnPcounts == 7
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4 )

			Case lnPcounts == 8
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5 )

			Case lnPcounts == 9
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6 )

			Case lnPcounts == 10
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7 )

			Case lnPcounts == 11
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8 )

			Case lnPcounts == 12
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9 )

			Case lnPcounts == 13
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10 )

			Case lnPcounts == 14
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11 )

			Case lnPcounts == 15
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12 )

			Case lnPcounts == 16
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13 )

			Case lnPcounts == 17
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14 )

			Case lnPcounts == 18
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15 )

			Case lnPcounts == 19
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16 )

			Case lnPcounts == 20
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17 )

			Case lnPcounts == 21
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18 )

			Case lnPcounts == 22
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19 )

			Case lnPcounts == 23
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20 )

			Case lnPcounts == 24
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21 )

			Case lnPcounts == 25
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22 )

			Case lnPcounts == 26
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22, tvParameter23 )

			Case lnPcounts == 27
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22, tvParameter23, tvParameter24 )

			Case lnPcounts == 28
				loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22, tvParameter23, tvParameter24, tvParameter25 )

			Otherwise
				loNewObject = Createobject( tcClass )

		Endcase

	Catch To loErr1

		Try

			Set Procedure To ( tcModule ) Additive
			Do Case
				Case Inlist( lnPcounts, 1, 2, 3 )
					loNewObject = Createobject( tcClass )

				Case lnPcounts == 4
					loNewObject = Createobject( tcClass, tvParameter1 )

				Case lnPcounts == 5
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2 )

				Case lnPcounts == 6
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3 )

				Case lnPcounts == 7
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4 )

				Case lnPcounts == 8
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5 )

				Case lnPcounts == 9
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6 )

				Case lnPcounts == 10
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7 )

				Case lnPcounts == 11
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8 )

				Case lnPcounts == 12
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9 )

				Case lnPcounts == 13
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10 )

				Case lnPcounts == 14
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11 )

				Case lnPcounts == 15
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12 )

				Case lnPcounts == 16
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13 )

				Case lnPcounts == 17
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14 )

				Case lnPcounts == 18
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15 )

				Case lnPcounts == 19
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16 )

				Case lnPcounts == 20
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17 )

				Case lnPcounts == 21
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18 )

				Case lnPcounts == 22
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19 )

				Case lnPcounts == 23
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20 )

				Case lnPcounts == 24
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21 )

				Case lnPcounts == 25
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22 )

				Case lnPcounts == 26
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22, tvParameter23 )

				Case lnPcounts == 27
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22, tvParameter23, tvParameter24 )

				Case lnPcounts == 28
					loNewObject = Createobject( tcClass, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22, tvParameter23, tvParameter24, tvParameter25 )

				Otherwise
					loNewObject = Createobject( tcClass )

			Endcase

		Catch To loErr2
			Try
				Do Case
					Case lnPcounts == 1
						loNewObject = Newobject( tcClass )

					Case lnPcounts == 2
						loNewObject = Newobject( tcClass, tcModule )

					Case lnPcounts == 3
						loNewObject = Newobject( tcClass, tcModule, tcInApplication )

					Case lnPcounts == 4
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1 )

					Case lnPcounts == 5
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2 )

					Case lnPcounts == 6
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3 )

					Case lnPcounts == 7
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4 )

					Case lnPcounts == 8
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5 )

					Case lnPcounts == 9
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6 )

					Case lnPcounts == 10
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7 )

					Case lnPcounts == 11
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8 )

					Case lnPcounts == 12
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9 )

					Case lnPcounts == 13
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10 )

					Case lnPcounts == 14
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11 )

					Case lnPcounts == 15
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12 )

					Case lnPcounts == 16
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13 )

					Case lnPcounts == 17
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14 )

					Case lnPcounts == 18
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15 )

					Case lnPcounts == 19
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16 )

					Case lnPcounts == 20
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17 )

					Case lnPcounts == 21
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18 )

					Case lnPcounts == 22
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19 )

					Case lnPcounts == 23
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20 )

					Case lnPcounts == 24
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21 )

					Case lnPcounts == 25
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22 )

					Case lnPcounts == 26
						loNewObject = Newobject( tcClass, tcModule, tcInApplication, tvParameter1, tvParameter2, tvParameter3, tvParameter4, tvParameter5, tvParameter6, tvParameter7, tvParameter8, tvParameter9, tvParameter10, tvParameter11, tvParameter12, tvParameter13, tvParameter14, tvParameter15, tvParameter16, tvParameter17, tvParameter18, tvParameter19, tvParameter20, tvParameter21, tvParameter22, tvParameter23 )

					Otherwise
						loNewObject = Newobject( tcClass, tcModule, tcInApplication )

				Endcase
			Catch To loErr3
				loErr3.Message = loErr3.Message + CR + CR ;
					+ 'Parameters: ' + Transform( lnPcounts ) + CR ;
					+ '	tcClass: ' + Transform( tcClass ) + CR ;
					+ '	tcModule: ' + Transform( tcModule ) + CR ;
					+ '	tcInApplication: ' + Transform( tcInApplication ) + CR ;
					+ '	tvParameter1: ' + Transform( tvParameter1 ) + CR ;
					+ '	tvParameter2: ' + Transform( tvParameter2 ) + CR ;
					+ '	tvParameter3: ' + Transform( tvParameter3 ) + CR ;
					+ '	tvParameter4: ' + Transform( tvParameter4 ) + CR ;
					+ '	tvParameter5: ' + Transform( tvParameter5 ) + CR ;
					+ '	tvParameter6: ' + Transform( tvParameter6 ) + CR ;
					+ '	tvParameter7: ' + Transform( tvParameter7 ) + CR ;
					+ '	tvParameter8: ' + Transform( tvParameter8 ) + CR ;
					+ '	tvParameter9: ' + Transform( tvParameter9 ) + CR ;
					+ '	tvParameter10: ' + Transform( tvParameter10 ) + CR ;
					+ '	tvParameter11: ' + Transform( tvParameter11 ) + CR ;
					+ '	tvParameter12: ' + Transform( tvParameter12 ) + CR ;
					+ '	tvParameter13: ' + Transform( tvParameter13 ) + CR ;
					+ '	tvParameter14: ' + Transform( tvParameter14 ) + CR ;
					+ '	tvParameter15: ' + Transform( tvParameter15 ) + CR ;
					+ '	tvParameter16: ' + Transform( tvParameter16 ) + CR ;
					+ '	tvParameter17: ' + Transform( tvParameter17 ) + CR ;
					+ '	tvParameter18: ' + Transform( tvParameter18 ) + CR ;
					+ '	tvParameter19: ' + Transform( tvParameter19 ) + CR ;
					+ '	tvParameter20: ' + Transform( tvParameter20 ) + CR ;
					+ '	tvParameter21: ' + Transform( tvParameter21 ) + CR ;
					+ '	tvParameter22: ' + Transform( tvParameter22 ) + CR ;
					+ '	tvParameter23: ' + Transform( tvParameter23 ) + CR ;
					+ '	tvParameter24: ' + Transform( tvParameter24 ) + CR ;
					+ '	tvParameter25: ' + Transform( tvParameter25 ) + CR ;
					+ '	tvParameter26: ' + Transform( tvParameter26 ) + CR

				Throw

			Finally

			Endtry

		Finally

		Endtry

	Finally

	Endtry

	Assert Vartype( loNewObject ) == 'O' Message 'loNewObject no es un objeto.'

	Return loNewObject

Endproc && _Newobject

